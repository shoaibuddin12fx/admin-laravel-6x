<?php

// Code within app\Helpers\Helper.php

namespace App\Helpers;

use App\Models\Job;
use App\Models\Jobstatus;
use App\Models\Message;
use App\Models\OtpNumbers;
use App\Models\Room;
use App\Models\Settings;
use Illuminate\Support\Facades\Storage;

class Helper
{

    public static function getDistanceBetweenPoints($lat1, $lon1, $lat2, $lon2) {
        $theta = $lon1 - $lon2;
        $miles = (sin(deg2rad($lat1)) * sin(deg2rad($lat2))) + (cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta)));
        $miles = acos($miles);
        $miles = rad2deg($miles);
        $miles = $miles * 60 * 1.1515;
        return $miles;
//        $feet = $miles * 5280;
//        $yards = $feet / 3;
//        $kilometers = $miles * 1.609344;
//        $meters = $kilometers * 1000;
//        return compact('miles','feet','yards','kilometers','meters');
    }

    public static function findSplitAmounts($amount, $job_id){

        $customer_amount = $amount;
        $driver_amount = 0;
        $admin_amount = 0;
        $settings = Settings::first();
        $job = Job::where('id', $job_id)->first();

        if($settings && $job){
            $jobstatus = Jobstatus::where('job_id', $job->id)->whereNotNull('latitude')->whereNotNull('longitude')->get();

            $lat = 0;
            $lng = 0;

            $distance = $jobstatus->reduce(function($carry, $item) use ($lat, $lng){
                //
                if($lat == 0 || $lng == 0){
                    $lat = $item['latitude'];
                    $lng = $item['longitude'];
                }

                $miles = Helper::getDistanceBetweenPoints($lat, $lng, $item['latitude'], $item['longitude']);
                return $carry + $miles;
            }, 0);



            $admin_amount = $amount * ($settings->admin_commission / 100);
//            $customer_amount =

            return [
                'customer_amount' => $customer_amount,
                'driver_amount' => $driver_amount,
                'admin_amount' => $admin_amount
            ];








        }



        return [
            'customer_amount' => $customer_amount,
            'driver_amount' => $driver_amount,
            'admin_amount' => $admin_amount
        ];
    }

    public static function incrementalHash($num = 4) {

        $digit_random_number = $num == 4 ? mt_rand(1000, 9999) : mt_rand(100000, 999999) ;
        return $digit_random_number;
    }

    public static function generateOtp(){
        $hash = Helper::incrementalHash();
        while(OtpNumbers::where('otp_code', $hash)->first()){
            $hash = Helper::incrementalHash();
        }

        return $hash;
    }

    public static function validatePhoneNumber($contact){
        // Allow +, - and . in phone number
        $filtered_phone_number = filter_var($contact, FILTER_SANITIZE_NUMBER_INT);
        // Remove "-" from number
        $phone_to_check = str_replace("-", "", $filtered_phone_number);
        // Check the lenght of number
        // This can be customized if you want phone number from a specific country
        if (strlen($phone_to_check) < 10 || strlen($phone_to_check) > 14) {
            return false;
        } else {
            return true;
        }
    }

    public static function clean($string) {
        return preg_replace('/[^A-Za-z0-9 ]/', '', $string); // Removes special chars.
    }

    public static function split_name($name) {

        $name = self::clean($name);
        $parts = array();

        while ( strlen( trim($name)) > 0 ) {
            $name = trim($name);
            $string = preg_replace('#.*\s([\w-]*)$#', '$1', $name);
            $parts[] = $string;
            $name = trim( preg_replace('#'.$string.'#', '', $name ) );
        }

        if (empty($parts)) {
            return false;
        }

        $parts = array_reverse($parts);
        $name = array();
        $name['first_name'] = ucfirst($parts[0]);
        $name['middle_name'] = (isset($parts[2])) ? ucfirst($parts[1]) : '';
        $name['last_name'] = (isset($parts[2])) ? ucfirst($parts[2]) : ( isset($parts[1]) ? ucfirst($parts[1]) : '');

        return $name;
    }

    public static function sendMessagePush(Message $message) {

        $room = Room::where('id' , $message['room_id'])->first();

        if (!$room) {
            return false;
        }


        if ($message['user_id'] === $room['initiator_id']) {
            return "It wroks";
        } else if (($message['user_id'] === $room['recipient_id'])) {
            return "rft";
        }

    }
}



