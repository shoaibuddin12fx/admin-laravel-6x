<?php

namespace App\Helpers;
use GuzzleHttp\Client;

use Illuminate\Support\Facades\Log;
use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\SandboxEnvironment;
use PayPalCheckoutSdk\Orders\OrdersCaptureRequest;
use PayPalCheckoutSdk\Orders\OrdersCreateRequest;
use phpDocumentor\Reflection\Types\Integer;

ini_set('error_reporting', E_ALL); // or error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');

class PaypalHelper{

    public static function client()
    {
        return new PayPalHttpClient(self::environment());
    }

    /**
     * Set up and return PayPal PHP SDK environment with PayPal access credentials.
     * This sample uses SandboxEnvironment. In production, use LiveEnvironment.
     */
    public static function environment()
    {
        $paypal_conf = \Config::get('paypal');
        $islive = $paypal_conf["mode"] == 'sandbox' ? false : true;
        $client_id = $islive ? $paypal_conf["live"]["client_id"] : $paypal_conf["sandbox"]["client_id"];
        $client_secret = $islive ? $paypal_conf["live"]["client_secret"] : $paypal_conf["sandbox"]["client_secret"];
        return new SandboxEnvironment($client_id, $client_secret);
    }

    public function __construct()
    {
        /** PayPal api context **/


        // dd($paypal_conf);
//        $this->_api_context = new ApiContext(new OAuthTokenCredential(
//                $paypal_conf['client_id'],
//                $paypal_conf['secret'])
//        );
//        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    /**
     * @param $jobId
     * @param $amount
     * @param string $currency
     */
    public static function callJobPaypalOrderCreate($jobId, $amount, $currency = "USD" ){

        $request = new OrdersCreateRequest();
        $request->prefer('return=representation');
        $request->body = [
            "intent" => "CAPTURE",
            "purchase_units" => [[
                "reference_id" => $jobId,
                "amount" => [
                    "value" => $amount,
                    "currency_code" => $currency
                ]
            ]]
        ];

        try {
            // Call API with your client and get a response for your call
            $response = self::client()->execute($request);
            // If call returns body in response, you can get the deserialized version from the result attribute of the respons

            if($response->statusCode == 201){

                $result = $response->result;
                if(isset($result) && $result->id){
                    return $result->id;
                }else{
                    return -1;
                }

            }


        }catch (HttpException $ex) {
            //echo $ex->statusCode;
            Log::error($ex->statusCode, [$ex->getMessage()]);
            return -1;
        }
    }

    public static function callJobPaypalOrderCapture($orderId){

        $request = new OrdersCaptureRequest($orderId);
        $request->prefer('return=representation');

        try {
            // Call API with your client and get a response for your call
            $response = self::client()->execute($request);

            // If call returns body in response, you can get the deserialized version from the result attribute of the respons
            Log::info(print_r($response, true));

            if($response->statusCode == 201){

                $result = $response->result;
                if(isset($result)){

                    // one link down
                    $captures = [];
                    foreach ($result->purchase_units as $purchase_unit){
                        $payments = $purchase_unit->payments;
                        foreach ($payments->captures as $capture){
                            $captures[] = $capture->id;
                        }

                    }



                    return $captures[0] ?? -1;
                }else{
                    return -1;
                }

            }


        }catch (HttpException $ex) {
            //echo $ex->statusCode;
            Log::error($ex->statusCode, [$ex->getMessage()]);
            return -1;
        }
    }

}
