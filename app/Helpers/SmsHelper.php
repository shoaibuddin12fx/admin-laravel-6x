<?php


namespace App\Helpers;
use GuzzleHttp\Client;

class SmsHelper{

    public function sendsms($contact, $message){

        //$payload = json_encode( $format );

        $client = new Client();
        $url = env('CLICKSEND_SMS_URL');
        $username = env('CLICKSEND_USERNAME');
        $password = env('CLICKSEND_PASSWORD');
        $authkey = "Authorization: Basic ".base64_encode($username.":".$password);

        $format = [
            "messages" => [
                [
                    "source"  => "php",
                    "body" => $message,
                    "to" => $contact
                ]
            ]
        ];

        try{
            $promise = $client->postAsync( $url, [
                'json' =>
                    $format
                ,
                'headers' => [
                    'Authorization' => $authkey,
                    'Content-Type'  => 'application/json',
                    'http_errors' => false
                ],
            ] )->then(function ($response) {
                return json_decode( $response->getBody(), true );
            });
            $promise->wait();
            return true;
        }catch (\Exception $e){
            return false;
        }





    }

}
