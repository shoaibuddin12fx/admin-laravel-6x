<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryTypeCollection;
use App\Models\Category;
use App\Models\CategoryType;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function getCategories(Request $request)
    {
        $categories = new CategoryTypeCollection( CategoryType::all() );
        return self::success('Categories', ['categories' => $categories  ]);
    }

    public function apiAddCategories(Request $request){

        $data = $request->all();
        $categories = $data['categories'];

        foreach ($categories as $category){

            // create type if not created
            $name =
            $type = $category['type'];
            $cat_type = CategoryType::firstOrCreate([
                'name' => $type
            ], [
                'image' => ''
            ]);

            Category::firstOrCreate([
                'document_id' => $category['id']
            ], [
                'name' => isset($category['name']) ? $category['name'] : "",
                'image' => $category['image'],
                'category_type_id' => $cat_type->id
            ]);

        }

        return self::success('Categories Updated');


    }
}
