<?php

namespace App\Http\Controllers\API;

use App\Helpers\Geohash;
use App\Helpers\Helper;
use App\Helpers\PaypalHelper;
use App\Http\Controllers\Controller;
use App\Http\Resources\JobCollection;
use App\Http\Resources\JobResource;
use App\Http\Resources\JobreviewResource;
use App\Http\Resources\WalletCollection;
use App\Http\Resources\WalletResource;
use App\Models\Cash;
use App\Models\Invoice;
use App\Models\Job;
use App\Models\Jobreview;
use App\Models\Jobstatus;
use App\Models\Joburls;
use App\Models\Location;
use App\Models\OtpNumbers;
use App\Models\Settings;
use App\Models\Wallet;
use App\User;
use Carbon\Carbon;
use Dotenv\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class JobController extends Controller
{

    // helper function
    private function updateJobStatus(Job $job, $latitude = null, $longitude = null){
        Jobstatus::create([
            'job_id' => $job->id,
            'receiver_id' => $job->receiver_id,
            'status' => $job->status,
            'latitude' => $latitude,
            'longitude' => $longitude,
        ]);

        $j = Job::where('id', $job->id)->first();
        if($j){
            $j->status = $job->status;
            $j->save();
        }

        // calc amount on status completed


    }

    public function apiAddJobs(Request $request){

        $data = $request->all();
        $jobs = $data['jobs'];

        foreach ($jobs as $job){

            try{
                // create type if not created
                Job::firstOrCreate([
                    'document_id' => $job['id']
                ], $job);

            } catch (\Exception $e){
                Log::error( $e->getLine() . " " .$e->getMessage());
                continue;
            }


        }

        return self::success('Jobs Updated');


    }

    public function nearbyJobs(Request $request){

        $user_id = \auth()->user()->id;
        $user = User::where('id', $user_id)->first();
        $data = $request->all();
//        $geohash = new Geohash();

        $location = $user->location;
        $latitude = isset($data['latitude']) ? $data['latitude'] : $location->latitude;
        $longitude = isset($data['longitude']) ? $data['longitude'] : $location->longitude;
        $distance = isset($location->distance) ? $location->distance : 25;
//        $bounds = $geohash->bounds($latitude, $longitude, $distance);
//        $bn = $geohash->getBoundsOfDistance($bounds['upper'], $bounds['lower']);


        // see the location filter in future
        $query = Job::query();
        $query->select('*');

//        $query->where('geohash', '<=', $bn['lower']);
//        $query->where('geohash', '>=', $bn['upper']);

        if(isset($data['vehicle_type'])){
            $sizes = $data['vehicle_type']; // collect($data['vehicle_type'])->values()->toArray();
            $query->whereIn('package_size', $sizes);
        }

        if(isset($data['item_category'])){
            $cats = $data['item_category']; // collect($data['vehicle_type'])->values()->toArray();
            $query->whereIn('item_category', $cats);
        }

        if(isset($data['job_price_between'])){
            $start = isset($data['job_price_between'][0]) ? $data['job_price_between'][0] : null;
            $end = isset($data['job_price_between'][1]) ? $data['job_price_between'][1] : null;

            if(isset($start)){
                $query->where('job_price', '>=', $start);
            }

            if(isset($end)){
                $query->where('job_price', '<=', $end);
            }
        }

        $query->addSelect( DB::raw('((ACOS(SIN( ' . $latitude .' * PI() / 180) * SIN(job_latitude * PI() / 180) + COS(' . $latitude .' * PI() / 180) * COS(job_latitude * PI() / 180) * COS((' . $longitude .' - job_longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) as distance') );
        $query->having('distance', '<=', $distance);
        $jobs = $query // ->where('sender_id', '!=', $user_id)
            ->where('status', 'Pending')
            ->orderBy('distance', 'asc')
            ->get();

        $jobs = new JobCollection($jobs);

        return self::success("near by job", ['jobs' => $jobs]);

    }

    public function deliveryJobs(Request $request){

        $user_id = \auth()->user()->id;
        $data = $request->all();

        $validator = \Validator::make($request->all(), [
            'deliverer_id' => 'required',
//            'status' => ['required' => 'array'],
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        $jobs = Job::where('receiver_id', $data['deliverer_id'])->where(function ($q) use ($data){
            if(isset($data['status'])){
                $q->whereIn('status', $data['status']);
            }else{
                $q->where('status', '!=', 'Pending')->where('status', '!=', 'Delivered');
            }


        })->get();
        $jobs = new JobCollection($jobs);

        return self::success("Delivery jobs", ['jobs' => $jobs]);

    }

    public function addJob(Request $request, Helper $helper){

        $user_id = \auth()->user()->id;
        $data = $request->all();

        $validator = \Validator::make($request->all(), [
            'job_latitude' => 'required',
            'job_longitude' => 'required',
            'job_address' => "required",
            'delivery_address' => "required",
            'job_price' => "required",
            'priority' => "required",
            'item_category' => "required",
            'package_size' => "required",
            'source_address_appartment' => "required",
            'delivery_address_appartment' => "required",
        ], [
            "job_latitude" => "Job Source Coordinates missing",
            "job_longitude" => "Job Source Coordinates missing",
            "delivery_latitude" => "Job Destination Coordinates missing",
            "delivery_longitude" => "Job Destination Coordinates missing",
            "priority" => "Job Delivery Type required",
            'package_size' => "Delivery Vehicle required",
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        $data['status'] = config('enums.job.status.PENDING');
        $job = new Job($data);
        $job->security_code = Helper::incrementalHash(6);
        $job->save();

        // update job status in table
        $this->updateJobStatus($job);

        // save photos of job


        $photos_urls = isset($data['photos_urls']) ? $data['photos_urls'] : [];

        foreach ($photos_urls as $url){
            Joburls::create([
               'owner_id' => $user_id,
               'job_id' => $job->id,
               'firebase_url' => $url
            ]);
        }

        $user_id = \auth()->user()->id;
        $receiver_id = null;
        $otpnumber = OtpNumbers::where('phone_number', $data['receiver_contact'] )->first();
        if($otpnumber){
            $otp_number_user_id = $otpnumber->user_id;
            if(isset($otp_number_user_id)){
                $receiver_id = $otp_number_user_id;
            }
        }else{

            $hash = $helper::generateOtp();
            OtpNumbers::create(['phone_number' => $data['receiver_contact'], 'is_verified' => 0, 'otp_code' => $hash, 'expires' => Carbon::now()->addMinutes(5) ]);

        }

        $job->update([
            'sender_id' => $user_id,
            'receiver_id' => $receiver_id
        ]);

        return self::success("New Job Created", [ 'jobId' => $job->id ] );

    }

    public function postedJobs(Request $request){

        $data = $request->all();

        $validator = \Validator::make($request->all(), [
            'jobtype' => ['required' => 'in:active,complete'],
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };



        $user_id = \auth()->user()->id;
        $offset = $request->offset ? $request->offset : 0;
        $query = Job::query();

        if($request->jobtype == 'complete'){
            $query->where('status', config('enums.job.status.DELIVERED'));
        }else{
            $query->where('status', '!=', config('enums.job.status.DELIVERED'));
        }
        $query->where('sender_id', $user_id );
        $jobs = $query->orderBy('created_at', 'DESC' , SORT_NATURAL|SORT_FLAG_CASE)
        ->offset($offset)
        ->limit(10)
        ->get();

        $jobs = new JobCollection($jobs);

        return self::success("Posted Jobs", ['jobs' => $jobs]);
    }

    public function updateJob(Request $request){

        $user_id = \auth()->user()->id;
        $data = $request->all();

        $validator = \Validator::make($request->all(), [
            'job_id' => 'required|integer',
            'status' => ['required' => 'in:Pending,Started,Accepted,Delivered,Received,Completed'],
            'latitude' => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'longitude' => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/']
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        $job = Job::where('id', $request->job_id)->first();
        if(!$job){
            return self::failure("Invalid Job");
        }

        $job->update([
            'status' => $request->status,
        ]);

        // update job status in table
        $this->updateJobStatus($job, $data["latitude"], $data["longitude"]);

        $job = Job::where('id', $request->job_id)->first();
        $job = new JobResource($job);

        return self::success("Job Updated", [ 'job' => $job ] );

    }

    public function verifyCompletedJob(Request $request){

        $user_id = \auth()->user()->id;
        $data = $request->all();

        $validator = \Validator::make($request->all(), [
            'job_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

    }

    public function deleteJob(Request $request)
    {
        $user_id = \auth()->user()->id;
        $data = $request->all();

        $validator = \Validator::make($request->all(), [
            'job_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        $job = Job::where('id', $request->job_id)->first();
        if(!$job){
            return self::failure("Invalid Job");
        }

        // update job status in table
        $job->status = config('enums.job.status.DELETED');
        $this->updateJobStatus($job);
        $job->delete();

        return self::success("Job Deleted");
    }

    public function trackJob(Request $request, $id){

        $user_id = \auth()->user()->id;
        $query = Job::query();
        $job = $query->where('id', $id )->first();
        $job = new JobResource($job);

        return self::success("Posted Job", ['job' => $job]);
    }

    public function getPayableAmount(Request $request){

        $user_id = \auth()->user()->id;
        $data = $request->all();

        $validator = \Validator::make($request->all(), [
            'distance' => 'required'
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        $settings = Settings::first();
        $distance = $data['distance'];

//        if($settings){
//            $milePerMeter = $settings->;
//            $mpg = config('enums.payment.mpg');
//            $gallon = config('enums.payment.gallon');
//            $minimumCharge = config('enums.payment.minimumCharge');
//            $tax = config('enums.payment.tax');
//            $serviceCharge = config('enums.payment.serviceCharge');
//        }else{
            $milePerMeter = config('enums.payment.milePerMeter');
            $mpg = config('enums.payment.mpg');
            $gallon = config('enums.payment.gallon');
            $minimumCharge = config('enums.payment.minimumCharge');
            $tax = config('enums.payment.tax');
            $serviceCharge = config('enums.payment.serviceCharge');
//        }




        $distanceMiles = $distance * $milePerMeter;
        $amount = ($distanceMiles / $mpg) * $gallon;

        if($amount < $minimumCharge) {
            $amount = $minimumCharge;
        }
        $tax_amount = $amount * $tax;
        $service_amount = $amount * $serviceCharge;

        $result = [
            'amount' => $amount,
            'tax' => $tax,
            'service_charge' => $service_amount,
            'total' => $amount + $tax + $service_amount
        ];

        return self::success("Payable Amount", $result);


    }

    public function getPaymentMethods(Request $request){



    }

    public function acceptJob(Request $request){

        $user_id = \auth()->user()->id;
        $data = $request->all();

        $validator = \Validator::make($request->all(), [
            'job_id' => 'required'
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        $jobId = $data['job_id'];
        $job = Job::where(['id' => $jobId])->first();
        if(!$job){
            return self::failure("Invalid Job Selected");
        }

        $job->update([
            'receiver_id' => $user_id,
            'status' => config('enums.job.status.ACCEPTED')
        ]);

        $this->updateJobStatus($job);
        $job = new JobResource($job);
        return self::success("Accepted Job", ['job' => $job]);



    }

    public function ratingsFromDriver(Request $request){

        $user_id = \auth()->user()->id;
        $data = $request->all();

        $validator = \Validator::make($request->all(), [
            'job_id' => 'required',
            'rating' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        $jobId = $data['job_id'];
        $job = Job::where(['id' => $jobId])->first();
        if(!$job){
            return self::failure("Invalid Job Selected");
        }

        $review = new Jobreview([
            'job_id' => $job->id,
            'reviewer_id' => $user_id,
            'sender_id' => $job->sender_id,
            'rating' => $data['rating'],
            'review' => isset($data['review']) ? $data['review'] : null,
            'client_reviewed' => isset($data['client_reviewed']) ? $data['client_reviewed'] : false,
            'driver_reviewed' => isset($data['driver_reviewed']) ? $data['driver_reviewed'] : false,
        ]);

        $review->save();
        $review = new JobreviewResource($review);
        return self::success("Added Review" , ['review' => $review]);

    }

    public function getWalletCashout(Request $request){

        $user_id = \auth()->user()->id;
        $data = $request->all();
        $source = 'paypal';

        $wallets = Wallet::where(['user_id' => $user_id, 'is_paid' => 0 ]);
        $total = $wallets->sum('amount');

        if( $total == 0 ){
            return self::failure("No Amount to Cashout" , [ 'total' => $total]);
        }

        $wallets->update([
            'is_paid' => 1,
            'paid_date' => Carbon::now()
        ]);

        $cashout = new Cash([
           'user_id' => $user_id,
           'cashout' => $total,
           'source' => $source
        ]);

        $cashout->save();



        return self::success("Added Amount" , [ 'total' => $total, 'cashout' => $cashout ]);

    }

    public function getWalletRecords(Request $request){

        $user_id = \auth()->user()->id;
        $data = $request->all();

        $validator = \Validator::make($request->all(), [
            'wallet_type' => ['required' => 'in:all,active,paid'],
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        $query = Wallet::query();
        $query->where('user_id', $user_id);

        if($data['wallet_type'] == "active" ){
            $query->where('is_paid', 0);
        }

        if($data['wallet_type'] == "paid" ){
            $query->where('is_paid', 1);
        }

        $wallets = $query->get();
        $total = $wallets->sum('amount');

        $wallets = new WalletCollection($wallets);
        return self::success("Added Amount" , [ 'total' => $total, 'wallets' => $wallets]);

    }

    public function setAmountInWallet(Request $request){

        $user_id = \auth()->user()->id;
        $data = $request->all();

        $validator = \Validator::make($request->all(), [
            'job_id' => 'required',
            'amount' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };
        $jobId = $data['job_id'];
        $job = Job::where(['id' => $jobId])->first();
        if(!$job){
            return self::failure("Invalid Job Selected");
        }

        // comment for testing
//        $walletExist = Wallet::where(['user_id' => $user_id, 'job_id' => $jobId])->count();
//        if($walletExist){
//            return self::failure("Job Amount Exist in Wallet ");
//        }

        $paypal = new PaypalHelper();
        $order_id = $paypal::callJobPaypalOrderCreate($jobId, $data['amount']);

        if($order_id == -1){
            return self::failure("Failed to create order" , ['order_id' => $order_id]);
        }

        $wallet = new Wallet([
            'user_id' => $user_id,
            'job_id' => $job->id,
            'amount' => $data['amount'],
            'order_id' => $order_id
        ]);
        $wallet->save();

        $wallet = new WalletResource($wallet);

        return self::success("Added Amount" , ['wallet' => $wallet]);

    }

    public function createPaypalCapture(Request $request){

        $user = \auth()->user();
        $user_id = $user->id;
        $data = $request->all();

        $validator = \Validator::make($request->all(), [
            'order_id' => 'required',
            'job_id' => 'required',
            'amount' => 'required'
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        $paypal = new PaypalHelper();
        $capture_id = $paypal::callJobPaypalOrderCapture($data['order_id']);

        $invoice = new Invoice([
           'user_id' => $user_id,
           'job_id' => $data['job_id'],
           'amount' => $data['amount'],
           'order_id' => $data['order_id'],
           'capture_id' => $capture_id
        ]);
        $invoice->save();

        return self::success("Payment Captured" , ['invoice' => $invoice, 'orderCaptured' => true, 'capture_id' => $capture_id]);





    }

    public function findNearByDrivers(Request $request){

        // get the job id and find nearby users with driver flag
        $user = \auth()->user();
        $user_id = $user->id;
        $data = $request->all();

        $validator = \Validator::make($request->all(), [
            'job_id' => 'required',
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };
        $jobId = $data['job_id'];
        $job = Job::where(['id' => $jobId])->first();
        if(!$job){
            return self::failure("Invalid Job Selected");
        }

        // find lat long of job
        $latitude = $job->job_latitude;
        $longitude = $job->job_longitude;

        $location = $user->location;
        $distance = isset($location->distance) ? $location->distance : 25;

        $query = Location::query();
        $query->select('*');

        $query->addSelect( DB::raw('((ACOS(SIN( ' . $latitude .' * PI() / 180) * SIN(latitude * PI() / 180) + COS(' . $latitude .' * PI() / 180) * COS(latitude * PI() / 180) * COS((' . $longitude .' - longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) as distance') );
        $query->having('distance', '<=', $distance);
        $locations = $query->where('user_id', '!=', $user_id)
            ->orderBy('distance', 'asc')
            ->get();

        return self::success("Found Locations" , ['locations' => $locations]);






    }












}
