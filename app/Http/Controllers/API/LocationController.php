<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Job;
use App\Models\Location;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LocationController extends Controller
{

    public function apiAddLocations(Request $request){

        $data = $request->all();
        $locations = $data['locations'];

        foreach ($locations as $location){

            try{

                $user_id = isset($location['user_id']) ? $location['user_id'] : 0;
                $user = User::where('document_id', $user_id)->first();
                if($user){
                    $user_id = $user->id;
                }

                $location['user_id'] = $user_id;

                // create type if not created
                Location::firstOrCreate([
                    'document_id' => $location['id']
                ], $location);

            } catch (\Exception $e){
                Log::error( $e->getLine() . " " .$e->getMessage());
                continue;
            }


        }

        return self::success('Location Updated');


    }

    public function apiCurrentUserLocation(Request $request){

        $data = $request->all();

        $user_id = \Auth()->user()->id;
        Location::updateOrCreate([
            'user_id' => $user_id
        ], $data);

        return self::success('Location Updated', $data);



    }







}
