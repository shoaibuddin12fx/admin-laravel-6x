<?php

namespace App\Http\Controllers\API;

use App\Helpers\Geohash;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use App\Models\Category;
use App\Models\Condition;
use App\Models\Favorite;
use App\Models\Job;
use App\Models\Photourl;
use App\Models\Product;
use App\Models\VehicleSize;
use App\S3ImageHelper;
use App\User;
use Carbon\Exceptions\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{

    public function apiAddProduct(Request $request, Geohash $geohash){

//        'name' => isset($product['name']) ? $product['name'] : "",
//                    'price' => isset($product['price']) ? $product['price'] : 0,


//                    'category_id' => $category_id,
//                    'description' => isset($product['description']) ? $product['description'] : "",
//                    'extra_labels' => isset($product['extra_labels']) ? json_encode( $product['extra_labels'] ) : "",
//                    'geohash' => isset($product['geohash']) ? $product['geohash'] : "",
//                    'insured' => isset($product['insured']) ? $product['insured'] : "",
//                    'latitude' => isset($product['latitude']) ? $product['latitude'] : "",
//                    'longitude' => isset($product['longitude']) ? $product['longitude'] : "",

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'price' => 'required|integer',
            'category_id' => 'required|integer',
            'description' => 'required|string|min:10',
            'latitude' => 'required',
            'longitude' => 'required',
            'photos_urls' => 'required|array|min:1'

        ]);

        if ($validator->fails())
        {
            return self::failure($validator->errors()->first());
        }

        $product = $request->all();

        try{
            $user_id = \auth()->user()->id;
            $created_by = isset($product['created_by']) ? $product['created_by'] : $user_id;
            $user = User::where('id', $created_by)->first();
            if($user){
                $created_by = $user->id;
            }

            $category_id = isset($product['category_id']) ? $product['category_id'] : 0;
            $category = Category::where('id', $category_id )->first();
            if($category){
                $category_id = $category->id;
            }

            // calculate geohash
            $latitude = isset($product['latitude']) ? $product['latitude'] : 27.775392;
            $longitude = isset($product['longitude']) ? $product['longitude'] : -81.353737;
            $hash = $geohash->encode($latitude, $longitude);


            $createdProduct = new Product([
                'name' => isset($product['name']) ? $product['name'] : "",
                'price' => isset($product['price']) ? $product['price'] : 0,
                'quantities_available' => isset($product['quantities_available']) ? $product['quantities_available'] : 100,
                'added_by' => $created_by,
                'category_id' => $category_id,
                'condition_id' => isset($product['condition_id']) ? $product['condition_id'] : 1,
                'brand' => isset($product['brand']) ? $product['brand'] : "",
                'vehicle_id' => isset($product['vehicle_id']) ? $product['vehicle_id'] : 1,
                'description' => isset($product['description']) ? $product['description'] : "",
                'extra_labels' => isset($product['extra_labels']) ? json_encode( $product['extra_labels'] ) : "",
                'geohash' => $hash,
                'insured' => isset($product['insured']) ? $product['insured'] : 0,
                'latitude' => $latitude,
                'longitude' => $longitude,
            ]);
            $createdProduct->save();

            if(isset($product['photos_urls'])){

                Photourl::where('product_id', $createdProduct->id )->delete();

                $urls = $product['photos_urls'];
                foreach ($urls as $url){

                    Log::debug('url', $url);

                    $owner = User::where('id', $user_id)->first();
                    if($owner){
                        $owner_id = $owner->id;
                    }

                    Photourl::create([
                        'name' => isset($url['name']) ? $url['name'] : "",
                        'type' => isset($url['type']) ? $url['type'] : "",
                        'owner_id' => $owner_id,
                        'product_id' => $createdProduct->id,
                        'firebase_url' => isset($url['firebase_url']) ? $url['firebase_url'] : "",
                    ]);
                }

            }

            return self::success("Product Added", ['product' => $createdProduct ]);


        } catch (\Exception $e){
            Log::error( $e->getLine() . " " .$e->getMessage());
            return self::failure("Add Product Error" . $e->getMessage() );
        }

    }

    public function myItems(Request $request)
    {
        $user_id = \auth()->user()->id;
        $offset = $request->offset ? $request->offset : 0;

        $filters = $request->all();
        $is_favorite = isset($filters['is_favorite']) ? $filters['is_favorite'] : 0;

        $query = Product::query();

        if(isset($filters['search'])){
            $search = $filters['search'];
            $query->where('name', 'LIKE', "%$search%");
            $query->orWhere('price', 'LIKE', "%$search%");
        }

        $query->with('photosUrl')->orderBy('created_at', 'desc');
//        $query->offset($offset);
//        $query->limit(10);
        $products = $query->get();

        $products = new ProductCollection($products);

        $count = count($products);
        $offset_count = ($count > 0) ? $offset + $count : -1;

        return self::success('Product List', ['offset' => $offset_count, 'products' => $products ]);

    }


    public function apiGetAllProducts(Request $request){

        $user_id = \auth()->user()->id;
        $offset = $request->offset ? $request->offset : 0;

        $filters = $request->all();
        $is_favorite = isset($filters['is_favorite']) ? $filters['is_favorite'] : 0;

        $query = Product::query();

        if(isset($filters['search'])){
            $search = $filters['search'];
            $query->where('name', 'LIKE', "%$search%");
            $query->orWhere('price', 'LIKE', "%$search%");
        }

        $query->with('photosUrl')->orderBy('created_at', 'desc');
        $query->offset($offset);
        $query->limit(10);
        $products = $query->get();

        $products = new ProductCollection($products);

        $count = count($products);
        $offset_count = ($count > 0) ? $offset + $count : -1;

        return self::success('Product List', ['offset' => $offset_count, 'products' => $products ]);

    }

    public function apiGetSingleProduct(Request $request, $id){

        $user_id = \auth()->user()->id;
        $query = Product::query();
        $query->with('photosUrl');
        $query->where('id', $id);
        $product = $query->first();

        $product = new ProductResource($product);

        return self::success('Single Product', ['product' => $product ]);


    }

    public function apiAddToFavorite(Request $request){

        $data = $request->all();
        $product_id = $data['product_id'];
        $is_favorite = isset($data['is_favorite']) ? $data['is_favorite'] : 0;

        $user_id = \auth()->user()->id;
//        $user = User::where('id', $user_id)->first();
        $favs = Favorite::where(['user_id' => $user_id, 'product_id' => $product_id ]);

        if($is_favorite == 1){

            if($favs->count() == 0){
                Favorite::create(['user_id' => $user_id, 'product_id' => $product_id ]);
            }

        }

        if($is_favorite == 0){

            if($favs->count() > 0){
                $favs->delete();
            }

        }

        return self::success("Favorite Updated", ['is_favorite' => $is_favorite, 'product_id' => $product_id]);

    }

    public function imageUploadPost(Request $request){

        $data = $request->all();
        $base64 = $data['file'];

        if (isset($base64) )
        {
            $url = S3ImageHelper::saveImage($base64, 'images/products/');
            if($url != null){
                $name = basename($url);
                return self::success("You have successfully upload Image", [ 'url' => $url, 'name' => $name ]  );
            }else{
                return self::failure("Select image first.");
            }
        }else{
            return self::failure("Select image first.");
        }

    }

    public function getConditions(Request $request){

        $conditions = Condition::all();
        return self::success("Conditions", ['conditions' => $conditions]);

    }

    public function getVehicleSizes(Request $request){

        $sizes = VehicleSize::all();
        return self::success("Conditions", ['sizes' => $sizes]);
    }

    public function orderProduct(Request $request){

        $user_id = \auth()->user()->id;

        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'buyer_latitude' => 'required',
            'buyer_longitude' => 'required',
            'buyer_address' => 'required',
            'delivery_charge' => 'required'
        ]);

        if ($validator->fails())
        {
            return self::failure($validator->errors()->first());
        }

        $data = $request->all();

        $product = Product::where('id', $data['product_id'])->first();
        if( !$product ){
            return self::success('Invalid Id, Product does not exist');
        }

        $job = new Job([
                    'delivery_latitude' =>  $data['buyer_latitude'],
                    'delivery_longitude' => $data['buyer_longitude'],
                    'delivery_address' => $data['buyer_address'],
                    'job_latitude' => $product->latitude,
                    'job_longitude' => $product->longitude,
                    'job_address' => '',
                    'expected_delivery_time' => Carbon::now(),
                    'package_size' => 'Sedan',
                    'priority' => 'Immediate',
                    'receiver_instructions' => '',
                    'item_category' => $product->category_id,
                    'job_price' => $data['delivery_charge'],
                    'description' => '',
                    'poster_id' => $user_id
        ]);

        $job->save();



        return self::success('Order Placed Successfully', $job->toArray());


    }



}
