<?php

namespace App\Http\Controllers\API;

use App\Helpers\FirebaseHelper;
use App\Helpers\Helper;
use App\Helpers\SmsHelper;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Mail\MailController;
use App\Http\Resources\UserResource;
use App\Models\OtpNumbers;
use App\Models\RuntimeRole;
use App\Models\Vehicle;
use App\Notifications\UserNotifications;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use League\OAuth2\Server\ResourceServer;
use Illuminate\Support\Facades\Log;


class UserController extends Controller
{

    public function __construct(ResourceServer $server)
    {
        $this->server = $server;
    }

    public function getUserFromHeader($request)
    {
        $psr = (new DiactorosFactory)->createRequest($request);

        try {
            $psr = $this->server->validateAuthenticatedRequest($psr);
        } catch (OAuthServerException $e) {
            throw new AuthenticationException;
        }

        $user = User::find($psr->getAttribute('oauth_user_id'));

        return $user;
    }

    public function apiAddUsers(Request $request){

        $data = $request->all();
        $users = $data['users'];

        foreach ($users as $user){

            try{



                $createdUser = User::firstOrCreate([
                    'document_id' => $user['id']
                ], [
                    'email' => isset($user['email']['address']) ? $user['email']['address'] : "",
                    'first_name' => isset($user['first_name']) ? $user['first_name'] : "",
                    'last_name' => isset($user['last_name']) ? $user['last_name'] : "",
                    'password' => isset($user['password']) ? $user['password'] : "",
                    'city' => isset($user['city']) ? $user['city'] : "",
                    'state' => isset($user['state']) ? $user['state'] : "",
                    'street' => isset($user['street']) ? $user['street'] : "",
                    'unit' => isset($user['unit']) ? $user['unit'] : "",
                    'zip' => isset($user['zip']) ? $user['zip'] : "",
                    'device_token' => isset($user['device_token']) ? $user['device_token'] : "",
                    'contact' => isset($user['contact']['number']) ? $user['contact']['number'] : "",
                ]);

                // assignRole
                $user['role'] = isset($user['role']) ? $user['role'] : "User";
                $createdUser->assignRole($user['role']);

            } catch (\Exception $e){
                continue;
            }

        }

        return self::success('Users Updated');


    }

    public function getProfile(Request $request){

        $user = \Auth()->user();
        if($user){
            $profile = User::where('id', $user->id)->first();
            if($profile){
                $profile = new UserResource($profile);
                return self::success("User Profile", ['profile' => $profile ] );
            }else{
                return self::failure("No User(s) Exist" );
            }

        }else{
            return self::failure("No User(s) Exist" );
        }

    }



    public function isVehicleVerified(Request $request){
        $user = \Auth()->user();
        if($user){
            $vehicle = Vehicle::where('user_id', $user->id)->first();
            if($vehicle){
                return self::success("User Vehicle", ['vehicle' => $vehicle ] );
            }else{
                return self::failure("No Vehicle(s) Exist" );
            }

        }else{
            return self::failure("No Vehicle(s) Exist" );
        }
    }

    public function updateProfile(Request $request){
        $user = \Auth()->user();
        $data = $request->all();
        if($user){
            $profile = User::where('id', $user->id)->first();
            if($profile){
                $profile->update($data);
                $profile = User::where('id', $user->id)->first();
                $profile = new UserResource($profile);
                return self::success("User Profile", ['profile' => $profile ] );
            }else{
                return self::failure("No User(s) Exist" );
            }

        }else{
            return self::failure("No User(s) Exist" );
        }
    }

    public function registerDeviceToken(Request $request){

        $user = \Auth()->user();
        $data = $request->all();
        $device_token = $data['device_token'];
        if($user){
            $profile = User::where('id', $user->id)->first();
            if($profile){

                $profile->update([
                    'device_token' => $device_token
                ]);

                $profile = new UserResource($profile);
                return self::success("User Profile", ['profile' => $profile ] );
            }else{
                return self::failure("No User(s) Exist" );
            }

        }else{
            return self::failure("No User(s) Exist" );
        }

    }

    public function changeRole(Request $request){

        $user = \Auth()->user();
        $user_id = $user->id;

        $data = $request->all();
        $role = $data['role'];

        RuntimeRole::create([
            'user_id' => $user_id,
            'role_id' => 0,
            'role_name' => $role
        ]);
        return self::success("Role Changed", ['role' => $role ] );
    }

    public function deleteAccount(Request $request){

        $user = \Auth()->user();
        $user_id = $user->id;
        $email = $user->email;
        $phone_number = $user->email;



        $query = OtpNumbers::query();
        $query->where('user_id', $user_id);

        if(isset($email)){
            $query->orWhere('email', $email);
        }

        if(isset($phone_number)){
            $query->orWhere('phone_number', $phone_number);
        }

        $query->delete();

        $user = User::where('id', $user_id)->first();
        $user->email = '(deleted-'. $user_id . '-)'.$user->email;
        $user->contact = null;
        $user->save();
        $user->delete();


        return self::success("Account Deleted", ['user_id' => $user_id ] );

    }

    public function consumerProfile(Request $request, $id){

        $user = \Auth()->user();
        $data = $request->all();
        $device_token = $data['device_token'];
        if($user){
            $profile = User::where('id', $id)->first();
            if($profile){

                $profile->update([
                    'device_token' => $device_token
                ]);

                $profile = new UserResource($profile);
                $rating = 2.5;
                $total = 100;

                return self::success("User Profile", ['profile' => $profile, 'first_name' => $profile->first_name , 'last_name' => $profile->last_name , 'rating' => $rating, 'total' => $total ] );
            }else{
                return self::failure("No User(s) Exist" );
            }

        }else{
            return self::failure("No User(s) Exist" );
        }
    }





    public function sendOTP(Request $request, SmsHelper $smsHelper, Helper $helper){

        $data = $request->all();
        $contact = isset($data['contact']) ? $data['contact'] : null;
        $forgetpassword = isset($data['forgetpassword']) ? $data['forgetpassword'] : true;
        $email = isset($data['email']) ? $data['email'] : null;
        $hash = $helper::generateOtp();
        $user = null;
        if( str_replace("Bearer ", "", $request->header('Authorization') ) != "null"){
            Log::info('User failed to login.', ['str' => str_replace("Bearer ", "", $request->header('Authorization') ), 'auth' => $request->header('Authorization')]);
            $user = auth('api')->user();
        }

        $user_id = isset($user) ? $user->id : null;



        if(!$forgetpassword){
            if(!$contact){
                return self::failure("Contact Number required");
            }else if($helper::validatePhoneNumber($contact) == false){
                return self::failure("Contact Number format incorrect");
            }

             $otpnumber = $user_id ? OtpNumbers::where(['user_id' => $user_id])->first() :  OtpNumbers::where(['phone_number' => $contact ])->first();

            if($otpnumber){


                if( $otpnumber->is_verified == 1) {
                    return self::failure("Phone Number Already verified");
                } else {
                    $otpnumber->update(['phone_number' => $contact, 'is_verified' => 0, 'otp_code' => $hash, 'user_id' => $user_id, 'expires' => Carbon::now()->addMinutes(5) ]);
                    $response = $smsHelper->sendsms($contact, "Use the following $hash to verify phone number");
//                    $response = true;
                    if(!$response){
                        return self::failure("OTP Sent Failed");
                    }else{
                        return self::success("OTP sent - use the code in app to verify", ['otp' => $otpnumber->otp_code ]);
                    }

                }

            }else{

                $otpnumber = new OtpNumbers(['phone_number' => $contact, 'is_verified' => 0, 'otp_code' => $hash, 'user_id' => $user_id, 'expires' => Carbon::now()->addMinutes(5) ]);
                $otpnumber->save();
//                  $response = $smsHelper->sendsms($contact, "Use the following $hash to verify phone number");
                return self::success("OTP sent - use the code in app to verify", ['otp' => $otpnumber->otp_code ]);

            }

        }else{

            $user = $user_id ? User::where(['id' => $user_id])->first() : User::where(['email' => $email])->first();

            if(!$user){
                return self::failure("Email Doesn't Exist In system");
            }else{

                $user_id = $user->id;
                $otpnumber = $user_id ? OtpNumbers::where(['user_id' => $user_id])->first() :  OtpNumbers::where(['email' => $email ])->first();

                if($otpnumber) {

                    $otpnumber->update(['email' => $email, 'otp_code' => $hash, 'user_id' => $user_id, 'expires' => Carbon::now()->addMinutes(5) ]);
                    $mainCtrl = new MailController();
                    $response = $mainCtrl->resetpassword_email($email, $hash);
                        $nuser = User::where('email', $email)->first();
//                    $response = $nuser->notify(new UserNotifications());
                    if(!$response){
                        return self::failure("Email Sent Failed - P");
                    }else{
                        return self::success("Email sent - use the code in app to verify - P");
                    }

                }else{

                    OtpNumbers::create(['email' => $email, 'otp_code' => $hash, 'user_id' => $user_id, 'expires' => Carbon::now()->addMinutes(5) ]);
                    $mainCtrl = new MailController();
                    $response = $mainCtrl->resetpassword_email($email, $hash);
                    if(!$response){
                        return self::failure("Email Sent Failed - Y");
                    }else{
                        return self::success("Email sent - use the code in app to verify - Y");
                    }

                }





            }






        }

        // extract phone number from request






    }

    public function updatePassword(Request $request){

        $data = $request->all();
        $newpassword = isset($data['new_password']) ? $data['new_password'] : null;

        if(!$newpassword){
            return self::failure("Password required");
        }

        $user = \auth()->user();
        $user->update([
            'password' => bcrypt($newpassword)
        ]);

        return self::success("Password Updated");

    }



    public function verifyOTP(Request $request){

        $data = $request->all();

        $validator = \Validator::make($request->all(), [
            'otp' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        $otp = isset($data['otp']) ? $data['otp'] : null;
        $forgetpassword = isset($data['forgetpassword']) ? $data['forgetpassword'] : null;


        if(!$otp){
            return self::failure("OTP required");
        }else{

            $otpnumber = OtpNumbers::where(['otp_code' => $otp ])->first();

            if(!$forgetpassword){

                if($otpnumber){

                    if( $otpnumber->is_verified == 1 ) {

                        $otpnumber->expires = null;
                        $otpnumber->otp_code = null;
                        $otpnumber->save();

                        return self::failure("OTP Expires");

                    }else{

                        $otpnumber->is_verified = 1;
                        $otpnumber->expires = null;
                        $otpnumber->otp_code = null;
                        $otpnumber->save();

                        return self::success("OTP Verified");


                    }


                }else{
                    return self::failure("OTP incorrect");
                }


            }else{

                if($otpnumber){

                        $otpnumber->is_verified = 1;
                        $otpnumber->expires = null;
                        $otpnumber->otp_code = null;
                        $otpnumber->save();
                        $user = User::where('email', $otpnumber->email)->first();
                        if($user){
                            $token = $user->createToken('Laravel Password Grant Client')->accessToken;
                            return self::success("OTP Verified", ['token' => $token]);
                        }else{
                            return self::failure("Invalid OTP");
                        }
                }else{
                    return self::failure("OTP incorrect");
                }
            }
        }
    }

    public function uploadDriverLicence(Request $request){

        $data = $request->all();
        $user_id = auth()->user()->id;

        $validator = \Validator::make($request->all(), [
            'registration_number' => 'required',
            'vehicle_type' => 'required',
            'driver_licence' => 'required',
            'is_verified' => 'required'
        ]);

        if ($validator->fails()) {
            return self::failure($validator->errors()->first());
        };

        $vehicle = new Vehicle([
            'user_id' => $user_id,
            'registration_number' => $data['registration_number'],
            'vehicle_type' => $data['vehicle_type'],
            'driver_licence' => $data['driver_licence'],
            'is_verified' => $data['is_verified']
        ]);
        $vehicle->save();

        return self::success("Vehicle Updated", [ 'vehicle' => $vehicle ] );

    }










}
