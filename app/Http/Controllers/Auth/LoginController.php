<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Console\Input\Input;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    // Check that function which is obviously not working :)
    public function setlogin(Request $req){



        $req->session()->post('data', $req->input());

        if($req->session()->has('data')
        )
        {
            return redirect('admin');
        }


    }

    public function login(Request $request){

        $data = $request->input();
        $rules = array(
            'email' => 'required|email', // make sure the email is an actual email
            'password' => 'required|min:8'
        );
      // password has to be greater than 3 characters and can only be alphanumeric and);
      // checking all field
      $validator = Validator::make($data , $rules);
      // if the validator fails, redirect back to the form
      if ($validator->fails())
      {
          return Redirect::to('login')->withErrors($validator) // send back all errors to the login form
          ->withInput($request->except('password')); // send back the input (not the password) so that we can repopulate the form
      }
      else
      {
          // create our user data for the authentication
          $userdata = array(
              'email' => $data['email'] ,
              'password' => $data['password']
          );
          // attempt to do the login
          if (\Auth::attempt($userdata))
          {
              $user = \Auth()->user();
              if($user->roles->isNotEmpty() && $user->role->name == "SuperAdmin"){
                  return Redirect::to('home');
              }
              else{
                  Auth::logout();
                  return redirect('/login');
              }
          }
          else
          {
              // validation not successful, send back to form
              Auth::logout();
              return Redirect::to('login');
          }
      }

    }

    public function logout() {
        Auth::logout();
        return redirect('/login');
    }
}
