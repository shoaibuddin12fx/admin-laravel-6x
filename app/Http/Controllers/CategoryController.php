<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\CategoryType;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumb = ['Categories'];
        $categories = Category::all();
        return view('admin.categories.index', compact('categories', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumb = ['Categories', 'Create'];
        $category_types = CategoryType::all();
        return view('categories.create', compact('breadcrumb', 'category_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name' => 'required'
        ]);

        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('uploads/categorie'), $imageName);

        $category = new Category(); 
        $category->name = $request->name;        
        $category->category_type_id = $request->category_type;
        $category->image = $imageName;
        $category->save();
        return back()->with('success','Succesfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $breadcrumb = ['Categories', 'Edit'];
        $category_types = CategoryType::all();
        return view('categories.edit', compact('breadcrumb', 'category', 'category_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        request()->validate([
            'name' => 'required'
        ]);

        $category = Category::findOrFail($id);

        if($request->image){
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('uploads/category'), $imageName);
            $category->image = $imageName;
        }
            
        $category->name = $request->name;
        $category->category_type_id = $request->category_type;
        $category->save();
        return back()->with('success','Succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::destroy($id);
        return back()->with('success','Succesfully');
    }
}
