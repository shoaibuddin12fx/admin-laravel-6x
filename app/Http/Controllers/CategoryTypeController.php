<?php

namespace App\Http\Controllers;

use App\Models\CategoryType;
use Illuminate\Http\Request;

class CategoryTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumb = ['Category Types'];
        $category_types = CategoryType::all();
        return view('admin.category_types.index', compact('category_types', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumb = ['Category Types', 'Create'];
        return view('category_types.create', compact('breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name' => 'required'
        ]);

        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('uploads/category_type'), $imageName);

        $categoryType = new CategoryType(); 
        $categoryType->name = $request->name;
        $categoryType->image = $imageName;
        $categoryType->save();
        return back()->with('success','Succesfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoryType $category_type)
    {
        $breadcrumb = ['Category Types', 'Edit'];
        return view('category_types.edit', compact('breadcrumb', 'category_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        request()->validate([
            'name' => 'required'
        ]);

        $categoryType = CategoryType::findOrFail($id);

        if($request->image){
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('uploads/category_type'), $imageName);
            $categoryType->image = $imageName;
        }
            
        $categoryType->name = $request->name;    
        $categoryType->save();
        return back()->with('success','Succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CategoryType::destroy($id);
        return back()->with('success','Succesfully');
    }
}
