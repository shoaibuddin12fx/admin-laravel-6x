<?php

namespace App\Http\Controllers;
use App\Http\Resources\InvoiceCollection;
use App\Models\Invoice;
use App\Models\Job;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumb = ['Invoices'];
        $invoices = Invoice::with(['job'])->get();
       
        return view('admin.invoices.index', compact('invoices', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumb = ['Invoices', 'Create'];
        $jobs = Job::all();
        return view('invoices.create', compact('breadcrumb', 'jobs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'amount' => 'required'
        ]);

        $invoice = new Invoice();
        $invoice->amount = $request->amount;
        $invoice->capture_id = $request->capture_id;
        $invoice->job_id = $request->job_id;
        $invoice->document_id = $request->document_id;
        $invoice->timestamp = $request->timestamp;
        $invoice->save();
        return back()->with('success','Succesfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        $breadcrumb = ['Invoices', 'Edit'];
        $jobs = Job::all();
        return view('invoices.edit', compact('breadcrumb', 'jobs', 'invoice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
       
        // request()->validate([
        //     'amount' => 'required'
        // ]);

        $invoice = Invoice::findOrFail($request->id);

        //$invoice->amount = $request->amount;
        $invoice->capture_id = $request->capture_id;
        // $invoice->job_id = $request->job_id;
        $invoice->document_id = $request->document_id;
       // $invoice->timestamp = $request->timestamp;
        $invoice->save();
        return back()->with('msg','Payment updated Succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Invoice::destroy($id);
        return back()->with('success','Succesfully');
    }
    function view(){
        $Invoice = job::all();
        // dd($Invoice);
         $data = new InvoiceCollection( $Invoice );
         $data = $data->toArray(\request());
         $columns = [];

         if(count($data) > 0){
             $columns = array_keys($data[0]);
         }
         $actions = [
            [
                'label' => 'edit',
                'action' => route('admin.users.show', ':id')
            ],
          ];
         $breadcrumb = "Invoices";
         $formaction="admin.jobs.edit";
         return view('admin.Invoice.index', compact('data','formaction', 'columns', 'breadcrumb'));
    }
    function trash($id){
        //return $id;
        job::find($id)->delete();
        return back()->with('msg','Innvoice trashed');

    }
}
