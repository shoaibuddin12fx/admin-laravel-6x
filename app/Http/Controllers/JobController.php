<?php

namespace App\Http\Controllers;

use App\Http\Resources\JobCollection;
use App\Http\Resources\JobreviewCollection;
use App\Http\Resources\JobViewCollection;
use App\Http\Resources\UserCollection;
use App\Models\Job;
use App\Models\Jobreview;
use App\User;
use Illuminate\Http\Request;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = Job::all();
        $data = new JobViewCollection($jobs);
        $data = $data->toArray(\request());
        $columns = [];

        if(count($data) > 0) {
            $columns = array_keys($data[0]);
        }
        $actions = [
            [
                'label' => 'edit',
                'action' => route('admin.users.show', ':id')
            ],
          ];
          $formaction="admin.jobs.edit";
        $breadcrumb = "Order Management";
        return view('admin.users.index', compact('data','formaction', 'actions','columns', 'breadcrumb') );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumb = ['Jobs', 'Create'];
        return view('jobs.create', compact('breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $job = new Job();
        $job->delivery_address = $request->delivery_address;
        $job->delivery_latitude = $request->delivery_latitude;
        $job->delivery_longitude = $request->delivery_longitude;
        $job->description = $request->description;
        $job->expected_delivery_time = $request->expected_delivery_time;
        $job->geohash = $request->geohash;
        $job->item_category = $request->item_category;
        $job->job_address = $request->job_address;
        $job->job_latitude = $request->job_latitude;
        $job->job_price = $request->job_price;
        $job->package_size = $request->package_size;
        $job->posted_at = $request->posted_at;
        $job->posted_id = $request->posted_id;
        $job->priority = $request->priority;
        $job->receiver_instructions = $request->receiver_instructions;
        $job->security_code = $request->security_code;
        $job->status = $request->status;
        $job->document_id = $request->document_id;
        $job->save();
        return back()->with('success','Succesfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function edit(Job $job)
    {
        $breadcrumb = ['Jobs', 'Edit'];
        return view('jobs.edit', compact('breadcrumb', 'job'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request)
    {

        $job = Job::findOrFail($request->id);
          $job->job_price = $request->price;
        // $job->package_size = $request->package_size;
        // $job->posted_at = $request->posted_at;
        // $job->posted_id = $request->posted_id;
        // $job->priority = $request->priority;
        // $job->receiver_instructions = $request->receiver_instructions;
        // $job->security_code = $request->security_code;
        $job->status = $request->status;
       // $job->document_id = $request->document_id;
        $job->created_at=$request->created_on;
        $job->save();
        return back()->with('msg','job updated Succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Job::destroy($id);
        return back()->with('success','Succesfully');
    }



    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        return view('admin.jobs.show', ['job' => Job::with('sender','receiver')->findOrFail($id)]);
    }

    public function changeStatus(Request $request){


        $job = Job::findOrFail($request->job_id);
        $job->status = $request->status;
        $job->save();

        return redirect()->back()->with('message', 'Job status has been change successfully');
    }

    public function getReviews(Request $request){

        $user_id = \auth()->user()->id;

        $jobreview = Jobreview::all();
        $data = new JobreviewCollection( $jobreview );
        $data = $data->toArray(\request());
        $columns = [];

        if(count($data) > 0) {
            $columns = array_keys($data[0]);
        }
        $actions = [
            [
                'label' => 'edit',
                'action' => route('admin.users.show', ':id')
            ],
          ];
          $formaction="admin.users.edit";

        $breadcrumb = "Reviews";
        return view('admin.reviews.index', compact('data','formaction', 'columns', 'breadcrumb') );

    }



}
