<?php

namespace App\Http\Controllers\Mail;

use App\Http\Controllers\Controller;
use App\Mail\NewUserNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    //
    public function resetpassword_email($sentto, $hash) {
        $data = array('name'=>"Virat Gandhi");
        try {
            $email = new \stdClass();
            $email = new \stdClass();
            $email->template = "mail";
            $email->full_name = "A";
            $email->sender = 'App Delivero';
            $email->receiver = $sentto;
            $email->subject = 'App Delivero - Reset Pasword Verification Email';

//            $dyn_template = EmailTemplates::where('assign_to', 'duplicate_verification_email')->first();
//            $email->dynamic_template = EmailTemplatesController::replaceTemplateStringsAndSendmail($dyn_template, $email);
            $email->dynamic_template = "The OTP Code $hash is generated for you to place in app and reset password,  Thank you";


            try {
                Log::info('check email ', ['email' => $sentto]);
                Mail::to($sentto)->send(new NewUserNotification($email));
                return true;
            } catch (\Exception $e) {
                Log::error($e->getMessage());
                return false;
            }
            return true;
        }catch (\Exception $e){
            return false;
        }
    }
    public function html_email($email) {
        $data = array('name'=>"Virat Gandhi");
        try{
            Mail::send('mail', $data, function($message) use ($email) {
                $message->to($email, 'Tutorials Point')->subject
                ('Laravel HTML Testing Mail');
                $message->from('support@deliveryist.com','Virat Gandhi');
            });
            return true;
        }catch (\Exception $e){
            return false;
        }

    }
    public function attachment_email() {
        $data = array('name'=>"Virat Gandhi");
        Mail::send('mail', $data, function($message) {
            $message->to('abc@gmail.com', 'Tutorials Point')->subject
            ('Laravel Testing Mail with Attachment');
            $message->attach('C:\laravel-master\laravel\public\uploads\image.png');
            $message->attach('C:\laravel-master\laravel\public\uploads\test.txt');
            $message->from('xyz@gmail.com','Virat Gandhi');
        });
        echo "Email Sent with attachment. Check your inbox.";
    }
}
