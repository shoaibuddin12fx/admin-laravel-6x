<?php

namespace App\Http\Controllers;

use App\Http\Resources\JobreviewCollection;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductOrdersCollection;
use App\Models\Jobreview;
use App\Models\Orders;
use App\Models\Product;
use App\User;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::with('image','category')->orderBy('id', 'desc')->get();
        // dd($Invoice);
         $data = new ProductCollection( $product );
         $data = $data->toArray(\request());
         $columns = [];
 
         if(count($data) > 0){
             $columns = array_keys($data[0]);
         }
         $actions = [
             [
                 'label' => 'edit',
                 'action' => route('admin.users.show', ':id')
             ],
           ];
         //return "final";
         $formaction="admin.users.edit";
         $breadcrumb = "Products";
         return view('admin.Payments.index', compact('data','formaction', 'actions','columns', 'breadcrumb'));
        // $breadcrumb = ['Products'];
        // $records = Product::with('image','category')->paginate(env('page_count'));
        // return view('admin.products.index', compact('records', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
