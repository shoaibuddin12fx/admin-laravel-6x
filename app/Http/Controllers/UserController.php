<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserViewCollection;
use App\Models\OtpNumbers;
use App\Models\Settings;
use App\User;
use Carbon\Carbon;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $data = new UserViewCollection($users);
        $data = $data->toArray(\request());
        $columns = [];

        if(count($data) > 0) {
            $columns = array_keys($data[0]);
        }

        $actions = [
          [
              'label' => 'edit',
              'action' => route('admin.users.edit', ':id')
          ],
          [
            'label' => 'Send notification',
            'action' => route('admin.users.sendnotification', ':id')
        ],
        ];  
        $formaction="admin.users.edit";
        $breadcrumb = "Users";
        return view('admin.users.index', compact('data','formaction', 'columns', 'actions', 'breadcrumb') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumb = ['Users', 'Create'];
        $users = User::all();
        return view('users.create', compact('breadcrumb', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();
        $user->device_token = $request->device_token;
        $user->geohash = $request->geohash;
        $user->latitude = $request->latitude;
        $user->longitude = $request->longitude;
        $user->role = $request->role;
        $user->document_id = $request->document_id;
        $user->user_id = $request->user;
        $user->save();
        return back()->with('success','Succesfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $breadcrumb = ['Users', 'Edit'];
        $users = User::all();
        return view('users.edit', compact('breadcrumb', 'user', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $user = User::findOrFail($id);

        $user->device_token = $request->device_token;
        $user->geohash = $request->geohash;
        $user->latitude = $request->latitude;
        $user->longitude = $request->longitude;
        $user->role = $request->role;
        $user->document_id = $request->document_id;
        $user->user_id = $request->user;
        $user->save();
        return back()->with('success','Succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return back()->with('success','Succesfully');
    }


    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        return view('admin.users.show', ['user' => User::findOrFail($id)]);
    }


    public function sendOTP(Request $request, Helper $helper){

        $user = User::findOrFail($request->user_id);
        $hash = $helper::generateOtp();
        $otpnumber = OtpNumbers::where(['phone_number' => $user->contact ])->first();

        if($otpnumber){
            $otpnumber->update(['phone_number' => $user->contact, 'is_verified' => 0, 'otp_code' => $hash, 'user_id' => $user->id, 'expires' =>      Carbon::now()->addMinutes(5) ]);

        }else{

            OtpNumbers::create(['phone_number' => $user->contact, 'is_verified' => 0, 'otp_code' => $hash, 'user_id' => $user->id,  'expires' => Carbon::now()->addMinutes(5) ]);
        }

        $user->notify(new \App\Notifications\SendOTPNotification($user,$hash));

        return redirect()->back()->with('message', 'OTP has been sent on user email');
    }

    public function sendResetPassword(Request $request){

        $user = User::findOrFail($request->user_id);

        $user->notify(new \App\Notifications\ResetPasswordNotification($user));

        return redirect()->back()->with('message', 'Reset password email has been sent.');
    }

    public function getSettings(){

        $user_id = \auth()->user()->id;

        return $settings = Settings::updateOrCreate([
            'user_id' => $user_id
        ]);

        if(count($settings)==0) {
            $settings="";
        }
        return view('admin.settings.index', ['user_id' => $user_id, 'settings' => $settings ] );
    }
    public function postSettings(Request $request){
//        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'admin_commission' => 'required',
            'dollar_per_mile' => 'required',
            'min_fare' => 'required',
            'max_fare' => 'required',
            'booking_cancel_time' => 'required',
            'admin_delivery_fee' => 'required',
            'min_fare_distance' => 'required',
        ]);
        if ($validator->fails()) {
            return $data = [
                'message' => 'Error',
                'data' => $validator->getMessageBag()->first(),
            ];
        }
        $user_id = \Auth()->user()->id;
        $settings = [
            "admin_commission" => $request->admin_commission,
            "dollar_per_mile" => $request->dollar_per_mile,
            "min_fare" => $request->min_fare,
            "max_fare" => $request->max_fare,
            "booking_cancel_time" => $request->booking_cancel_time,
            "admin_delivery_fee" => $request->admin_delivery_fee,
            "min_fare_distance" => $request->min_fare_distance,
            "user_id" => $user_id,

        ];

        Settings::updateOrCreate([
            'user_id' => $user_id
        ], $settings);

        $settings = Settings::updateOrCreate([
            'user_id' => $user_id
        ]);

        return view('admin.settings.index', ['user_id' => $user_id, 'settings' => $settings ] );



//print ('dsadasdas');
//        return view('admin.dashboard.index');

    }
    function edituser($id){
       // return $id;
       $user=  User::find($id);
       return view('admin.edit.edituser',compact('user'));
    }
    function updateuser(Request $request){
        $user=User::find($request->id);
        $user->first_name=$request['name'];
      //  $user->last_name=$request['lname'];
        $user->email=$request['email'];
        $user->contact=$request['phone'];
        $user->save();
        return redirect('admin/users')->with('msg','User updated');


    }
}

