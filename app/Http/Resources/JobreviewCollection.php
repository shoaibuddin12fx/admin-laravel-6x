<?php

namespace App\Http\Resources;

use App\Models\Jobreview;
use Illuminate\Http\Resources\Json\ResourceCollection;

class JobreviewCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (Jobreview $jobreview){
            return new JobreviewResource($jobreview);
        });

        return parent::toArray($request);
    }

    public static function toArrayOfObjects($obj)
    {

        $obj->transform(function (Jobreview $jobreview){
            return JobreviewResource::toObject($jobreview);
        });

        return $obj;
    }
}
