<?php

namespace App\Http\Resources;
use App\notification;
use Illuminate\Http\Resources\Json\ResourceCollection;

class Notificationcollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (notification $notification){
            return new NotificationRecource($notification);
        });

        return parent::toArray($request);
    }

    public static function toArrayOfObjects($obj)
    {

        $obj->transform(function (notification $notification){
            return NotificationRecource::toObject($notification);
        });

        return $obj;
    }
}
