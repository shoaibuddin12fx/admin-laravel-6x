<?php

namespace App\Http\Resources;
use App\Models\Wallet;
use App\Models\Job;
use App\Models\jobprice;
use Illuminate\Http\Resources\Json\JsonResource;

class PaymentRecourse extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $obj = self::toObject($this);
        return $obj;
    }
    public static function toObject($obj, $lang = 'en')
    {
        $jobobj = $obj->job;
        $sendername = $jobobj->sender->fullName ?? "";
        $reciever = $jobobj->receiver->fullName ?? "";


        $jobpriceobj=new jobprice();
        $customerprice=$jobpriceobj->costomername($obj->job_id);
        $driver_amount=$jobpriceobj->driver_amount($obj->job_id);
        $admin_amount=$jobpriceobj->admin_amount($obj->job_id);
        $objofwallet=new wallet();
        $paymentstatus=$objofwallet->status($obj->job_id);
        //calc comition
        if($customerprice=="" || $admin_amount=="" ){
            $commition="";
        }else{

            $commition=$admin_amount*100/$customerprice;
        }

        $jobs=job::find($obj->job_id);
        if($jobs==null){
            $package_size="";
        }else{
           // dd($jobs);
            $package_size=$jobs->package_size;
        }
        $jobstatus=job::find($obj->job_id);
        if($jobstatus==null){
            $status="";
        }else{
            $status= $jobstatus->status;
        }

        if(isset($jobobj)){

            if($jobobj->id == 12){
                dd( "12");
            }
         }
        $pickupdate = null;
        if(isset($jobobj)){
            $pickupdate = $jobobj->pickupDate;
        }

         $data = [
             "id" => $obj->id,
            "order_id"=>$obj->order_id,
            "job_id"=>$obj->job_id,
            "customer"=>$sendername,
            "reciever"=>$reciever,
            "pickup_date"=>$pickupdate,
            "customerprice"=>$customerprice,
            "driveramount"=>$driver_amount,
            "admin_amount"=>$admin_amount,
             "commition"=>$commition,
             "paymentstatus"=>$paymentstatus,
              "vehicle_type"=>$package_size,
              "status"=>$status


        ];

        return $data;


    }
}
