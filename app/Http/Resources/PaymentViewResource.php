<?php

namespace App\Http\Resources;

use App\Models\Job;
use App\Models\jobprice;
use App\Models\Wallet;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class PaymentViewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $obj = self::toObject($this);

        return $obj;
    }

    public static function toObject($obj, $lang = 'en')
    {
        $jobobj = $obj->job;
        $sendername = $jobobj->sender->fullName ?? "";
        $reciever = $jobobj->receiver->fullName ?? "";

        $jobprices = $obj->job->jobprices;
        $customerprice = isset($jobprices) ? $jobprices->customer_amount : $obj->job->job_price;
        $driver_amount = isset($jobprices) ? $jobprices->driver_amount : 0;
        $admin_amount = isset($jobprices) ? $jobprices->admin_amount : 0;
        $commission = ($admin_amount/$customerprice)*100;


        $wallet = $obj->job->wallet;
        $paymentstatus = isset($wallet) ? $wallet->is_paid : 0;
        $package_size = $jobobj->package_size;
        $status = $jobobj->status;
        $pickupdate = isset($jobobj->pickupDate);

        $data = [
            "id" => $obj->id,
            "order_id" => $obj->order_id,
            "job_id" => $obj->job_id,
            "customer" => $sendername,
            "reciever" => $reciever,
            "pickup_date" => $pickupdate,
            "customer_price" => $customerprice,
            "driver_amount" => $driver_amount,
            "admin_amount" => $admin_amount,
            "commission" => $commission . '%',
            "payment_status" => $paymentstatus,
            "vehicle_type" => $package_size,
            "status" => $status
        ];

        return $data;
    }
}
