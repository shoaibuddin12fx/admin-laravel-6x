<?php

namespace App\Http\Resources;
use App\Models\Invoice;
use App\Http\Resources\PaymentRecourse;
use Illuminate\Http\Resources\Json\ResourceCollection;

class Paymentcollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $this->collection->transform(function (Invoice $Invoice){
            return new PaymentRecourse($Invoice);
        });

        return parent::toArray($request);
    }

    public static function toArrayOfObjects($obj)
    {

        $obj->transform(function (Vehicle $Invoice){
            return PaymentRecourse::toObject($Invoice);
        });

        return $obj;
    }
}
