<?php

namespace App\Http\Resources;

use App\Models\Photourl;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PhotourlCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (Photourl $photourl){
            return new PhotourlResource($photourl);
        });

        return parent::toArray($request);
    }

    public static function toArrayOfObjects($obj)
    {

        $obj->transform(function (Photourl $photourl){
            return PhotourlResource::toObject($photourl);
        });

        return $obj;
    }
}
