<?php

namespace App\Http\Resources;

use App\Models\Favorite;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $obj = self::toObject($this);

        return $obj;
    }

    public static function toObject($obj, $lang = 'en')
    {
        $fav = 0;
        if(auth()){

            $user_id = auth()->user()->id;
            $product_id = $obj->id;
            if($user_id){
                $fav = Favorite::where(['user_id' => $user_id, 'product_id' => $product_id])->count();
//                $fav = $user->favorites()->where('product_id', $product_id)->count();
            }

        }

        return [
            "id" => $obj->id,
            "name" => $obj->name,
            "category_id" => $obj->category_id,
            "category_name" => $obj->categoryName,
            "condition_id" => $obj->condition_id,
            "condition_name" => $obj->conditionName,
            "vehicle_id" => $obj->vehicle_id,
            "vehicle_name" => $obj->vehicleName,
            "brand" => $obj->brand,
            "description" => $obj->description,
            "extra_labels" => $obj->extra_labels,
            "geohash" => $obj->geohash,
            "insured" => $obj->insured,
            "latitude" => $obj->latitude,
            "longitude" => $obj->longitude,
            "price" => $obj->price,
            "quantities_available" => $obj->quantities_available,
            'photos_urls' => new PhotourlCollection($obj->photosUrl),
            'is_favorite' => $fav,
            'created_at' => $obj->created_at->format('m-d-Y')
        ];
    }
}
