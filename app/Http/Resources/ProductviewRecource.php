<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\models\Product;
class ProductviewRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $obj = self::toObject($this);
//        return parent::toArray($request);
        return $obj;
    }

    public static function toObject($obj, $lang = 'en')
    {

      //  $photo=$obj->photosUrl;
        $photo = isset($obj->photosUrl[0])  ? $obj->photosUrl[0]->firebase_url : "";
        $category=isset($obj->category) ? $obj->category->name : "";

        return [
            "image"=>"<img style='width: 75px; height: 75px' class='attachment-img' src='$photo' alt='Attachment Image'>",
            "category"=>$category,
            "id" => $obj->id,
            "name"=>$obj->name,
            "price"=>$obj->price,
            "quantity"=>$obj->quantities_available,
            "location"=>$obj->latitude,


             
           

        ];
    }
}
