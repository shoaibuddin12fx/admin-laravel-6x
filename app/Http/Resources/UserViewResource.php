<?php

namespace App\Http\Resources;

use App\Models\Vehicle;
use Illuminate\Http\Resources\Json\JsonResource;

class UserViewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $obj = self::toObject($this);
//        return parent::toArray($request);
        return $obj;
    }

    public static function toObject($obj, $lang = 'en')
    {

        $vehicle = Vehicle::where('user_id', \auth()->user()->id)->first();
        $isVehicleVerified = false;
        if($vehicle){
            $isVehicleVerified = $vehicle->is_verified == 1;
        }

        $drivingLicence = isset($obj->vehicle)  ? $obj->vehicle->driver_licence : "";

        return [
            "id" => $obj->id,
            "name" => $obj->fullName,
            "email" => $obj->email,
            "phone" => $obj->contact,
            "ssn" => '',
            "driving_license" => $drivingLicence,
            "status" => $obj->runtimeRole,
            "created_at" => $obj->created_at ? $obj-> created_at->format('m-d-Y') : ''

        ];
    }
}
