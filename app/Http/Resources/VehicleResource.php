<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VehicleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $obj = self::toObject($this);
        return $obj;
    }

    public static function toObject($obj, $lang = 'en')
    {
        $data = [
            "id" => $obj->id,
            "user_id" => $obj->user_id,
            "driver_name" => isset($obj->user) ?  $obj->user->fullName : "",
            "regisration_number" => $obj->registration_number,
            "vehicle_type" => $obj->vehicle_type,
            "driver_licence" => $obj->driver_licence,
            "is_verified" => $obj->is_verified == 1,
            "is_paid" => $obj->is_paid,
           // "action"=>"<a href='deletevehicle/$obj->id' onclick='return confirm('Do you wants to delete')'><i class='fa fa-trash text-danger' aria-hidden='true'></i></a>"
        ];

        return $data;


    }
}
