<?php

namespace App\Http\Resources;

use App\Helpers\Helper;
use App\Http\Controllers\API\ApiServiceController;
use App\Models\Job;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class WalletResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $obj = self::toObject($this);

        return $obj;
    }

    public static function toObject($obj, $lang = 'en')
    {

        $user = User::where('id', $obj->user_id)->first();
        $job = Job::where('id', $obj->job_id)->first();

        return [
            "id" => $obj->id,
            "user_id" => $obj->user_id,
            "driver_name" => isset($user) ?  $user->fullName : "",
            "job_id" => $obj->job_id,
            "job_title" => isset($job) ?  $job->delivery_address : "",
            "amount" => $obj->amount,
            "completed_at" => $obj->created_at,
            "is_paid" => $obj->is_paid,
            "paid_date" => $obj->paid_date,
            "order_id" => $obj->order_id
        ];
    }
}
