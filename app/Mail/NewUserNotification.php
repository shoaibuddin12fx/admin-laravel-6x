<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewUserNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $emailer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($emailer)
    {
        //
        $this->emailer = $emailer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->emailer->subject)
            return $this->subject($this->emailer->subject)->from( env('MAIL_USERNAME', 'appmail@deliveryist.com') )->view("emails.{$this->emailer->template}");
        else
            return $this->from( env('MAIL_USERNAME', 'appmail@deliveryist.com') )->view("emails.{$this->emailer->template}");
    }
}
