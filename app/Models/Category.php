<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $fillable = ['name', 'image', 'category_type_id', 'document_id'];

    public function category_type()
    {
        return $this->belongsTo('App\Models\CategoryType', 'category_type_id', 'id');
    }

    public function getCreatedDateAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y');
    }

    public function getImageAttribute($value)
    {
        $path = 'uploads/category/';
        if($value){
            return url($path . $value);
        }
        return url($path . 'placeholder.png');
    }
}
