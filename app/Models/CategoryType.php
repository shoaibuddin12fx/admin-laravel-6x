<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryType extends Model
{
    //
    protected $table = "category_types";
    protected $fillable = ['name', 'image'];
    // protected $appends = [''];

    public function getCreatedDateAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y');
    }

    public function getImageAttribute($value)
    {
        $path = 'uploads/category_type/';
        if($value){
            return url($path . $value);
        }
        return url($path . 'placeholder.png');
    }
}
