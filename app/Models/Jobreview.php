<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jobreview extends Model
{
    //
    protected $table = "jobreviews";
    protected $fillable = [
        'job_id', 'reviewer_id', 'rating', 'review', 'sender_id', 'client_reviewed', 'driver_reviewed'
    ];
}
