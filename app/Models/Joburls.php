<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Joburls extends Model
{
    //
    protected $table = "joburls";
    protected $fillable = [
        'owner_id', 'job_id', 'firebase_url'
    ];
}
