<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OtpNumbers extends Model
{
    // otp_numbers
    protected $table = "otp_numbers";
    protected $fillable = [
        'phone_number', 'otp_code', 'email', 'is_verified', 'user_id', 'expires'
    ];
}
