<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = "products";
    protected $fillable = [
        'name', 'price', 'quantities_available', 'added_by', 'category_id', 'description', 'extra_labels', 'geohash',
        'insured', 'latitude', 'longitude', 'document_id', 'condition_id', 'brand', 'vehicle_id'
    ];
    // protected $appends = [''];

    public function photosUrl(){
        return $this->hasMany('\App\Models\Photourl', 'product_id', 'id');
    }

    public function category(){
        return $this->belongsTo('App\Models\Category');
    }

    public function image() {
        return $this->hasOne('App\Models\Photourl')->latest();
    }

    public function getCategoryNameAttribute()
    {
        $cat = Category::where('id', $this->attributes['category_id'])->first();
        return $cat ? $cat->name : "";
    }

    public function getConditionNameAttribute()
    {
        $con = Condition::where('id', $this->attributes['condition_id'])->first();
        return $con ? $con->name : "";
    }

    public function getVehicleNameAttribute()
    {
        $veh = VehicleSize::where('id', $this->attributes['vehicle_id'])->first();
        return $veh ? $veh->name : "";
    }

}
