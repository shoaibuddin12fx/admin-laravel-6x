<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RuntimeRole extends Model
{
    //
    protected $table = "runtime_roles";
    protected $fillable = [
        'user_id', 'role_id', 'role_name',
    ];
    // protected $appends = [''];


}
