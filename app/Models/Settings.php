<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    //
    protected $table = "settings";
    protected $fillable = [
        'admin_commission',
        'dollar_per_mile',
        'min_fare',
        'max_fare',
        'booking_cancel_time',
        'admin_delivery_fee',
        'min_fare_distance',
        'user_id'

    ];
}
