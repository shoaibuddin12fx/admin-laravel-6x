<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vehicle extends Model
{
    use SoftDeletes;
    //
    protected $table = "vehicles";
    protected $fillable = [
        'user_id', 'registration_number', 'vehicle_type', 'driver_licence', 'is_verified'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

}
