<?php

namespace App\Providers;

use App\Helpers\Geohash;
use Illuminate\Support\ServiceProvider;

class GeohashProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        //
        $this->app->bind('App\Helpers\Geohash', function ($app) {
            return new Geohash();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
