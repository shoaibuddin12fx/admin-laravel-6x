<?php

namespace App\Providers;

use App\Helpers\PaypalHelper;
use Illuminate\Support\ServiceProvider;

class PaypalProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind('App\Helpers\PaypalHelper', function ($app) {
            return new PaypalHelper();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
