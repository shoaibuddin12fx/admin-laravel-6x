<?php

namespace App\Providers;

use App\Helpers\SmsHelper;
use Illuminate\Support\ServiceProvider;

class SmsApiProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind('App\Helpers\SmsHelper', function ($app) {
            return new SmsHelper();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
