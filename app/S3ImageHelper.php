<?php
namespace App;

use Illuminate\Support\Str;
use Storage;

//https://laravel.com/docs/5.8/filesystem

Class S3ImageHelper {

    public static function saveImage($base64file, $location){

        $imageString = S3ImageHelper::delete_all_between("data:image", "base64,", $base64file);
        $product_image = base64_decode($imageString);
        $imageName = md5(uniqid()) . '.' . 'png';
        $pathAndFileName = $location.$imageName;
        $path = S3ImageHelper::storeImageInBucket($pathAndFileName, $product_image);
        $path = asset('storage/app/'.$path );
        return Str::replaceFirst('/public', '', $path);

    }

    public static function storeImageInBucketWithBase64($pathAndFileName, $image){
        $image = S3ImageHelper::delete_all_between("data:image", "base64,", $image);
        $image = base64_decode($image);
        return S3ImageHelper::storeImageInBucket($pathAndFileName, $image);
    }
    public static function storeImageInBucket($pathAndFileName, $image){

        $path = Storage::disk('local')->put($pathAndFileName, $image);
        if ( $path === true ) {
            $path = $pathAndFileName;
        }
        return $path;
    }

    //For getting image
    public static function getImageFromBucket($pathAndFileName){
        if( stripos($pathAndFileName, 'dynamic/') !== false ){//image is in bucket
            $s3 = \Storage::disk('s3');
            $client  = $s3->getDriver()->getAdapter()->getClient();
            $expiry  = "+10 minutes";
            $command = $client->getCommand('GetObject', [
                        'Bucket' => env('AWS_BUCKET'),
                        'Key'    => $pathAndFileName
                    ]);

            $request = $client->createPresignedRequest($command, $expiry);
            //https://stackoverflow.com/questions/36154281/get-files-signed-url-from-amazon-s3-using-filesystem-laravel-5-2

            $aws_path = (string) $request->getUri();

            return $aws_path;
        }
        else {//image is in server directory
            return base_path('public/images/'. $pathAndFileName);
        }
    }

    //Delete image from s3 bucket
    public static function deleteImageFromBucket($pathAndFileName){
        if( stripos($pathAndFileName, 'dynamic/') !== false ){//image is in bucket
            $s3 = \Storage::disk('s3');
            if ( $s3->exists($pathAndFileName) ) {
                $s3->delete($pathAndFileName);
            }
        }else if( file_exists( base_path('public/images/'. $pathAndFileName) )  ){//image is in server directory
            unlink( public_path().'/images/'.$pathAndFileName );
        }
    }

    /**
     * @param $beginning
     * @param $end
     * @param $string
     * @return mixed
     */
    public static function delete_all_between(string $beginning, string $end, string $string) {
        $beginningPos = strpos($string, $beginning);
        $endPos = strpos($string, $end);
        if ($beginningPos === false || $endPos === false) {
            return $string;
        }
        $textToDelete = substr($string, $beginningPos, ($endPos + strlen($end)) - $beginningPos);
        return str_replace($textToDelete, '', $string);
    }
}
?>
