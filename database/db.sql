-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Aug 26, 2020 at 11:48 PM
-- Server version: 5.7.26
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `delivero_admin`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
                              `id` int(11) NOT NULL,
                              `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                              `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                              `category_type_id` int(11) DEFAULT NULL,
                              `document_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                              `created_at` timestamp NULL DEFAULT NULL,
                              `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `image`, `category_type_id`, `document_id`, `created_at`, `updated_at`) VALUES
(1, 'Toys & Games', NULL, 1, '1CSHyFcMv9DhamII9Kb6', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(2, 'Books, Movies & Music', NULL, 2, '1HKh0pBHJfMREUst9jSy', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(3, 'Women\'s Clothing & Shoes', NULL, 3, '3tbWp5B2zzEbvLhBMxce', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(4, 'Baby Products', NULL, 1, '5MzBEFDNysZFLeee39Uw', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(5, 'Antiques & Collectibles', NULL, 2, '6MDjUXyGnSXesufnwpgJ', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(6, 'Home Decor', NULL, 4, '7RTjF0TY6vgQGQwsEo9l', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(7, 'Major Appliances', NULL, 4, 'G8IwuYLqEaygc8xJEgnD', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(8, 'Men\'s Clothing & Shoes', NULL, 3, 'HBNxhsuO0GkFym2RC8vY', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(9, 'Garden & Outdoor', NULL, 4, 'HVoZ4X3NvJKfXsv1x5Nx', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(10, 'Bath', NULL, 4, 'JPkUZRY4SOWSm29ymg34', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(11, 'Arts & Crafts', NULL, 2, 'Kt6ouh29FGvP7rduthcK', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(12, 'Miscellaneous', NULL, 5, 'NwgK6kbeDTvBXkvjRs8u', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(13, 'Health & Beauty', NULL, 5, 'Oe6zyOJ65SWwJ1QN4pLN', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(14, 'Kid\'s Clothing', NULL, 3, 'Ot7sGj11r2vDBJ0II7w3', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(15, 'Handbags', NULL, 3, 'Pc6tpLjgs8uHYdx1HdwY', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(16, 'Sporting Goods', NULL, 2, 'UBUZBgsY1K1eyhB2uyjC', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(17, 'Office Supplies', NULL, 5, 'VtnWNVvzOo1CwPCRHyCi', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(18, 'Tools & Home Improvement', NULL, 4, 'Wx2kbGyMlWwuujt3dCOX', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(19, 'Furniture', NULL, 4, 'ZrN6irn4Dz4KZre7Dh5P', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(20, 'Home Storage & Organisations', NULL, 4, 'ZyIRPenc8vbWihJDTZRR', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(21, 'Household Supplies & Cleaning', NULL, 4, 'bAzBOWBK0af8XYVNdGyK', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(22, 'Lamps & Lighting', NULL, 4, 'bupFPa0s7IzM5IBpQErR', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(23, 'Kitchen & Dining', NULL, 4, 'goh6Q52m4mvxnfFuEDTI', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(24, 'Kid\'s Clothing', NULL, 1, 'jmaKq2pOr39KnZwngQPr', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(25, 'Video Games & Consoles', NULL, 6, 'lS4EhCiuIdUtVkifvwFJ', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(26, 'Toys & Games', NULL, 2, 'orhQcUpnvrgPVSICK2AS', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(27, 'Musical Instrument', NULL, 2, 'p0DCMOBYAZqwU6ulVIok', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(28, 'Jewellery & Watches', NULL, 3, 'sDZAES7MRz0wvYoKb75e', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(29, 'Auto Parts & Accessories', NULL, 5, 'sSBhItV4tc1vDCalCpoh', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(31, 'Luggage', NULL, 3, 'v4PRx6irEesrrSRomWgu', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(32, 'Computers & Other Electronics', NULL, 6, 'vGlhBdDzGWyjBwVMxR9t', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(33, '', NULL, 3, 'w7FeKadZPkDBquzXxrjh', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(34, 'Bedding', NULL, 4, 'wxwoxKBpgxFzgVv1KtUD', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(35, 'Pet Supplies', NULL, 5, 'yNv3jVebCgLkKaYB1pwB', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(36, 'Cell Phones & Accessories', NULL, 6, 'yPNplEvOQvCnJgWeNpVP', '2020-08-26 15:40:39', '2020-08-26 15:40:39');

-- --------------------------------------------------------

--
-- Table structure for table `category_types`
--

CREATE TABLE `category_types` (
                                  `id` int(11) NOT NULL,
                                  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                  `created_at` timestamp NULL DEFAULT NULL,
                                  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_types`
--

INSERT INTO `category_types` (`id`, `name`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Baby & Kids', '', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(2, 'Entertainment & Hobbies', '', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(3, 'Clothing & Accessories', '', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(4, 'Home & Garden', '', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(5, 'Miscellaneous', '', '2020-08-26 15:40:39', '2020-08-26 15:40:39'),
(6, 'Electronics', '', '2020-08-26 15:40:39', '2020-08-26 15:40:39');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
                               `id` int(11) NOT NULL,
                               `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
                               `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
                               `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
                               `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
                               `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
                            `id` int(11) NOT NULL,
                            `amount` double DEFAULT NULL,
                            `capture_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                            `job_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                            `document_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                            `timestamp` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                            `created_at` timestamp NULL DEFAULT NULL,
                            `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `amount`, `capture_id`, `job_id`, `document_id`, `timestamp`, `created_at`, `updated_at`) VALUES
(3, 11, '2AC22342N8007443X', '78', '05KJcxNscaC1J1TNKPBQ', '1592925468331', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(5, 11, '0E335874U8970894S', '30', '0DtqQPm2DfFwLMujmBUH', '1595832626296', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(11, 11, '2A7524374D4396218', '134', '1IemgjtHJo3oiBeXVd20', '1592415720860', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(12, 11, '2ED936645C2606801', '63', '1Iv3tXTaRwNAGD8XweS7', '1596303814587', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(16, 11, '0E335874U8970894S', '30', '1ijscTMPBpKRmKbMN5nc', '1595907105387', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(17, 11, '2ED936645C2606801', '63', '1kAYRL5eI026nfZRrCHL', '1596296571217', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(19, 11, '2ED936645C2606801', '63', '1uSwMAUcGiwiHE60cT0e', '1596267543888', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(21, 11, '0E335874U8970894S', '30', '25M3XjMvmKsmAIQyKOaM', '1595883541480', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(25, 11, '8U4557355R056771V', '66', '2LGZd3q4M7LtkdAIS4qd', '1594733647978', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(30, 11, '0E335874U8970894S', '30', '2nddM8KC0VDgQyhul77i', '1595959047594', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(31, 11, '2ED936645C2606801', '63', '2tr8cSg6L5HSW4QAo2j9', '1596282040681', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(34, 10, '9JK76687C02870028', '136', '2ut2KkGbRYXGAxe0YPXv', '1593275545407', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(35, 11, '8U4557355R056771V', '66', '30l24SfGLVfJglEFP7HQ', '1594596805027', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(47, 11, '2A7524374D4396218', '134', '4vrTFIjTS5uR88MkTr5D', '1592283575146', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(51, 10, '9JK76687C02870028', '136', '58mS12HnQhWVNzdmQdiH', '1592731985216', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(53, 11, '2A7524374D4396218', '134', '5Ya1NYCImaoGAIbIdRTo', '1592675089810', '2020-08-26 18:15:47', '2020-08-26 18:15:47'),
(54, 11, '8U4557355R056771V', '66', '5gfci0fOZW4uv12XFMHa', '1594560304283', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(61, 11, '0E335874U8970894S', '30', '6blFVAMZVm8p1rs8RA2w', '1595868990883', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(66, 11, '95E71356UF668590D', '38', '7akBT64LAWAXXzUEkv0X', '1592371558472', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(68, 11, '2AC22342N8007443X', '78', '7htjABZ7YwemY7xOJdZu', '1592325189259', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(71, 11, '2ED936645C2606801', '63', '80ANlR9lAeF2sAk5vZ5T', '1596376317207', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(81, 11, '2ED936645C2606801', '63', '90A2oNaSG0V9edXMVfPO', '1596347331675', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(84, 11, '2AC22342N8007443X', '78', '9bWheKcNINYeFoXUQepv', '1592369806760', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(86, 11, '0E335874U8970894S', '30', '9r0q9Ob6i2tMDymZWDqd', '1595966302289', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(87, 11, '2AC22342N8007443X', '78', '9urd9xXdhtZsUDb1biee', '1592559352661', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(89, 10, '9JK76687C02870028', '136', 'AFXW8ADUS9yNy0A7UKh1', '1593524333104', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(93, 11, '95E71356UF668590D', '38', 'Ajb0kBU5bZ0m98ALWtkN', '1592510161550', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(94, 11, '8U4557355R056771V', '66', 'AlBxXNyxmPM9K7AszRjx', '1594643003768', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(95, 11, '8U4557355R056771V', '66', 'AoKGLCgSv3QhxHhO5eLS', '1594580677121', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(96, 11, '2AC22342N8007443X', '78', 'AqYKwfNsUBgfdCcmEmHC', '1592460373903', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(102, 11, '8U4557355R056771V', '66', 'BYgiRzj1jH21Coae3n4J', '1594696258594', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(110, 10, '9JK76687C02870028', '136', 'CO9Zci5G2EcytG3iLQXs', '1592995092736', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(111, 11, '2A7524374D4396218', '134', 'CQqZz6i1Vp2ACCqEGat5', '1592098550202', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(117, 11, '8U4557355R056771V', '66', 'DGMO4SRdqUY32QxoNLyo', '1594686794985', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(118, 11, '2ED936645C2606801', '63', 'DY107tO5rdl1Tb5Zscsw', '1596398082942', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(120, 10, '9JK76687C02870028', '136', 'DjiGRvmqk0xVPxHy6ZfQ', '1593556488088', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(127, 10, '9JK76687C02870028', '136', 'EWejQywwVX8E7xMDxg1X', '1593348672250', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(128, 10, '9JK76687C02870028', '136', 'Ec4utciIQEYUcx7H30hf', '1593238196177', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(129, 11, '8U4557355R056771V', '66', 'EepbIIiBbcFBXb7BjcS0', '1594665343834', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(130, 11, '0E335874U8970894S', '30', 'Ele7MqFVIF009GiWctoo', '1595898125514', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(136, 11, '8U4557355R056771V', '66', 'FbkFQDK7bAuFEQuGNg5m', '1594546517183', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(141, 11, '8U4557355R056771V', '66', 'GSMcxl9rqGL1Hopl1Yni', '1594574291888', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(145, 11, '2AC22342N8007443X', '78', 'Gul5WsMJDFdZDHG0f8aX', '1592828888528', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(146, 10, '9JK76687C02870028', '136', 'GvUuJHS3XuCNj5CeROo7', '1592580244566', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(149, 11, '2ED936645C2606801', '63', 'HcW44j1ZEgbxUyvya0QO', '1596311054241', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(152, 11, '8U4557355R056771V', '66', 'HmbcYoluuUowYXs0le36', '1594608843103', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(156, 11, '8U4557355R056771V', '66', 'IAGMBv1vxP2pQ28ZMwhi', '1594587786212', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(158, 11, '95E71356UF668590D', '38', 'IHdQb0hZpwVgJOJu4qKG', '1591987957245', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(161, 11, '2ED936645C2606801', '63', 'IoEmA1GeKgahQYjMUDTX', '1596261667447', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(164, 11, '0E335874U8970894S', '30', 'JZFVrpEOSHhEla7zsjy3', '1595825604972', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(165, 11, '2A7524374D4396218', '134', 'JaztTZD1MMypTftFNMQ2', '1591972474338', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(170, 11, '2AC22342N8007443X', '78', 'K50m7mFtZDvb5HtTGV1T', '1591986110155', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(173, 10, '9JK76687C02870028', '136', 'KRiukLlJ2RU3ml2iSx5M', '1592773935561', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(176, 11, '95E71356UF668590D', '38', 'Kss5OCeg4QNNmxpWGjwu', '1592160373271', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(178, 11, '0E335874U8970894S', '30', 'LDQ0dWA9FJjlq0FSm4gk', '1595861743448', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(179, 11, '2ED936645C2606801', '63', 'LGCMveXFz9gEoUjBbxht', '1596259574278', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(181, 11, '8U4557355R056771V', '66', 'LYAxOUuHz1Gi58jryh5B', '1594553677922', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(184, 11, '2ED936645C2606801', '63', 'LetVIhisFc4Y1pupmFx2', '1596259637109', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(185, 11, '0E335874U8970894S', '30', 'Lg9UKKsqtM9TvVecqO73', '1595824731502', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(186, 11, '8U4557355R056771V', '66', 'LhRWZ1ludHNXmM60S85B', '1594726369504', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(187, 10, '9JK76687C02870028', '136', 'Ljq5zjUP7tw4xDsl20Ed', '1593163150286', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(188, 11, '95E71356UF668590D', '38', 'M1AxzYSvnlEt1arYJO3S', '1593012472665', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(189, 11, '8U4557355R056771V', '66', 'M5SOWKfkd2R67TbnjQTN', '1594719061779', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(191, 11, '2ED936645C2606801', '63', 'MQH5D5JLzpL3GDdHiDMp', '1596260662244', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(192, 11, '8U4557355R056771V', '66', 'MQh2P7gSlE526NQwM5nI', '1594567742028', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(194, 11, '8U4557355R056771V', '66', 'MWe18ejTrD7r8gPj8uvb', '1594748145503', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(196, 11, '2A7524374D4396218', '134', 'MbOFO9Hn5JWcFg6w4mQy', '1592461532105', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(200, 11, '95E71356UF668590D', '38', 'MnRnBqHIKzH81RAAtlmb', '1592924933360', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(204, 11, '2ED936645C2606801', '63', 'NBTZjBuOadBP9AlLIInM', '1596318310170', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(205, 11, '0E335874U8970894S', '30', 'NGaXy2AduW2z9y6p24Ys', '1595847216772', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(206, 10, '9JK76687C02870028', '136', 'NIpsaF5idTobkoryZcvy', '1592628489442', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(207, 11, '95E71356UF668590D', '38', 'NM9kGHg2lnzNi8d58RWr', '1591924904705', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(209, 11, '2AC22342N8007443X', '78', 'NRkrmuWZJFV49MBHvyoc', '1593012926373', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(210, 10, '9JK76687C02870028', '136', 'NbjUVHzlZswSkiVcjG2F', '1593597721494', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(215, 10, '9JK76687C02870028', '136', 'OY6MxLTgk8s6SlojVYtF', '1593383784921', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(216, 11, '2ED936645C2606801', '63', 'Obqt53AoxBL7R5NXn4W1', '1596369088369', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(217, 11, '2AC22342N8007443X', '78', 'Oh2CnJYqG0zPTD5NaCC6', '1592612996734', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(220, 11, '95E71356UF668590D', '38', 'Ol8UDD5qXlsJ9xps2yGP', '1591781806806', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(221, 11, '0E335874U8970894S', '30', 'OlXSJ2mDZapoBnFyIYWd', '1595915503195', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(232, 11, '95E71356UF668590D', '38', 'QhahlxTxj1UITLUTSNpp', '1592732607542', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(234, 11, '8U4557355R056771V', '66', 'QxNzmQo7H9gQCuH7JxQv', '1594654217457', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(240, 11, '2AC22342N8007443X', '78', 'RUIbcR3sD6ETMlxA9iLw', '1592100318017', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(246, 10, '9JK76687C02870028', '136', 'S6lceZRKR0iMeZ50Ahlx', '1592951477991', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(248, 10, '9JK76687C02870028', '136', 'SBQdBX3bTiXXUqIQ6ONZ', '1593311904244', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(251, 11, '2ED936645C2606801', '63', 'SNH0IHv3uklNg24MeKI4', '1596354583196', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(254, 11, '0E335874U8970894S', '30', 'StrSVFTSu6H8jNVbZEhV', '1595825022668', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(256, 11, '2A7524374D4396218', '134', 'TBYnPEHoFSdPclQGfSDm', '1592159981573', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(262, 11, '2AC22342N8007443X', '78', 'Tq3GSNBq47s4ixjG1B5r', '1592969462275', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(263, 11, '0E335874U8970894S', '30', 'TqvjJYnPBV9RwNZcjbeo', '1595930026919', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(266, 11, '2AC22342N8007443X', '78', 'UBxHGuRDVAJDfpMk83yw', '1592508438416', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(268, 11, '2ED936645C2606801', '63', 'UUwm0XU3j2lA5hnxaE04', '1596390830276', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(272, 11, '8U4557355R056771V', '66', 'Usm7YhCffqTI3bQTzLwz', '1594676133116', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(276, 11, '2AC22342N8007443X', '78', 'VFI34KTDcqEl58nGPLoU', '1591923206362', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(277, 10, '9JK76687C02870028', '136', 'VMynHzrge7yV9pDKCHN3', '1593082418465', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(278, 11, '2A7524374D4396218', '134', 'VSNbGeWr7NCDVBW6Frhd', '1592509326562', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(280, 11, '95E71356UF668590D', '38', 'VVArXMgeckmzbL8I1I9c', '1592327108770', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(283, 11, '95E71356UF668590D', '38', 'VmZgWCBrxjJSyampG7DN', '1593055060894', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(290, 10, '9JK76687C02870028', '136', 'WhO2ZBRgcK5sE8qJ7tzJ', '1593122964837', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(296, 10, '9JK76687C02870028', '136', 'XT1mHceCkP7HU9JxFbUm', '1593453611389', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(297, 11, '2A7524374D4396218', '134', 'XU3GhHTnb6IjJAHFhcnx', '1591718890183', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(298, 11, '0E335874U8970894S', '30', 'XiPgn2xaV4qAQNsE0fzn', '1595944555299', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(305, 11, '2A7524374D4396218', '134', 'YbwZapWCjLpDxqtapw9o', '1592779899043', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(314, 11, '2AC22342N8007443X', '78', 'ZeCLhssomaclm5DqLMhe', '1592233908396', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(315, 11, '0E335874U8970894S', '30', 'ZhYz2XmTtwGfvbQ53pdj', '1595890838322', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(316, 11, '2ED936645C2606801', '63', 'ZoAAoQToud8gDImIwSJg', '1596259788772', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(322, 11, '95E71356UF668590D', '38', 'aUljL4mCYubroOiUeCJ7', '1591826565152', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(323, 11, '0E335874U8970894S', '30', 'abtimxTi35FeNJ1Srf80', '1595876254293', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(324, 11, '2A7524374D4396218', '134', 'aiYZMVawECW154at9F5g', '1592371137361', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(325, 11, '2AC22342N8007443X', '78', 'ajZCB6PG5qWN81FgKnF9', '1592731533741', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(326, 11, '2A7524374D4396218', '134', 'amHXmMmRF387y2rUqqgQ', '1591852262812', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(331, 10, '9JK76687C02870028', '136', 'bJQdUbiBjzJPEe664DDU', '1593039477893', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(332, 11, '8U4557355R056771V', '66', 'bMmayR5h8t9SfctkcPqe', '1594704569916', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(333, 11, '0E335874U8970894S', '30', 'bkzv2V2LRx2DhFqFYsG4', '1595824336084', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(343, 11, '95E71356UF668590D', '38', 'cj4QMKc2qi60lNc8Fjza', '1592462326932', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(348, 11, '8U4557355R056771V', '66', 'dQz6gyvTLoxpQQG9UZVn', '1594620557489', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(352, 11, '2A7524374D4396218', '134', 'dlc3i9bpv8kxBfTKpiVZ', '1591644325824', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(354, 11, '0E335874U8970894S', '30', 'eLzsiTyn5dF0sZL6GDMd', '1595839975377', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(359, 11, '2A7524374D4396218', '134', 'emVAGKvmx2F1omhoygq6', '1591804020127', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(361, 11, '2ED936645C2606801', '63', 'ewekfxMFzOSlafEwIzwG', '1596263679262', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(362, 11, '2ED936645C2606801', '63', 'eyDqcYlQCzAeSQB6d8tU', '1596289294376', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(365, 11, '8U4557355R056771V', '66', 'fcBd2YvYhIi8Giv2ZzbI', '1594768608305', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(369, 11, '95E71356UF668590D', '38', 'g3EHGYLB3S9mOt8VLF6O', '1592829218067', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(374, 10, '9JK76687C02870028', '136', 'gsvCNYqspFPD4ukKbzQP', '1593419020667', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(381, 11, '8U4557355R056771V', '66', 'hRbgp7R9BJiZCEO30HjD', '1594631874165', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(393, 10, '9JK76687C02870028', '136', 'iHfROZSowwQaN7lEWUB9', '1593200330851', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(396, 11, '95E71356UF668590D', '38', 'iZk2xzEGk5tvQod4b4d0', '1592969093778', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(397, 11, '0E335874U8970894S', '30', 'icQVloZdfSdScxTy4jfb', '1595951807512', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(400, 11, '2AC22342N8007443X', '78', 'iiWwphjMrVE3M6W5G6O3', '1592674744139', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(408, 11, '8U4557355R056771V', '66', 'kHlftyFWDvqR3h5Dp0TK', '1594755677206', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(410, 11, '95E71356UF668590D', '38', 'kLR8hL1KMN382T3Malaf', '1592877730663', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(414, 11, '95E71356UF668590D', '38', 'kl9kxAI31bWXY1WbJsxt', '1592676186509', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(425, 11, '95E71356UF668590D', '38', 'ls93eWj0tYb2ITI3c5Wj', '1591874764154', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(429, 11, '2A7524374D4396218', '134', 'mzPcSravJzVYXqQTgJrC', '1592560481389', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(438, 11, '2ED936645C2606801', '63', 'o3m8IgDYOXUgaE4GrJL2', '1596361850071', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(442, 10, '9JK76687C02870028', '136', 'oKnduITM3E0XBSkErqAT', '1592682824378', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(443, 11, '2ED936645C2606801', '63', 'oXdIxyHfGDKbLoMU8ZTm', '1596325570294', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(444, 11, '2A7524374D4396218', '134', 'oeNILvIq87DbvjFmggdz', '1592878296692', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(447, 11, '2AC22342N8007443X', '78', 'okIlV1hnn4ZgBXn7SSiX', '1592878496369', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(448, 11, '2A7524374D4396218', '134', 'onqO45LsE9jqHyRwpH3T', '1591905315027', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(449, 11, '95E71356UF668590D', '38', 'oozkEeh2zCLtw4UIQ4Py', '1592236009577', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(450, 11, '95E71356UF668590D', '38', 'orSyKgLuwStzA1zii6dr', '1592615422551', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(461, 11, '95E71356UF668590D', '38', 'pnsWFnIGc72zpC9S7Ft8', '1592047996663', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(462, 11, '2ED936645C2606801', '63', 'pp0HGV29TJa6oDUnqMwU', '1596274791757', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(463, 11, '2A7524374D4396218', '134', 'pp2fJhNAuTq4YCfDShOh', '1592614282394', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(464, 11, '2AC22342N8007443X', '78', 'q9k50mN2LaJKYctDhheH', '1593055321860', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(466, 11, '95E71356UF668590D', '38', 'qG1idcHDplLm7yH7zdq3', '1592561224218', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(470, 11, '2AC22342N8007443X', '78', 'qh7DdTcQXsP9vhkG1MAX', '1591873587048', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(471, 11, '2AC22342N8007443X', '78', 'qjaTMe8yV5Kt6dJFPuPV', '1592282294849', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(473, 11, '2A7524374D4396218', '134', 'qm6MjHx45KYx79kKb1WP', '1592038661164', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(474, 11, '0E335874U8970894S', '30', 'qzwRqYvVSnOmBWwz6Px8', '1595824497103', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(477, 11, '2AC22342N8007443X', '78', 'rXynuy2ayOOkvbtJXki8', '1592158281877', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(479, 11, '2ED936645C2606801', '63', 'rmkQTVWJNdRNk7yo6z7l', '1596340082954', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(480, 11, '0E335874U8970894S', '30', 'roBBkaEsrqGvdEB6m4Wx', '1595937307498', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(481, 11, '2AC22342N8007443X', '78', 's8mmKBa1s3Kld2Rh1Soy', '1591825463437', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(484, 10, '9JK76687C02870028', '136', 'sOHydlSYfbHuATCzxXez', '1593488733409', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(490, 11, '0E335874U8970894S', '30', 't1OrESQh85pd7wdiq0SQ', '1595854467193', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(492, 11, '0E335874U8970894S', '30', 'tNlatK98Q0MJF6Nh0eEJ', '1595828676965', '2020-08-26 18:15:48', '2020-08-26 18:15:48'),
(509, 11, '2AC22342N8007443X', '78', 'ugno8Vr0kQh1GMJjC1KV', '1592414315541', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(511, 11, '95E71356UF668590D', '38', 'ukx1EHjjnGAvSfXaarDd', '1592102150030', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(515, 11, '8U4557355R056771V', '66', 'vKy3gPTl8OPfRHUK5ABh', '1594740892671', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(519, 11, '2A7524374D4396218', '134', 'w3Jw3FQzOo79EIvapgZc', '1592235878048', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(520, 11, '2ED936645C2606801', '63', 'wABQVJTU4fz86mhs7N3S', '1596332821309', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(521, 11, '2ED936645C2606801', '63', 'wFOk395dduFQUnzLOreV', '1596260120830', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(526, 10, '9JK76687C02870028', '136', 'wKg9xFLfYrUu6BsDzIBR', '1592816384507', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(531, 11, '2A7524374D4396218', '134', 'wrUel3fcQxQFvLJiimgU', '1592731578757', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(532, 11, '2A7524374D4396218', '134', 'xC62jvJ7gQIrR1JB8Mpf', '1591680275890', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(533, 11, '2AC22342N8007443X', '78', 'xGP6TL3HtwXOO5hh1cJt', '1591780633922', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(535, 10, '9JK76687C02870028', '136', 'xI6CiQ6O4XGphmvbn8Lb', '1592860738796', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(538, 11, '95E71356UF668590D', '38', 'xVWMSH7CeQ0psIYxvHFh', '1592416138922', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(542, 11, '0E335874U8970894S', '30', 'y4lzJtRda26mLGEXpTbS', '1595826685690', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(544, 11, '8U4557355R056771V', '66', 'yIpEakyi5ey7iY65JdvJ', '1594711811474', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(546, 11, '2AC22342N8007443X', '78', 'yhbwwHGHPxVIZGTdFG7a', '1592045870676', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(548, 11, '2A7524374D4396218', '134', 'yktbT3617oxIF3JynMLV', '1592327206460', '2020-08-26 18:15:49', '2020-08-26 18:15:49'),
(556, NULL, NULL, NULL, '3zkkGid4rZtaTt0aMQTV', NULL, '2020-08-26 18:33:39', '2020-08-26 18:33:39'),
(557, NULL, NULL, NULL, 'DI8Q5xwiKpYnX2J58M1O', NULL, '2020-08-26 18:33:39', '2020-08-26 18:33:39'),
(558, NULL, NULL, NULL, 'FXerJyvpp5HmuXKjUMwo', NULL, '2020-08-26 18:33:39', '2020-08-26 18:33:39'),
(559, NULL, NULL, NULL, 'bbBemZorT3o3C0RM0ILo', NULL, '2020-08-26 18:33:39', '2020-08-26 18:33:39'),
(560, NULL, NULL, NULL, 'cEo042HGUu3GqduHsSKa', NULL, '2020-08-26 18:33:39', '2020-08-26 18:33:39'),
(561, NULL, NULL, NULL, 'g1jXf1lkaHLVoLBBTR0x', NULL, '2020-08-26 18:33:39', '2020-08-26 18:33:39'),
(562, NULL, NULL, NULL, 'jg42ZEDBTMWaV3tRMVAZ', NULL, '2020-08-26 18:33:39', '2020-08-26 18:33:39'),
(563, NULL, NULL, NULL, 'lxqfT8ABjswc1kWTuypt', NULL, '2020-08-26 18:33:39', '2020-08-26 18:33:39');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
                        `id` int(11) NOT NULL,
                        `delivery_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `delivery_latitude` double DEFAULT NULL,
                        `delivery_longitude` double DEFAULT NULL,
                        `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `expected_delivery_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `geohash` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `item_category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `job_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `job_latitude` double DEFAULT NULL,
                        `job_longitude` double DEFAULT NULL,
                        `job_price` double NOT NULL,
                        `package_size` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `posted_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `posted_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `priority` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `receiver_instructions` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `security_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `document_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `created_at` timestamp NULL DEFAULT NULL,
                        `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `delivery_address`, `delivery_latitude`, `delivery_longitude`, `description`, `expected_delivery_time`, `geohash`, `item_category`, `job_address`, `job_latitude`, `job_longitude`, `job_price`, `package_size`, `posted_at`, `posted_id`, `priority`, `receiver_instructions`, `security_code`, `status`, `document_id`, `created_at`, `updated_at`) VALUES
(1, 'Essen, Germany', 51.4556432, 7.0115552, 'N/A', '2020-08-19T18:30:00.000Z', 'gcx4zr0784e', 'Miscellaneous', 'York, UK', 53.9599651, -1.0872979, 41.1, 'Sedan', '1596734485874', NULL, 'Flexible', 'N/A', '537679', 'Pending', '0iwgRlRY06lw3YI0DDDo', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(2, 'Mavdi, Rajkot, Gujarat, India', 22.2605665, 70.7815301, 'N/A', '2020-08-15T18:30:00.000Z', 'tefqf2vzewb', 'Miscellaneous', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.18, 'SUV', '1597135250991', NULL, 'Flexible', 'N/A', '3028', 'Pending', '0uylmG4DfDNMTDlshEYw', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(3, 'Teston Ln, Teston, Maidstone ME18 5BX, UK', 51.2529798, 0.4451043, 'N/A', 'N/A', 'sr2y5zk90ep', 'Accessories', 'Monte Testaccio, 00153 Rome, Metropolitan City of Rome, Italy', 41.875952, 12.475694, 10.1775, 'Sedan', '1596635469237', NULL, 'Immediate', 'N/A', '403717', 'Pending', '1ltcnekXgZSZnQgvnWUN', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(4, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', 'N/A', 'tefqf2vzewb', 'Documents', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Sedan', '1596892146012', NULL, 'Immediate', 'N/A', '0313', 'Pending', '1pUz47knXnSsrlU38W2h', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(5, 'Sydney NSW, Australia', -33.8688197, 151.2092955, 'N/A', '2020-08-19T18:30:00.000Z', 'ttn9ted3h4m', 'Home', 'TCI Supply Chain Solutions, Khasra No. 4/21, Pataudi Road, Sector 97, Gurugram, Haryana 122505, India', 28.4080829, 76.8855938, 10, 'Sedan', '1596734764763', NULL, 'Flexible', 'N/A', '642452', 'Pending', '1sTdKm1Dn9cXvSUZB0X3', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(6, 'Rome, Metropolitan City of Rome, Italy', 41.9027835, 12.4963655, 'N/A', 'N/A', '9xj64fk3vf6', 'Home', 'Denver, CO, USA', 39.7392358, -104.990251, 10, 'SUV', '1596733943251', NULL, 'Immediate', 'N/A', '631376', 'Pending', '1xE3htDtqTF4VzX6yL1O', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(7, 'Mavdi, Rajkot, Gujarat, India', 22.2605665, 70.7815301, 'N/A', '2020-08-09T18:30:00.000Z', 'tefqf2vzewb', 'Documents', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'SUV', '1596887009053', NULL, 'Flexible', 'N/A', '9488', 'Pending', '25lahnkuTGiD70zpZZ9Z', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(8, 'Kotecha Cir, Nutan Nagar, Kotecha Nagar, Rajkot, Gujarat 360001, India', 22.2903413, 70.7785845, 'N/A', 'N/A', 'tefqdx4yqzs', 'Furniture', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'SUV', '1595846504189', NULL, 'Immediate', 'N/A', '318138', 'Pending', '27aiQHxjEUnlI8Cpfjdi', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(9, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', 'N/A', 'tefqf2vzewb', 'Miscellaneous', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Sedan', '1596892213380', NULL, 'Immediate', 'N/A', '1228', 'Pending', '2T3t8Tl3G5VxmlY6sNq7', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(10, 'Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan', 24.9180271, 67.0970916, 'N/A', 'N/A', 'tkrtyb4w58c', 'Accessories', 'Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan', 24.9180271, 67.0970916, 10.1775, 'Sedan', '1592434692112', NULL, 'Immediate', 'N/A', '812216', 'Pending', '2ykYB5azPryUoc0S1ARW', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(11, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', 'N/A', 'tefqf2vzewb', 'House Hold', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Sedan', '1592581847359', NULL, 'Immediate', 'N/A', '223757', 'Pending', '3J1oSY5hi2fYw93y2Ma3', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(12, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdx4yqzs', 'Home', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1596266004029', NULL, 'Immediate', 'N/A', '668743', 'Pending', '3feqgfYdgh3F0mSEGTKe', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(13, 'Frankfurt, Germany', 50.1109221, 8.6821267, 'N/A', '2020-08-13T18:30:00.000Z', '9qdbfpvtjtv', 'Home', 'Fresno, CA, USA', 36.7377981, -119.7871247, 10, 'SUV', '1596736171273', NULL, 'Flexible', 'N/A', '176823', 'Pending', '3uehiTuySUrnsP2H4cm3', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(14, 'Toulouse, France', 43.604652, 1.444209, 'N/A', 'N/A', 'u0smh8p7g2e', 'Home', 'Nancy, France', 48.692054, 6.184417, 39.398481109307, 'SUV', '1596734220875', NULL, 'Immediate', 'N/A', '182251', 'Pending', '3y7wkU53Pmg3nV4e60Yh', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(15, 'Fresno, CA, USA', 36.7377981, -119.7871247, 'N/A', 'N/A', '9q8yyk8yuv5', 'Electronic', 'San Francisco, CA, USA', 37.7749295, -122.4194155, 13.85095101004, 'SUV', '1596704070042', NULL, 'Immediate', 'N/A', '112276', 'Pending', '4Z5fyKXbdMyHSJQ18mLi', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(16, 'Rimini, Province of Rimini, Italy', 44.0678288, 12.5695158, 'N/A', '2020-08-27T18:30:00.000Z', 'u3tm0q6p6pv', 'Documents', 'Gdańsk, Poland', 54.3520252, 18.6466384, 10.18, 'SUV', '1596734932264', NULL, 'Flexible', 'N/A', '642452', 'Pending', '4lxGhHZF5E3Pgx1S2PGI', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(17, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', 'N/A', 'tefqf2vzewb', 'Miscellaneous', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Sedan', '1596949257189', NULL, 'Immediate', 'N/A', '7149', 'Pending', '5TuLLUakzYX8ciymQllO', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(18, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', 'N/A', 'tefqf2vzewb', 'Documents', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Truck', '1596886206442', NULL, 'Immediate', 'N/A', '5901', 'Pending', '5VRq5jQZ44WwNH961h98', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(19, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', 'N/A', 'tefqf2vzewb', 'Documents', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'SUV', '1596949466600', NULL, 'Immediate', 'N/A', '7149', 'Pending', '6431sNiTF6SnAAlKnNVH', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(20, 'The Gallowan River, 194401', 34.7589668, 78.1701053, 'N/A', '2020-08-17T18:30:00.000Z', 'twptdz9txmp', 'Accessories', 'The Gallowan River, 194401', 34.7589668, 78.1701053, 10.1775, 'Sedan', '1596547389856', NULL, 'Flexible', 'N/A', '895239', 'Pending', '6A7M9Q8xW22xmN7u8KU7', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(21, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdmvqn0m', 'Documents', 'Nana Mava Main Rd, Shastri Nagar, Rajkot, Gujarat 360005, India', 22.2689454, 70.7704147, 10.1775, 'Sedan', '1596092478219', NULL, 'Immediate', 'N/A', '667300', 'Delivered', '6qV9JsBOGhk2576FwbjE', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(22, 'Toulouse, France', 43.604652, 1.444209, 'N/A', 'N/A', 'spc00cgxs82', 'Accessories', 'Toulouse, France', 43.604652, 1.444209, 10.1775, 'Sedan', '1596543934713', NULL, 'Immediate', 'N/A', '340960', 'Pending', '7UnlUw5B7xkbnMoI9R3W', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(23, 'Toulouse, France', 43.604652, 1.444209, 'N/A', 'N/A', 'dpz83dffmxp', 'Accessories', 'Toronto, ON, Canada', 43.653226, -79.3831843, 10, 'Sedan', '1596543343362', NULL, 'Immediate', 'N/A', '634648', 'Pending', '7kJ5QD7HWBUz6RfHLImq', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(24, 'Kotecha Cir, Nutan Nagar, Kotecha Nagar, Rajkot, Gujarat 360001, India', 22.2903413, 70.7785845, 'N/A', 'N/A', 'tefqf2vzewb', 'Home', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Sedan', '1596267590431', NULL, 'Immediate', 'N/A', '483869', 'Pending', '8M4B23ThJSUJsyyGOjpf', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(25, 'Dallas, TX, USA', 32.7766642, -96.7969879, 'N/A', 'N/A', 'thrr3squys6', 'Accessories', 'Dubai - United Arab Emirates', 25.2048493, 55.2707828, 10, 'Sedan', '1596544567430', NULL, 'Immediate', 'N/A', '138817', 'Pending', '8RmUwJIfj1H2hLoLEmtM', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(26, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', '2020-08-15T18:30:00.000Z', 'tefqf2vzewb', 'Miscellaneous', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'SUV', '1596885368622', NULL, 'Flexible', 'N/A', '6351', 'Pending', '97nLnBYbHEtfreN6epnV', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(27, 'Nana Mava Rd, Satyam Park, Nana Mava, Rajkot, Gujarat, India', 22.27144, 70.7687105, 'N/A', 'N/A', 'tefqf2vzewb', 'Miscellaneous', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'SUV', '1596282162422', NULL, 'Immediate', 'N/A', '573570', 'Pending', '9JqmXPqsz8nSojhob48m', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(28, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', 'N/A', 'tefqf2vzewb', 'Documents', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Truck', '1596885526294', NULL, 'Immediate', 'N/A', '6351', 'Pending', '9lUpe0KvXNeFqldbW1DX', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(29, 'Rome, Metropolitan City of Rome, Italy', 41.9027835, 12.4963655, 'N/A', '2020-08-13T18:30:00.000Z', 'sxk93kq8272', 'Miscellaneous', 'Çifte Havuzlar, YTÜ-Davutpaşa Kampüsü, Esenler/İstanbul, Turkey', 41.0243832, 28.8919951, 10.18, 'Sedan', '1596734977230', NULL, 'Flexible', 'N/A', '642452', 'Pending', '9zz1lqHQaBfqYf6CzxAx', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(30, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdx4yqzs', 'Home', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'SUV', '1595824129612', NULL, 'Immediate', 'Please be careful.', '193091', 'Pending', 'A1SruskFCE87GToKiGfj', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(31, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdx4yqzs', 'Accessories', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1592581563697', NULL, 'Immediate', 'N/A', '511188', 'Pending', 'BgPPZPENtTNIOOkLstCI', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(32, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', '2020-08-23T18:30:00.000Z', 'tefqf2vzewb', 'Furniture', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Sedan', '1596891009445', NULL, 'Flexible', 'N/A', '8166', 'Pending', 'BjfhSw0V4RxhPj2MJwm8', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(33, 'vulica Pinskaja 28, Minsk, Belarus', 53.9055061, 27.5167927, 'N/A', 'N/A', 'u15pmus9dtr', 'Miscellaneous', 'Rotterdam, Netherlands', 51.9244201, 4.4777326, 10.18, 'Sedan', '1596896414934', NULL, 'Immediate', 'N/A', '6128', 'Pending', 'BoN5FSsMPyjydlnY4Dty', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(34, 'Shapar, Gujarat 360024, India', 22.1572507, 70.7990772, 'N/A', 'N/A', 'tefr43669t2', 'Miscellaneous', 'Madhapar, Rajkot, Gujarat 360006, India', 22.3315435, 70.7660939, 10.1775, 'SUV', '1596949920625', NULL, 'Immediate', 'N/A', '0313', 'Accepted', 'Ca5YTDg1dbLUXZ8xHNzW', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(35, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', 'N/A', 'tefqf2vzewb', 'Documents', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Sedan', '1596271580445', NULL, 'Immediate', 'N/A', '518547', 'Pending', 'CfWe6zYHRLheZyQv5OsU', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(36, 'Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan', 24.9180271, 67.0970916, 'N/A', 'N/A', 'tkrtyb4w58c', 'Accessories', 'Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan', 24.9180271, 67.0970916, 10.1775, 'Sedan', '1592434786523', NULL, 'Immediate', 'N/A', '812216', 'Pending', 'CgGhHcTE86LxUp0of343', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(37, 'Kotecha Cir, Nutan Nagar, Kotecha Nagar, Rajkot, Gujarat 360001, India', 22.2903413, 70.7785845, 'N/A', 'N/A', 'tefqdx4yqzs', 'Accessories', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1596116236235', NULL, 'Immediate', 'N/A', '972292', 'Pending', 'CgTAVuKAYtlhtHAgB974', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(38, 'Indira Circle, Rajkot, Gujarat, India', 22.2883798, 70.7709278, 'N/A', 'N/A', 'tefqdnnw18r', 'Accessories', 'Nana Mava, Rajkot, Gujarat 360005, India', 22.2703186, 70.7609536, 10.1775, 'Sedan', '1591781710503', NULL, 'Immediate', 'N/A', NULL, 'Pending', 'CxMPVf3yMUgDz570acXT', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(39, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', 'N/A', 'tefqf2vzewb', 'House Hold', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Sedan', '1594913758170', NULL, 'Immediate', 'N/A', '615633', 'Pending', 'DCXd0AIC3NmZt768KYWv', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(40, 'Rome, Metropolitan City of Rome, Italy', 41.9027835, 12.4963655, 'N/A', 'N/A', 'dpz83dffmxp', 'Home', 'Toronto, ON, Canada', 43.653226, -79.3831843, 10, 'SUV', '1596735955478', NULL, 'Immediate', 'N/A', '176823', 'Pending', 'DazUUMgKShrNs7S15zQx', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(41, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdx4yqzs', 'Accessories', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1594891112675', NULL, 'Immediate', 'N/A', '4880', 'Pending', 'DiEkGfD1l1xlYlFhLg3Q', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(42, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', '2020-08-16T18:30:00.000Z', 'tefqf2vzewb', 'Miscellaneous', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Truck', '1596949111921', NULL, 'Flexible', 'N/A', '9181', 'Pending', 'E4ES2VPGKWXKclI3TRR8', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(43, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', '2020-08-08T18:30:00.000Z', 'tefqdq6cyp8', 'Miscellaneous', '202, Nana Mava Rd, Satyam Park, Shastri Nagar, Rajkot, Gujarat 360005, India', 22.271002, 70.766974, 10.1775, 'Sedan', '1596882217591', NULL, 'Flexible', 'N/A', '1909', 'Pending', 'EJ3xsGQSuKpMewvbkV3E', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(44, 'Frankfurt, Germany', 50.1109221, 8.6821267, 'N/A', 'N/A', 'gbquseudyn2', 'Home', 'Nantes, France', 47.218371, -1.553621, 43.619377386577, 'SUV', '1596734040279', NULL, 'Immediate', 'N/A', '631376', 'Pending', 'ET0daXW9bLXYooXxXJbM', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(45, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', '2020-07-23T18:30:00.000Z', 'tefqf9g6ssc', 'Accessories', 'Kotecha Cir, Nutan Nagar, Kotecha Nagar, Rajkot, Gujarat 360001, India', 22.2903413, 70.7785845, 10.1775, 'Sedan', '1594627792987', NULL, 'Flexible', 'N/A', '0781', 'Pending', 'EfJjyHbtEn1JlRblSyU6', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(46, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdx4yqzs', 'Gift', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1594964115252', NULL, 'Immediate', 'N/A', '061808', 'Pending', 'FUc91WiTlYl1vHdjBKZF', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(47, 'Kotecha Circle, Nirmala Convent Rd, Jala Ram Nagar, Rajkot, Gujarat 360007, India', 22.2908169, 70.7782449, 'N/A', 'N/A', 'tefqf2vzewb', 'Accessories', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Sedan', '1596267480330', NULL, 'Immediate', 'N/A', '483869', 'Pending', 'FuLkLf1v7hu9eSb0zRcx', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(48, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', 'N/A', 'tefqf2vzewb', 'Documents', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Truck', '1596885602726', NULL, 'Immediate', 'N/A', '0604', 'Pending', 'G7TkE3wvO7nUY5sANurG', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(49, '31 Kelantan Ln, Singapore 200031', 1.3066189, 103.8570136, 'N/A', 'N/A', '9qdbfpvtjtv', 'Miscellaneous', 'Fresno, CA, USA', 36.7377981, -119.7871247, 10, 'Sedan', '1596733556673', NULL, 'Immediate', 'N/A', '779775', 'Pending', 'G9mPzKdEFOKOHrYmM7Br', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(50, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', 'N/A', 'tefqf2vzewb', 'Accessories', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Sedan', '1592581727363', NULL, 'Immediate', 'N/A', '511188', 'Pending', 'IGoRNJj9SFMaLd2PIutc', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(51, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdx4yqzs', 'House Hold', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10, 'Sedan', '1595329023350', NULL, 'Immediate', 'N/A', '979967', 'Pending', 'IIoXXAFkLP9feTpM3f6t', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(52, 'Galwan Valley, 194401', 34.7601744, 78.1883076, 'N/A', '2020-08-19T18:30:00.000Z', 'dpz83dffmxp', 'Home', 'Toronto, ON, Canada', 43.653226, -79.3831843, 10, 'Truck', '1596734648168', NULL, 'Flexible', 'N/A', '537679', 'Pending', 'JK3ucsgdtRylv0IabHzM', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(53, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdx4yqzs', 'Home', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1596265618419', NULL, 'Immediate', 'N/A', '208590', 'Pending', 'JpyqdSHEJ1M4JmFYh98U', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(54, 'Trondheim, Norway', 63.4305149, 10.3950528, 'N/A', 'N/A', 'sr2y5zk90ep', 'Accessories', 'Monte Testaccio, 00153 Rome, Metropolitan City of Rome, Italy', 41.875952, 12.475694, 10.1775, 'Sedan', '1596543544822', NULL, 'Immediate', 'N/A', '634648', 'Pending', 'LjL1NxUHQ7YxllZ40oaI', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(55, '4505 Plaza Dr, Irving, TX 75063, USA', 32.9139997, -97.0117516, 'N/A', '2020-09-30T05:00:00.000Z', '9vg54mws958', 'Home', '11500 Lago Vista E, Farmers Branch, TX 75234, USA', 32.902098, -96.923403, 18, 'SUV', '1596921998907', NULL, 'Flexible', 'Show this to driver', '3793', 'Accepted', 'LwhFRkvneYMmGgOqxFOs', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(56, 'Essen, Germany', 51.4556432, 7.0115552, 'N/A', 'N/A', 'sr2ykk5t6kv', 'Documents', 'Rome, Metropolitan City of Rome, Italy', 41.9027835, 12.4963655, 67.09348475315, 'Sedan', '1596703663245', NULL, 'Immediate', 'N/A', '179578', 'Pending', 'MUKWL1qU1JYCBBiG6u4W', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(57, 'United States', 44.427963, -110.588455, 'N/A', '2020-08-19T18:30:00.000Z', 'dpz83dffmxp', 'Miscellaneous', 'Toronto, ON, Canada', 43.653226, -79.3831843, 10.18, 'Sedan', '1596734553561', NULL, 'Flexible', 'N/A', '537679', 'Pending', 'MrRxBMsWzUJNE2oVWsp8', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(58, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', '2020-08-13T18:30:00.000Z', 'tefqf9g6ssc', 'Miscellaneous', 'Kotecha Cir, Nutan Nagar, Kotecha Nagar, Rajkot, Gujarat 360001, India', 22.2903413, 70.7785845, 12, 'Truck', '1596624404146', NULL, 'Flexible', 'N/A', '491162', 'Pending', 'N525X6A0SMIIi14qa3E9', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(59, '1940 Lantana Ln, Irving, TX 75063, USA', 32.9230462, -96.9694637, 'N/A', 'N/A', '9vg5190btjc', 'Home', '1136 Hidden Ridge, Irving, TX 75038, USA', 32.8767037, -96.9640861, 10.1775, 'SUV', '1596903150713', NULL, 'Immediate', 'N/A', '2601', 'Accepted', 'N9guEkgNUCPY2VouS3mp', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(60, 'The Gallowan River, 194401', 34.7589668, 78.1701053, 'N/A', 'N/A', '9q9m316pqj1', 'Miscellaneous', '45500 Fremont Blvd, Fremont, CA 94538, USA', 37.4934895, -121.9452364, 10, 'Sedan', '1596896184031', NULL, 'Immediate', 'N/A', '8086', 'Pending', 'NDfnYP8LXoa9SfUrveiM', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(61, 'Bakshi Bazar, Dhaka, Bangladesh', 23.7218725, 90.3954387, 'N/A', 'N/A', 'wh0r0eqyqm3', 'Other', 'Kalabagan, Dhaka 1205, Bangladesh', 23.7494231, 90.3830754, 10.1775, 'SUV', '1595857278391', NULL, 'Immediate', 'N/A', '107931', 'Pending', 'NthWgckOG7Ttxcekvdjl', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(62, 'Fresno, CA, USA', 36.7377981, -119.7871247, 'N/A', 'N/A', 'dpz83dffmxp', 'Accessories', 'Toronto, ON, Canada', 43.653226, -79.3831843, 10.1775, 'Sedan', '1596702674759', NULL, 'Immediate', 'N/A', '575939', 'Pending', 'OdEkwcIZ73aAagbR8w0w', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(63, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdx4yqzs', 'Documents', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1596259179082', NULL, 'Immediate', 'N/A', '437775', 'Pending', 'Q4OJXgc0x9mYahuunthR', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(64, 'Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan', 24.9180271, 67.0970916, 'N/A', 'N/A', 'tkrtyb4w58c', 'Accessories', 'Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan', 24.9180271, 67.0970916, 10.1775, 'Sedan', '1592434161260', NULL, 'Immediate', 'N/A', '238327', 'Pending', 'Qjjn2xqTsoHiTBozPCNR', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(65, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', 'N/A', 'tefqf2vzewb', 'Accessories', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Sedan', '1594913988469', NULL, 'Immediate', 'N/A', '615633', 'Pending', 'TPvUzMojqYIimq1545Z5', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(66, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdmvqn0m', 'Accessories', 'Nana Mava Main Rd, New Gandhinagar Society, Shastri Nagar, Rajkot, Gujarat 360005, India', 22.2689454, 70.7704147, 10.1775, 'Sedan', '1594546356276', NULL, 'Immediate', 'N/A', '7967', 'Received', 'U1YkTv0n5f7lq21xrgHb', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(67, 'Toronto, ON, Canada', 43.653226, -79.3831843, 'N/A', 'N/A', 'spc00cgxs82', 'Miscellaneous', 'Toulouse, France', 43.604652, 1.444209, 10, 'Sedan', '1596896362501', NULL, 'Immediate', 'N/A', '8086', 'Pending', 'U2ql7sC5pG4hILxBHAqs', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(68, 'Rimini, Province of Rimini, Italy', 44.0678288, 12.5695158, 'N/A', '2020-08-24T18:30:00.000Z', 'gbquseudyn2', 'Miscellaneous', 'Nantes, France', 47.218371, -1.553621, 66.06, 'SUV', '1596734407557', NULL, 'Flexible', 'N/A', '537679', 'Pending', 'UCV08qpD2h2nWsGo39yW', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(69, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdx4yqzs', 'Gift', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10, 'Sedan', '1594963099163', NULL, 'Immediate', 'N/A', '965297', 'Pending', 'UXvwUGfuBWkZ90YiwGNd', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(70, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdx4yqzs', 'Home', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1596267036546', NULL, 'Immediate', 'N/A', '693010', 'Pending', 'V56kLzE8EpGlwqAoFguK', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(71, '9800 Hjørring, Denmark', 57.4567788, 9.9957635, 'N/A', 'N/A', '9vg18sjtqj0', 'Miscellaneous', 'Joe Pool Lake, Texas, USA', 32.6303234, -97.0014657, 10, 'Truck', '1597160955756', NULL, 'Immediate', 'N/A', '9662', 'Pending', 'VD0hRKNlH1ZTRLpFHyq2', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(72, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdx4yqzs', 'Documents', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1596092654529', NULL, 'Immediate', 'N/A', '667300', 'Pending', 'W7aaIhB0KV1fiQIQXTUP', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(73, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', 'N/A', 'tefqf2vzewb', 'Documents', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Sedan', '1596886319949', NULL, 'Immediate', 'N/A', '5901', 'Pending', 'WaFkG8zWC9Xa5ujW0XdY', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(74, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdmvqn0m', 'Accessories', 'Nana Mava Main Rd, New Gandhinagar Society, Shastri Nagar, Rajkot, Gujarat 360005, India', 22.2689454, 70.7704147, 10.1775, 'Sedan', '1594887875214', NULL, 'Immediate', 'N/A', '8415', 'Accepted', 'X4pLLmImtTzUmoqGWTYk', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(75, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdx4yqzs', 'Gift', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1594965607656', NULL, 'Immediate', 'N/A', '858374', 'Pending', 'XjJ7Ehwqie2VORzrW7eu', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(76, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', '2020-08-15T18:30:00.000Z', 'tefqdq6cyp8', 'Miscellaneous', '202, Nana Mava Rd, Satyam Park, Shastri Nagar, Rajkot, Gujarat 360005, India', 22.271002, 70.766974, 10.1775, 'Sedan', '1596884473729', NULL, 'Flexible', 'N/A', '6804', 'Pending', 'XqqIQqjvkJvppsBXHGSo', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(77, 'Visakhapatnam, Andhra Pradesh, India', 17.6868159, 83.2184815, 'N/A', 'N/A', 'tepffhb5pzp', 'Furniture', 'Hyderabad, Telangana, India', 17.385044, 78.486671, 28.513011762662, 'SUV', '1596898032452', NULL, 'Immediate', 'N/A', '3639', 'Pending', 'YQpOfkvCp8HKeXJEgPvF', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(78, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdqnt2u2', 'Accessories', 'Nana Mava Rd, Shastri Nagar, Rajkot, Gujarat 360005, India', 22.2702117, 70.7718983, 10.1775, 'Sedan', '1591780478222', NULL, 'Immediate', 'N/A', NULL, 'Pending', 'Yso7KU0xGgpDmKJJYY7B', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(79, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', 'N/A', 'tefqf2vzewb', 'Documents', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Truck', '1596892340189', NULL, 'Immediate', 'N/A', '1228', 'Pending', 'ZTZuDRnqQThbDOOzgggp', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(80, 'Indira Circle, Rajkot, Gujarat, India', 22.2883798, 70.7709278, 'N/A', 'N/A', 'tefqf2vzewb', 'Accessories', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Sedan', '1596267172654', NULL, 'Immediate', 'N/A', '693010', 'Pending', 'ZuQEGj3aBldnnFLGGVdW', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(81, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdx4yqzs', 'Accessories', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1594914058551', NULL, 'Immediate', 'N/A', '108781', 'Pending', 'aPxr2UJ6No8gxFVE3LEe', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(82, 'Montana, USA', 48.7596128, -113.7870225, 'N/A', 'N/A', 'tc0z313zmb7', 'Miscellaneous', '299 Galle Rd, Colombo 00300, Sri Lanka', 6.907527, 79.8512878, 10, 'Sedan', '1596703158252', NULL, 'Immediate', 'N/A', '389496', 'Pending', 'aeyY4DMFvQU74MjvGzRZ', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(83, 'Hyderabad, Telangana, India', 17.385044, 78.486671, 'N/A', '2020-07-27T18:30:00.000Z', 'tfcmghy56vy', 'Electronic', 'Vijayawada, Andhra Pradesh, India', 16.5061743, 80.6480153, 12, 'Sedan', '1595840507224', NULL, 'Flexible', 'Not specific', '502193', 'Pending', 'bAxzFjMm8KnukYKfDSAy', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(84, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', 'N/A', 'tefqf2vzewb', 'Accessories', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Sedan', '1594914032135', NULL, 'Immediate', 'N/A', '108781', 'Pending', 'bckegslryF2pLXGGPiXo', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(85, 'The Gallowan River, 194401', 34.7589668, 78.1701053, 'N/A', 'N/A', 'dpz83dffmxp', 'Accessories', 'Toronto, ON, Canada', 43.653226, -79.3831843, 10, 'Sedan', '1596544383155', NULL, 'Immediate', 'N/A', '667444', 'Pending', 'bfrQf5yAyLtQGAejj4Au', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(86, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdx4yqzs', 'Accessories', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1594964944058', NULL, 'Immediate', 'N/A', '279397', 'Pending', 'c5fPntHXmR5El9jdOR2N', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(87, 'Kotecha Cir, Nutan Nagar, Kotecha Nagar, Rajkot, Gujarat 360001, India', 22.2903413, 70.7785845, 'N/A', 'N/A', 'tefqdx4yqzs', 'Accessories', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1596116242347', NULL, 'Immediate', 'N/A', '972292', 'Pending', 'ch5w6wrRwiyUHZJLQUXP', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(88, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdx4yqzs', 'Accessories', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1592481402760', NULL, 'Immediate', 'N/A', '378484', 'Pending', 'd1UpX3hw2wBGHbWVvI5V', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(89, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdx4yqzs', 'Gift', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1594965637165', NULL, 'Immediate', 'N/A', '858374', 'Pending', 'dOwFVOVI1R4WhsEIxURK', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(90, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', 'N/A', 'tefqf2vzewb', 'Home', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Sedan', '1596266239430', NULL, 'Immediate', 'N/A', '493409', 'Pending', 'dztFAImhwbJu5OpcUGYU', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(91, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', 'N/A', 'tefqf2vzewb', 'Documents', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'SUV', '1596892053451', NULL, 'Immediate', 'N/A', '0313', 'Pending', 'e5eFtgruH6uETBBSBrZb', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(92, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdnnw18r', 'Accessories', 'Nana Mava, Rajkot, Gujarat 360005, India', 22.2703186, 70.7609536, 10, 'Sedan', '1592581413173', NULL, 'Immediate', 'N/A', '126887', 'Pending', 'eKduYTS68DzNrN8mgWSA', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(93, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdx4yqzs', 'Accessories', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1593100711863', NULL, 'Immediate', 'N/A', '140546', 'Pending', 'ebj4uNm4UMLMQfuzqXGC', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(94, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', '2020-08-16T18:30:00.000Z', 'tefqf2vzewb', 'Documents', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Sedan', '1596886867217', NULL, 'Flexible', 'N/A', '9488', 'Accepted', 'f7Ar7HBcFKaI7eg0M4Kp', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(95, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdmvqn0m', 'Accessories', 'Nana Mava Main Rd, New Gandhinagar Society, Shastri Nagar, Rajkot, Gujarat 360005, India', 22.2689454, 70.7704147, 10, 'Sedan', '1592486023459', NULL, 'Immediate', 'N/A', '768226', 'Delivered', 'fhacyI6k18gZp8QEXily', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(96, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdx4yqzs', 'Accessories', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1594964993773', NULL, 'Immediate', 'N/A', '279397', 'Pending', 'g307eCmjnazK6k7hehA9', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(97, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdmvqn0m', 'Miscellaneous', 'Nana Mava Main Rd, Shastri Nagar, Rajkot, Gujarat 360005, India', 22.2689454, 70.7704147, 10.1775, 'Sedan', '1596092368322', NULL, 'Immediate', 'N/A', '239804', 'Accepted', 'gRelBxufl55yVZwNHGuG', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(98, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdx4yqzs', 'House Hold', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1595846071826', NULL, 'Immediate', 'N/A', '015394', 'Pending', 'hkTH7bDP8GAWERwFkROZ', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(99, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', 'N/A', 'tefqf2vzewb', 'Documents', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Sedan', '1596271785026', NULL, 'Immediate', 'N/A', '518547', 'Pending', 'i67n2kxgfLp4u4shMkkx', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(100, 'Nanikhodiyar, Anand, Gujarat 388001, India', 22.566763, 72.949059, 'N/A', 'N/A', 'ts1s1qy5361', 'Miscellaneous', '98, Vijay Nagar, Bhuj, Gujarat 370001, India', 23.2407731, 69.6726027, 18.590202970483, 'Sedan', '1596635414550', NULL, 'Immediate', 'N/A', '403717', 'Pending', 'j6RHvmGgQT1N7h3FAUbL', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(101, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', 'N/A', 'tefqf2vzewb', 'Miscellaneous', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Sedan', '1596949392015', NULL, 'Immediate', 'N/A', '7149', 'Pending', 'k4Zt1ytxxNaDsBTAW3z1', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(102, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', 'N/A', 'tefqf2vzewb', 'Documents', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Truck', '1596093381228', NULL, 'Immediate', 'N/A', '198484', 'Pending', 'kZCID9nKM6eydlC6PexA', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(103, 'Indira Circle, Rajkot, Gujarat, India', 22.2883798, 70.7709278, 'N/A', 'N/A', 'tefqf2vzewb', 'Documents', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Truck', '1596887230150', NULL, 'Immediate', 'N/A', '0829', 'Pending', 'kbwU30mW9nxYe0f0EQ05', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(104, 'Oklahoma City, OK, USA', 35.4675602, -97.5164276, 'N/A', 'N/A', 'dpz83dffmxp', 'Accessories', 'Toronto, ON, Canada', 43.653226, -79.3831843, 10.1775, 'Sedan', '1596544703349', NULL, 'Immediate', 'N/A', '138817', 'Pending', 'l1Kej3TP4cBlcitH9aEv', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(105, '4505 Plaza Dr, Irving, TX 75063, USA', 32.9139997, -97.0117516, 'N/A', '2020-09-30T05:00:00.000Z', '9vg54mws958', 'Home', '11500 Lago Vista E, Farmers Branch, TX 75234, USA', 32.902098, -96.923403, 18, 'SUV', '1596922000425', NULL, 'Flexible', 'Show this to driver', '3793', 'Pending', 'lRvP8ceaawBwbLlABpJ1', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(106, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdx4yqzs', 'Accessories', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1594887928464', NULL, 'Immediate', 'N/A', '2125', 'Pending', 'mQrKij7xptWVnD5LE007', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(107, 'Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan', 24.9180271, 67.0970916, 'N/A', 'N/A', 'tkrtyb4w58c', 'Accessories', 'Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan', 24.9180271, 67.0970916, 10.1775, 'Sedan', '1592435019910', NULL, 'Immediate', 'N/A', '112330', 'Pending', 'mzjQVEBtAF6SPSw6wnH9', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(108, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdmvqn0m', 'Accessories', 'Nana Mava Main Rd, New Gandhinagar Society, Shastri Nagar, Rajkot, Gujarat 360005, India', 22.2689454, 70.7704147, 10.1775, 'Sedan', '1592483632664', NULL, 'Immediate', 'N/A', '771388', 'Pending', 'nfp8VwTHZIpSruyYU24S', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(109, 'River Front Dr, Cedar Creek, TX 78612, USA', 30.1862839, -97.4392156, 'N/A', 'N/A', '9vff0mq6ykc', 'Miscellaneous', 'Riverfront Dr, Fort Worth, TX 76107, USA', 32.72465, -97.3629727, 15.77627211897, 'SUV', '1596143484192', NULL, 'Immediate', 'N/A', '783984', 'Accepted', 'nptn3NTLOyncfqnyCcsv', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(110, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdmvqn0m', 'Accessories', 'Nana Mava Main Rd, New Gandhinagar Society, Shastri Nagar, Rajkot, Gujarat 360005, India', 22.2689454, 70.7704147, 10.1775, 'Sedan', '1592480481543', NULL, 'Immediate', 'N/A', '952487', 'Pending', 'nrwIcHh5aiEGFv1fyQxN', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(111, '825 Las Palmas Dr, Grand Prairie, TX 75052, USA', 32.7044936, -96.9930688, 'N/A', 'N/A', '9vg5190btjc', 'Home', '1136 Hidden Ridge, Irving, TX 75038, USA', 32.8767037, -96.9640861, 10, 'SUV', '1596919301249', NULL, 'Immediate', 'N/A', '0631', 'Pending', 'nxzTMCW3AAMXysTG4Q3P', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(112, '7319 29 Ave Unit 108, Edmonton, AB T6K 2P1, Canada', 53.4596135, -113.4441275, 'N/A', '2020-08-21T18:30:00.000Z', 'spc00cgxs82', 'Miscellaneous', 'Toulouse, France', 43.604652, 1.444209, 10, 'SUV', '1596736278253', NULL, 'Flexible', 'N/A', '475889', 'Pending', 'oShtGPEm3KQTlzbzNTSh', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(113, 'Visakhapatnam, Andhra Pradesh, India', 17.6868159, 83.2184815, 'N/A', 'N/A', 'tepffhb5pzp', 'Home', 'Hyderabad, Telangana, India', 17.385044, 78.486671, 28.513011762662, 'SUV', '1596900683004', NULL, 'Immediate', 'N/A', '2532', 'Pending', 'pDPftMykJsa7XJuqYPxu', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(114, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', 'N/A', 'tefqf2vzewb', 'Accessories', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Sedan', '1595403730743', NULL, 'Immediate', 'hi there', '405730', 'Pending', 'pYnBoEN7Gl9JvOvNZIoT', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(115, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdmvqn0m', 'Documents', 'Nana Mava Main Rd, Shastri Nagar, Rajkot, Gujarat 360005, India', 22.2689454, 70.7704147, 10.1775, 'Sedan', '1596092783712', NULL, 'Immediate', 'N/A', '540273', 'Pending', 'paMHrcQE3YA2hRLprqD4', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(116, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqf3mz4ph', 'Documents', 'Indira Circle, Rajkot, Gujarat, India', 22.2883798, 70.7709278, 10.1775, 'Truck', '1596891981735', NULL, 'Immediate', 'N/A', '0313', 'Pending', 'pusTdGyco7OLNOOe8W5R', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(117, '1920 Westridge Dr, Irving, TX 75038, USA', 32.8823239, -96.9764084, 'N/A', '2020-08-31T05:00:00.000Z', '9vg5190btjc', 'Home', '1136 Hidden Ridge, Irving, TX 75038, USA', 32.8767037, -96.9640861, 10, 'SUV', '1597004763307', NULL, 'Immediate', 'For driver to view', '9585', 'Pending', 'pwFMOsbBsWJveSyHyKDH', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(118, 'Visakhapatnam, Andhra Pradesh, India', 17.6868159, 83.2184815, 'N/A', '2020-08-20T18:30:00.000Z', 'tepffhb5pzp', 'Furniture', 'Hyderabad, Telangana, India', 17.385044, 78.486671, 28.513011762662, 'SUV', '1596895908695', NULL, 'Immediate', 'N/A', '1058', 'Pending', 'pwmr2oqEhQBojCFa56L9', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(119, '6097 Canoga Ave, Woodland Hills, CA 91367, USA', 34.1810006, -118.597547, 'N/A', 'N/A', '66jc26exrjm', 'Accessories', 'San Petersburgo 6351, San Miguel, Región Metropolitana, Chile', -33.5152668, -70.6479625, 10, 'Sedan', '1596544147026', NULL, 'Immediate', 'N/A', '340960', 'Pending', 'qJndahK5N6SjtLtXqEQo', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(120, '825 Las Gallinas Ave, San Rafael, CA 94903, USA', 38.0066896, -122.5470407, 'N/A', 'N/A', '9vg5190btjc', 'Documents', '1136 Hidden Ridge, Irving, TX 75038, USA', 32.8767037, -96.9640861, 10.1775, 'Truck', '1596919108858', NULL, 'Immediate', 'This message to be seen by driver.', '4925', 'Pending', 'r7JXU2tPtsKPqkCmOt9D', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(121, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdx4yqzs', 'Documents', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1596090949525', NULL, 'Immediate', 'N/A', '049545', 'Pending', 'sP2KFGafLttZp7NDxu3R', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(122, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', 'N/A', 'tefqf2vzewb', 'House Hold', 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 10.1775, 'Sedan', '1594913618542', NULL, 'Immediate', 'N/A', '150273', 'Pending', 'tDHx5FntNgITraJ6IgUF', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(123, 'Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan', 24.9180271, 67.0970916, 'N/A', 'N/A', 'tkrtyb4w58c', 'Accessories', 'Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan', 24.9180271, 67.0970916, 10.1775, 'Sedan', '1592434153516', NULL, 'Immediate', 'N/A', '238327', 'Pending', 'tZ0OYgI4hTH0E4CHzZWq', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(124, 'Edmonton, AB, Canada', 53.5461245, -113.4938229, 'N/A', 'N/A', '9g7p4vb4tpe', 'Miscellaneous', 'Xalapa, Ver., Mexico', 19.5437751, -96.9101806, 10.1775, 'Sedan', '1596703450338', NULL, 'Immediate', 'N/A', '948454', 'Pending', 'tp5oZyZUeLjKMLQ71HT5', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(125, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', '2020-08-11T18:30:00.000Z', 'tefqdx4yqzs', 'Home', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1596625605152', NULL, 'Flexible', 'N/A', '734825', 'Pending', 'u9LvDlg3yrbDkqcrXWUN', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(126, 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 'N/A', '2020-08-08T18:30:00.000Z', 'tefqdq6cyp8', 'Miscellaneous', '202, Nana Mava Rd, Satyam Park, Shastri Nagar, Rajkot, Gujarat 360005, India', 22.271002, 70.766974, 10.18, 'Truck', '1596779542164', NULL, 'Flexible', 'N/A', '455368', 'Pending', 'uS0WZqQe8tCexV4h8cbB', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(127, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdx4yqzs', 'House Hold', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1594964302648', NULL, 'Immediate', 'N/A', '061808', 'Pending', 'uoiTxLxGG1Daz81XTNIq', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(128, 'Kotecha Cir, Nutan Nagar, Kotecha Nagar, Rajkot, Gujarat 360001, India', 22.2903413, 70.7785845, 'N/A', 'N/A', 'tefqdx4yqzs', 'Home', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1596624280340', NULL, 'Immediate', 'N/A', '193835', 'Pending', 'v3wmW7jruYAtNzTAfWL7', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(129, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdx4yqzs', 'Accessories', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1594890664885', NULL, 'Immediate', 'N/A', '1083', 'Pending', 'vQNxLDkQiaeNaAIA6oal', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(130, 'Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan', 24.9180271, 67.0970916, 'N/A', 'N/A', 'tkrtyb4w58c', 'Accessories', 'Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan', 24.9180271, 67.0970916, 10.1775, 'Sedan', '1592500522818', NULL, 'Immediate', 'N/A', '131491', 'Pending', 'vm1pV2hrgES6BZxlDC1v', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(131, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdx4yqzs', 'House Hold', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'SUV', '1594913548363', NULL, 'Immediate', 'N/A', '150273', 'Pending', 'y3LU0vYF5VUMjw92VPyz', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(132, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', '2020-08-08T18:30:00.000Z', 'tefqdx4yqzs', 'Documents', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1596890929472', NULL, 'Flexible', 'N/A', '4094', 'Pending', 'yAIvaGibVnV3kUK5Bxc7', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(133, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', '2020-08-15T18:30:00.000Z', 'tefqdq6cyp8', 'Miscellaneous', '202, Nana Mava Rd, Satyam Park, Shastri Nagar, Rajkot, Gujarat 360005, India', 22.271002, 70.766974, 10.1775, 'Sedan', '1596883470845', NULL, 'Flexible', 'N/A', '3298', 'Accepted', 'ysjNzKu0tsJ6s05DvoRJ', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(134, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdx4yqzs', 'Accessories', 'Nana Mava Circle, Padmi Society, Mavdi, Rajkot, Gujarat 360005', 22.2758934, 70.7779983, 10.1775, 'Sedan', '1591603209875', NULL, 'Immediate', 'N/A', NULL, 'Pending', 'ywBKxU3Kr1tcxO8MOEdw', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(135, 'Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan', 24.9180271, 67.0970916, 'N/A', 'N/A', 'tkrtyb4w58c', 'Documents', 'Gulshan-e-Iqbal, Karachi, Karachi City, Sindh, Pakistan', 24.9180271, 67.0970916, 10.1775, 'Truck', '1592415055037', NULL, 'Immediate', 'N/A', '594231', 'Pending', 'z1L3jMV1v0qktOw4rtkl', '2020-08-26 17:50:40', '2020-08-26 17:50:40'),
(136, 'Royal Park, Rajkot, Gujarat 360005, India', 22.2857183, 70.7709867, 'N/A', 'N/A', 'tefqdmvqn0m', 'Accessories', 'Nana Mava Main Rd, New Gandhinagar Society, Shastri Nagar, Rajkot, Gujarat 360005, India', 22.2689454, 70.7704147, 10, 'Sedan', '1592580165123', NULL, 'Immediate', 'N/A', '882037', 'Pending', 'zQEWjB8N88LjUou9CwDv', '2020-08-26 17:50:40', '2020-08-26 17:50:40');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
                             `id` int(11) NOT NULL,
                             `device_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             `geohash` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             `latitude` double DEFAULT NULL,
                             `longitude` double DEFAULT NULL,
                             `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             `user_id` int(11) DEFAULT NULL,
                             `document_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                             `created_at` timestamp NULL DEFAULT NULL,
                             `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `device_token`, `geohash`, `latitude`, `longitude`, `role`, `user_id`, `document_id`, `created_at`, `updated_at`) VALUES
(1, 'f861911e5fb7317383978d', 'tkrtktu8r8u', 24.8607343, 67.0011364, 'Driver', 27, '3zkkGid4rZtaTt0aMQTV', '2020-08-26 18:37:41', '2020-08-26 18:37:41'),
(2, 'e2833d876557a98bb2ce09', 'tefqdryb6ez', 22.2789632, 70.7723264, 'Consumer', 32, 'DI8Q5xwiKpYnX2J58M1O', '2020-08-26 18:37:41', '2020-08-26 18:37:41'),
(3, '4d5914a5c91b62944e2547', 'tg0pn1k0e5h', 18.1124372, 79.0192997, 'Driver', 22, 'FXerJyvpp5HmuXKjUMwo', '2020-08-26 18:37:41', '2020-08-26 18:37:41'),
(4, '37d241cc9914d3cc34c52d', '9vg512zu3vw', 32.8759731, -96.9655921, 'Consumer', 24, 'bbBemZorT3o3C0RM0ILo', '2020-08-26 18:37:41', '2020-08-26 18:37:41'),
(5, '391c8fa525e75d2475b02f', 'tefqdryb6ez', 22.2789632, 70.7723264, 'Consumer', 20, 'cEo042HGUu3GqduHsSKa', '2020-08-26 18:37:41', '2020-08-26 18:37:41'),
(6, '4d5914a5c91b62944e2547', 'tg0pn1k0e5h', 18.1124372, 79.0192997, 'Consumer', 22, 'g1jXf1lkaHLVoLBBTR0x', '2020-08-26 18:37:41', '2020-08-26 18:37:41'),
(7, '8e77089e8ab0e266c7d1a3', 'tefqdq5571v', 22.2698551, 70.7671948, 'Consumer', 30, 'jg42ZEDBTMWaV3tRMVAZ', '2020-08-26 18:37:41', '2020-08-26 18:37:41'),
(8, 'e601e712dbcea16efaa8a1', 'wh0r0et9dk0', 23.7499741, 90.381185, 'Consumer', 19, 'lxqfT8ABjswc1kWTuypt', '2020-08-26 18:37:41', '2020-08-26 18:37:41');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
                              `id` int(10) UNSIGNED NOT NULL,
                              `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                              `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2020_08_19_215845_create_role_user_table', 1),
(10, '2020_08_19_223303_create_categories_table', 1),
(11, '2020_08_19_223652_create_category_types_table', 1),
(12, '2020_08_19_224531_create_invoices_table', 1),
(13, '2020_08_20_112207_create_wallets_table', 1),
(14, '2020_08_20_121850_create_locations_table', 1),
(15, '2020_08_20_122438_create_jobs_table', 1),
(16, '2020_08_20_122732_create_rooms_table', 1),
(17, '2020_08_20_124447_create_products_table', 1),
(18, '2020_08_20_130140_create_products_photos_urls_table', 1),
(19, '2020_08_22_143532_create_permission_tables', 1),
(21, '2020_08_26_213237_create_photourls_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
                                         `permission_id` bigint(20) UNSIGNED NOT NULL,
                                         `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                         `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
                                   `role_id` bigint(20) UNSIGNED NOT NULL,
                                   `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(7, 'App\\User', 19),
(7, 'App\\User', 20),
(7, 'App\\User', 21),
(7, 'App\\User', 22),
(7, 'App\\User', 23),
(7, 'App\\User', 24),
(7, 'App\\User', 25),
(7, 'App\\User', 26),
(7, 'App\\User', 27),
(7, 'App\\User', 28),
(7, 'App\\User', 29),
(7, 'App\\User', 30),
(7, 'App\\User', 31),
(7, 'App\\User', 32);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
                                       `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                                       `user_id` bigint(20) UNSIGNED DEFAULT NULL,
                                       `client_id` bigint(20) UNSIGNED NOT NULL,
                                       `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                       `scopes` text COLLATE utf8mb4_unicode_ci,
                                       `revoked` tinyint(1) NOT NULL,
                                       `created_at` timestamp NULL DEFAULT NULL,
                                       `updated_at` timestamp NULL DEFAULT NULL,
                                       `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('2b7cb093cc3527d51513624c177f9d78634e1a2f35ab5ea77a67a3f4ee2264339005b70fb486e7df', 1, 1, 'Laravel Password Grant Client', '[]', 0, '2020-08-23 15:09:04', '2020-08-23 15:09:04', '2021-08-23 20:09:04'),
('3633bd1594ec681a05e2db8bde5b73cb557dfba6c7a58a8fc263094085dc6f49f54b3b51e45ae7bd', 1, 1, 'Laravel Password Grant Client', '[]', 1, '2020-08-23 10:03:14', '2020-08-23 10:03:14', '2021-08-23 15:03:14'),
('3abf865f10bd5a26f96de0ce247ce61ca53df5d752ff6a890c4b93e12de89a03bb3e06b27e3fda45', 1, 1, 'Laravel Password Grant Client', '[]', 1, '2020-08-23 14:10:01', '2020-08-23 14:10:01', '2021-08-23 19:10:01'),
('90cf20e7f350a6bd32193c7aca72a24594aa0a645d419d528fb784b39504f96db3d22aa470d47d0c', 1, 1, 'Laravel Password Grant Client', '[]', 0, '2020-08-23 10:00:44', '2020-08-23 10:00:44', '2021-08-23 15:00:44'),
('9317d3a276734d560cf015f007a1c3a8139415ca2b087854bb564c7eb83c5789a51ac358b3213b0d', 1, 1, 'Laravel Password Grant Client', '[]', 0, '2020-08-23 10:01:56', '2020-08-23 10:01:56', '2021-08-23 15:01:56'),
('c14f8608699ada9c089861deb0e15a805b40563095766f07522bf1f05572f5f123d62a26b9d2f70d', 1, 1, 'Laravel Password Grant Client', '[]', 0, '2020-08-23 10:00:23', '2020-08-23 10:00:23', '2021-08-23 15:00:23'),
('c59b07aee3a133fab97cd96a5fc26f4f2debfb01ff90802d86987409a2e491285be137eddff48205', 1, 1, 'AUthToken', '[]', 0, '2020-08-23 05:08:57', '2020-08-23 05:08:57', '2021-08-23 10:08:57');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
                                    `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                                    `user_id` bigint(20) UNSIGNED NOT NULL,
                                    `client_id` bigint(20) UNSIGNED NOT NULL,
                                    `scopes` text COLLATE utf8mb4_unicode_ci,
                                    `revoked` tinyint(1) NOT NULL,
                                    `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
                                 `id` bigint(20) UNSIGNED NOT NULL,
                                 `user_id` bigint(20) UNSIGNED DEFAULT NULL,
                                 `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                 `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                 `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `personal_access_client` tinyint(1) NOT NULL,
                                 `password_client` tinyint(1) NOT NULL,
                                 `revoked` tinyint(1) NOT NULL,
                                 `created_at` timestamp NULL DEFAULT NULL,
                                 `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'BPzC5NoPzTupswgu7DM1MTQoQQLF5QTNHSj7ucEi', NULL, 'http://localhost', 1, 0, 0, '2020-08-23 05:08:53', '2020-08-23 05:08:53');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
                                                 `id` bigint(20) UNSIGNED NOT NULL,
                                                 `client_id` bigint(20) UNSIGNED NOT NULL,
                                                 `created_at` timestamp NULL DEFAULT NULL,
                                                 `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-08-23 05:08:53', '2020-08-23 05:08:53');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
                                        `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                                        `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                                        `revoked` tinyint(1) NOT NULL,
                                        `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
                                   `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
                               `id` bigint(20) UNSIGNED NOT NULL,
                               `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                               `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                               `created_at` timestamp NULL DEFAULT NULL,
                               `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `photourls`
--

CREATE TABLE `photourls` (
                             `id` bigint(20) UNSIGNED NOT NULL,
                             `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                             `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                             `owner_id` int(11) NOT NULL,
                             `product_id` int(11) NOT NULL,
                             `firebase_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                             `created_at` timestamp NULL DEFAULT NULL,
                             `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `photourls`
--

INSERT INTO `photourls` (`id`, `name`, `type`, `owner_id`, `product_id`, `firebase_url`, `created_at`, `updated_at`) VALUES
(1, '1592682500000-img5.jpg', 'product', 27, 1, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1592682500000-img5.jpg?alt=media&token=c7ef9eeb-505a-4f9f-a33d-b2accb630532', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(2, '1594569524000-img7.jpg', 'product', 21, 2, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1594569524000-img7.jpg?alt=media&token=bda55769-4cb9-4b22-8fc7-6c525013169d', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(3, '1594568287000-img4.jpg', 'product', 21, 3, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1594568287000-img4.jpg?alt=media&token=de9352e6-e3c6-4c05-bbf7-281c8f2f2cd4', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(4, '1591299931000-6f3b215b6530bbd88dfc7f3d3b390aa1.jpg', 'product', 27, 4, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591299931000-6f3b215b6530bbd88dfc7f3d3b390aa1.jpg?alt=media&token=943e91ab-1057-45d7-b11d-e6f522016876', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(5, '1591526431000-0ef2d09faab8f4aa03a6a898472a54e1.jpg', 'product', 27, 5, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591526431000-0ef2d09faab8f4aa03a6a898472a54e1.jpg?alt=media&token=9e515153-1660-4a33-b286-1af04489456e', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(6, '1591268045000-1ec2838d5cd0cd53eaface83a1bfa921.jpg', 'product', 27, 6, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591268045000-1ec2838d5cd0cd53eaface83a1bfa921.jpg?alt=media&token=0a9eb420-4204-4ed3-9f23-63a946b7c4f5', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(7, '1591278598000-5a24033a48ce0dfe82ca9ed68dff0d46.jpg', 'product', 27, 7, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591278598000-5a24033a48ce0dfe82ca9ed68dff0d46.jpg?alt=media&token=fed9533d-e763-4230-8933-2c3f924852ce', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(8, '1592682419000-img7.jpg', 'product', 27, 8, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1592682419000-img7.jpg?alt=media&token=4cbc3d8e-326b-4b8b-bfea-5e2f5a9c840c', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(9, '1591267801000-0bfcbc2ef78ad1ec040541ab72000bfe.jpg', 'product', 27, 9, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591267801000-0bfcbc2ef78ad1ec040541ab72000bfe.jpg?alt=media&token=27fe59d5-3cff-45b9-9aa9-d9dfc99d5fb8', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(10, '1591698232000-3f65da91ce35d9d9364a1a2fd75a4a63.jpg', 'product', 27, 10, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591698232000-3f65da91ce35d9d9364a1a2fd75a4a63.jpg?alt=media&token=991f9cd1-002b-40c0-a313-c8066df6a2ad', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(11, '1591278655000-5120 x 2880.jpg', 'product', 27, 11, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591278655000-5120%20x%202880.jpg?alt=media&token=2ae9d903-5cf8-4580-9620-ad87f7507019', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(12, '1591526165000-1 JAVA SCRIPT 2.png', 'product', 27, 12, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591526165000-1%20JAVA%20SCRIPT%202.png?alt=media&token=97ec6348-aac6-4c1d-8d0b-4c810c7ceba7', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(13, '1594569784000-img5.jpg', 'product', 21, 13, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1594569784000-img5.jpg?alt=media&token=83c46e59-f287-49bb-8852-b971534f8908', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(14, '1591268493000-1c6249eced8071ae829575b0b59a9712.jpg', 'product', 27, 14, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591268493000-1c6249eced8071ae829575b0b59a9712.jpg?alt=media&token=0de2601e-67c5-4ea3-a78c-11fcab3b64b9', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(15, '1591268423000-1c46e74a667b88260e699408d659ce38.jpg', 'product', 27, 15, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591268423000-1c46e74a667b88260e699408d659ce38.jpg?alt=media&token=a8941c94-bd35-4936-9b71-3d0a9c79af54', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(16, '1591268341000-1b4a55d9d5fa19756be4c73500007934.jpg', 'product', 27, 16, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591268341000-1b4a55d9d5fa19756be4c73500007934.jpg?alt=media&token=7904fb13-e194-43fd-a145-857f647c07e8', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(17, '1591279007000-Wall92.jpg', 'product', 27, 17, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591279007000-Wall92.jpg?alt=media&token=8d3de557-9e62-4ed6-a5c6-2d431e51cf98', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(18, '1591268446000-3f65da91ce35d9d9364a1a2fd75a4a63.jpg', 'product', 27, 18, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591268446000-3f65da91ce35d9d9364a1a2fd75a4a63.jpg?alt=media&token=ded2709a-b822-46e3-97f7-be183348c711', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(19, '1591268182000-1d2430b7f93bd1867186c2f15b076f74.jpg', 'product', 27, 19, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591268182000-1d2430b7f93bd1867186c2f15b076f74.jpg?alt=media&token=dcfda565-99e5-470c-8e2c-c3478400674b', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(20, '1591268186000-2a879d3c957727694634d9e5234bd94c.jpg', 'product', 27, 19, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591268186000-2a879d3c957727694634d9e5234bd94c.jpg?alt=media&token=115d410f-8dd9-4b7c-9a79-a54b1a70bdd8', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(21, '1591387090000-052ce910cc5b24a696b788d603407adb.jpg', 'product', 27, 20, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591387090000-052ce910cc5b24a696b788d603407adb.jpg?alt=media&token=e783c8fc-c9d2-460e-8d45-3cae920495d8', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(22, '1591387093000-3f65da91ce35d9d9364a1a2fd75a4a63.jpg', 'product', 27, 20, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591387093000-3f65da91ce35d9d9364a1a2fd75a4a63.jpg?alt=media&token=f91684b7-efa5-4050-895d-3fa4de6727f8', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(23, '1591387096000-9aadf953cee378f14496675c7f23095a.jpg', 'product', 27, 20, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591387096000-9aadf953cee378f14496675c7f23095a.jpg?alt=media&token=44aceea6-5098-43fb-9742-0316a2821bb8', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(24, '1591379023000-2f409645a0f2d134c77cac7f1ea50cb9.jpg', 'product', 27, 22, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591379023000-2f409645a0f2d134c77cac7f1ea50cb9.jpg?alt=media&token=00ed7ab9-2dbd-4fb3-bd1b-5abd095a6d64', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(25, '1591379024000-3aa0dd5eaffc4dddfd637990f2cead48.jpg', 'product', 27, 22, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591379024000-3aa0dd5eaffc4dddfd637990f2cead48.jpg?alt=media&token=0fabe708-71de-4452-a0ca-f9471f63a7e2', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(26, '1591379026000-3f65da91ce35d9d9364a1a2fd75a4a63.jpg', 'product', 27, 22, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591379026000-3f65da91ce35d9d9364a1a2fd75a4a63.jpg?alt=media&token=f7805d43-2e51-4c32-97d9-b8d4a41fec06', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(27, '1591379028000-3edfbb5f58144180b9933e9df562e273.jpg', 'product', 27, 22, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591379028000-3edfbb5f58144180b9933e9df562e273.jpg?alt=media&token=0c151a53-edf8-42f2-a2d7-2e771c754bd4', '2020-08-26 17:25:20', '2020-08-26 17:25:20'),
(28, '1591268327000-3f44c357d1648815285bd5daaa629277.jpg', 'product', 27, 24, 'https://firebasestorage.googleapis.com/v0/b/deliveryist.appspot.com/o/images%2FproductImages%2F1591268327000-3f44c357d1648815285bd5daaa629277.jpg?alt=media&token=a4748afa-3628-4b14-aa06-36f5856b74fd', '2020-08-26 17:25:20', '2020-08-26 17:25:20');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
                            `id` int(11) NOT NULL,
                            `added_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                            `category_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                            `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                            `extra_labels` json NOT NULL,
                            `geohash` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                            `insured` tinyint(1) NOT NULL,
                            `latitude` double NOT NULL,
                            `longitude` double NOT NULL,
                            `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                            `price` double NOT NULL,
                            `quantities_available` double NOT NULL,
                            `document_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                            `created_at` timestamp NULL DEFAULT NULL,
                            `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `added_by`, `category_id`, `description`, `extra_labels`, `geohash`, `insured`, `latitude`, `longitude`, `name`, `price`, `quantities_available`, `document_id`, `created_at`, `updated_at`) VALUES
(1, '21', '1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrtvgd97ff', 1, 24.936448, 67.0531584, 'Microsoft Os', 1500, 20, '3dyvREeXZgKq9jeB0Qp6', '2020-08-26 17:18:43', '2020-08-26 17:18:43'),
(2, '21', '31', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrttfwqteh', 0, 24.887913, 67.0583857, 'Twilight', 855, 2, '3phqgpgMrIIYLqM93pPs', '2020-08-26 17:18:43', '2020-08-26 17:18:43'),
(3, '21', '1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrtktu8r8u', 1, 24.8607343, 67.0011364, 'Product 1', 44, 100, '7LhSKGikI792VLtRQww7', '2020-08-26 17:18:43', '2020-08-26 17:18:43'),
(4, '21', '1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"foo\"]', 'tkrttyv08hn', 1, 24.9102336, 67.0564352, 'Product 100058', 5000, 100, '7dZstmkdkRiJGHq7NiVn', '2020-08-26 17:18:43', '2020-08-26 17:18:43'),
(5, '21', '1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrttwjve1k', 0, 24.9069568, 67.0466048, 'Product 1', 44, 100, 'BHjpKH27pHoAmjhYhwIo', '2020-08-26 17:18:43', '2020-08-26 17:18:43'),
(6, '21', '1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"fooo\", \"freeee\"]', 'tkrttyv08hn', 1, 24.9102336, 67.0564352, 'table', 5000, 110, 'G6uF27QOm0LaQQIsmnGs', '2020-08-26 17:18:43', '2020-08-26 17:18:43'),
(7, '21', '1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"foo\"]', 'tkrttyv08hn', 1, 24.9102336, 67.0564352, 'Ubuntu Dis', 4455, 1000, 'HCTrjwMd7Jrxw2eIsHFz', '2020-08-26 17:18:43', '2020-08-26 17:18:43'),
(8, '21', '7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"free\", \"10%\"]', 'wy6u72v1c54', 1, 35.907757, 127.766922, 'Product Testing', 500, 50, 'OWQBdnyJSxv3pRb9z1Ho', '2020-08-26 17:18:43', '2020-08-26 17:18:43'),
(9, '21', '14', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"foo\", \"boo\"]', 'tkrttyv08hn', 1, 24.9102336, 67.0564352, 'Product 1852', 500, 100, 'OovTWSiehRVSuDTAxzpb', '2020-08-26 17:18:43', '2020-08-26 17:18:43'),
(10, '21', '14', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrttyv08hn', 0, 24.9102336, 67.0564352, 'Product 1', 44, 100, 'ThyDIKrHsmSF1ZaA1wcl', '2020-08-26 17:18:43', '2020-08-26 17:18:43'),
(11, '21', '7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"free\", \"limited\", \"75%\"]', 'tkrtwpywc9g', 1, 24.9167872, 67.0695424, 'Product 12345', 555, 100, 'UH9oVxe0QENFUKxjn6kj', '2020-08-26 17:18:43', '2020-08-26 17:18:43'),
(12, '21', '14', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrttyv08hn', 0, 24.9102336, 67.0564352, 'Product 1', 500, 100, 'XL5dsPTLyOBSuvUwr6RQ', '2020-08-26 17:18:43', '2020-08-26 17:18:43'),
(13, '21', '10', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrttfwqteh', 0, 24.887913, 67.0583857, 'just this', 4455, 10, 'XQC9CbK2iZGrAL4Qkczi', '2020-08-26 17:18:43', '2020-08-26 17:18:43'),
(14, '21', '7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"foo\", \"free\"]', 'tkrttyv08hn', 1, 24.9102336, 67.0564352, 'Product 1', 44, 100, 'apsBwLrzjpwAE0HxH1lt', '2020-08-26 17:18:43', '2020-08-26 17:18:43'),
(15, '21', '7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrttyv08hn', 1, 24.9102336, 67.0564352, 'Product 5005', 44, 100, 'eci1mVkIROjpijp4NInE', '2020-08-26 17:18:43', '2020-08-26 17:18:43'),
(16, '21', '2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrttyv08hn', 0, 24.9102336, 67.0564352, 'Product 1', 44, 100, 'ehZNwDqI4zo9tjLAI0UR', '2020-08-26 17:18:43', '2020-08-26 17:18:43'),
(17, '21', '7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"freeee\"]', 'tkrttyv08hn', 1, 24.9102336, 67.0564352, 'Testing 150', 44, 100, 'gOeXbBEwIx1hOEC86gNF', '2020-08-26 17:18:43', '2020-08-26 17:18:43'),
(18, '21', '14', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrttyv08hn', 0, 24.9102336, 67.0564352, 'Product 1', 44, 100, 'gb5vHGcjwmBliYMJAYBI', '2020-08-26 17:18:43', '2020-08-26 17:18:43'),
(19, '21', '7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"free\", \"foo\"]', 'tkrttyv08hn', 0, 24.9102336, 67.0564352, 'Computer Speakers', 50, 100, 'grOVNZPfozu6neSanTmA', '2020-08-26 17:18:43', '2020-08-26 17:18:43'),
(20, '21', '2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[]', 'tkrwnnhj14y', 0, 24.994770260615, 67.066092302655, 'Product 1', 44, 100, 'juI5RrvyAnED5bEogv3I', '2020-08-26 17:18:43', '2020-08-26 17:18:43'),
(21, '21', '7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"ditro\", \"dsd\", \"asdas\"]', 'tkrtwpywc9g', 1, 24.9167872, 67.0695424, 'Linux DX', 44, 100, 'kmeQPn0BlPTGmkWVXcYC', '2020-08-26 17:18:43', '2020-08-26 17:18:43'),
(22, '21', '2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"foo\", \"fere\"]', 'tkrtvgd97ff', 0, 24.936448, 67.0531584, 'TESTING 128', 44, 100, 'lcAKxFTSAAkeyidpebq3', '2020-08-26 17:18:43', '2020-08-26 17:18:43'),
(23, '21', '2', 'Rarely used, inflatable, good quality mattress', '[\"Furniture\", \"Mattress\"]', 'tusxf00d638', 0, 26.6752, 85.1668, 'Ezcomf Single Air Mattress', 45, 1, 'orTxKl0ERFmFWwTwtU0J', '2020-08-26 17:18:43', '2020-08-26 17:18:43'),
(24, '21', '2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,', '[\"foo\", \"free\"]', 'tkrttyv08hn', 0, 24.9102336, 67.0564352, 'Bed Sheet', 44, 100, 'xK4YvEcW3xs9ALzNvhod', '2020-08-26 17:18:43', '2020-08-26 17:18:43');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
                         `id` bigint(20) UNSIGNED NOT NULL,
                         `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                         `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                         `created_at` timestamp NULL DEFAULT NULL,
                         `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'SuperAdmin', 'web', '2020-08-23 05:08:31', '2020-08-23 05:08:31'),
(2, 'SuperAdmin', 'api', '2020-08-23 05:08:31', '2020-08-23 05:08:31'),
(3, 'Admin', 'web', '2020-08-23 05:08:31', '2020-08-23 05:08:31'),
(4, 'Admin', 'api', '2020-08-23 05:08:31', '2020-08-23 05:08:31'),
(5, 'Driver', 'api', '2020-08-23 05:08:31', '2020-08-23 05:08:31'),
(6, 'Consumer', 'api', '2020-08-23 05:08:31', '2020-08-23 05:08:31'),
(7, 'User', 'api', '2020-08-23 05:08:31', '2020-08-23 05:08:31');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
                                        `permission_id` bigint(20) UNSIGNED NOT NULL,
                                        `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
                             `id` int(11) NOT NULL,
                             `user_id` int(11) NOT NULL,
                             `role_id` int(11) NOT NULL,
                             `created_at` timestamp NULL DEFAULT NULL,
                             `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roooms`
--

CREATE TABLE `roooms` (
                          `id` int(11) NOT NULL,
                          `messeges_count` int(11) NOT NULL,
                          `initiator_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                          `recipient_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                          `document_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                          `created_at` timestamp NULL DEFAULT NULL,
                          `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
                         `id` int(11) NOT NULL,
                         `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                         `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                         `contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `zip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `api_token` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `fcm_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `web_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `device_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `contact_verified` int(11) NOT NULL DEFAULT '0',
                         `email_verified` int(11) NOT NULL DEFAULT '0',
                         `email_verified_at` timestamp NULL DEFAULT NULL,
                         `document_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                         `created_at` timestamp NULL DEFAULT NULL,
                         `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `first_name`, `last_name`, `email`, `password`, `contact`, `city`, `state`, `street`, `unit`, `zip`, `username`, `api_token`, `fcm_token`, `web_token`, `device_token`, `contact_verified`, `email_verified`, `email_verified_at`, `document_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(19, NULL, 'John', 'McLane', 'john@mclanes.com', '78638cd6bcda9209cc959fa8f4a01bf3b808f4a8876ce26e2ef9b29f51e42782', '8801971904008', 'Dhaka', 'Dhaka', 'House 98 Bashundhara', 'B', '1130', NULL, NULL, NULL, NULL, 'e601e712dbcea16efaa8a1', 0, 0, NULL, '3KbVcZWJKZ29gpuvn1TY', NULL, '2020-08-26 16:13:46', '2020-08-26 16:13:46'),
(20, NULL, 'Test', 'Test', 'test@mailinator.com', '18167fcff6066a2c05890634b88c08535800dfe6bdc6168b46156d5fa20af335', '919925014279', 'Rajkot', 'Gujarat', 'Aalay Park', 'B6', '360005', NULL, NULL, NULL, NULL, '2677270d4bd9c813bfa70e', 0, 0, NULL, '9N2qyTGfJUgCcNUXQtGv', NULL, '2020-08-26 16:13:46', '2020-08-26 16:13:46'),
(21, NULL, 'Kranthi', 'Kandagatla', 'Krantisruj@gmail.com', '6f1a5f5557f2cd8f030aa13561a9720e1498c937d041ee278734e3913b8d84d3', '+16287779619', 'Irving', 'TX', '1136 hidden ridge', '47', '75038', NULL, NULL, NULL, NULL, '', 0, 0, NULL, 'AYGpBQmQ51Rg8HcVSKlD', NULL, '2020-08-26 16:13:46', '2020-08-26 16:13:46'),
(22, NULL, 'Swamy', 'Kottu', 'swamykottu@gmail.com', '4ea5a7fdf62b9fcf6eadfa5bc1456ea10357c7700fe91b64b6c6a20ea22e4af6', '919010557261', 'Hyderabad', 'Telangana', 'Huda Layout', 'JNTU', '500072', NULL, NULL, NULL, NULL, '4d5914a5c91b62944e2547', 0, 0, NULL, 'Boe9GsCMt2Xp9jpm8Ag3', NULL, '2020-08-26 16:13:46', '2020-08-26 16:13:46'),
(23, NULL, 'Tapuk', 'Timak', 'tapuk@mailinator.com', '18167fcff6066a2c05890634b88c08535800dfe6bdc6168b46156d5fa20af335', '917041439950', 'rajkot', 'Gujarat', 'rajkot', 'B6', '360005', NULL, NULL, NULL, NULL, '', 0, 0, NULL, 'H2F6WqvHs69RHLn6t7RL', NULL, '2020-08-26 16:13:46', '2020-08-26 16:13:46'),
(24, NULL, 'Zippy', 'Cronenberg', 'zippy@cronenbergs.com', 'cf3569591cee9a3ea28a8c9726acc37e6f4c42338907e0d6735f4c8a02e7d014', '+17096782000', 'New Jersey', 'New Jersey', '5 Conway Street', '4B', 'J1C2W9', NULL, NULL, NULL, NULL, 'bf5f87a07b1585f4804341', 0, 0, NULL, 'IohRNi3Evame32OvzdQh', NULL, '2020-08-26 16:13:46', '2020-08-26 16:13:46'),
(25, NULL, 'Zippy', 'Cronenberg', 'abrarhgalib@gmail.com', 'cf3569591cee9a3ea28a8c9726acc37e6f4c42338907e0d6735f4c8a02e7d014', '+17096782000', 'New Jersey', 'New Jersey', '5 Conway Street', '4B', 'J1C2W9', NULL, NULL, NULL, NULL, '', 0, 0, NULL, 'MZjoeHovFsAB9xyPiaA5', NULL, '2020-08-26 16:13:46', '2020-08-26 16:13:46'),
(26, NULL, 'shoaib', 'uddin', 'shoaibuddin@mailinator.com', '857ef2a3d6ed3367b624d32a2170cafa1b67e5b125bbe486e1909fefdb83fd6d', '923432322008', 'karachi', 'Sindh', 'Raza Square', 'N6', '75300', NULL, NULL, NULL, NULL, '', 0, 0, NULL, 'Qqqb2quQSx1EgpBa2d2X', NULL, '2020-08-26 16:13:46', '2020-08-26 16:13:46'),
(27, NULL, 'Ali', 'Khan', 'alikhan@mailinator.com', '857ef2a3d6ed3367b624d32a2170cafa1b67e5b125bbe486e1909fefdb83fd6d', '+17096782000', 'New Jersey', 'New Jersey', '5 Conway Street', '4B', 'J1C2W9', NULL, NULL, NULL, NULL, '81ff270b54840917c5a11f', 0, 0, NULL, 'Yj0kRaj3Ps6r6qgN2VM0', NULL, '2020-08-26 16:13:46', '2020-08-26 16:13:46'),
(28, NULL, 'Shams', 'Khan', 'shams@mailinator.com', '857ef2a3d6ed3367b624d32a2170cafa1b67e5b125bbe486e1909fefdb83fd6d', '+17096782000', 'New Jersey', 'New Jersey', '5 Conway Street', '4B', 'J1C2W9', NULL, NULL, NULL, NULL, 'fkaQlNwGrEIKASKe-t5ctu:APA91bGK8LHhwZ2hx1dTX-HILV9RkV2mYrz2bGH0vqwQrYGDnWJcdZRMqfTtliIx1TKFV6DYyBeLUxbzaFySb3u0LQ0e1T-uHJ2F9JKKxZgRtt0ZuS8zsNF3zT3m7vMsTjkvNO8iAz_Q', 0, 0, NULL, 'dT4L6N30GP3ZZF58lVWv', NULL, '2020-08-26 16:13:46', '2020-08-26 16:13:46'),
(29, NULL, 'Gargi', 'Kantesaria', 'punit@mailinator.com', '01b5d309f39065fe4d2157fa3d37123817b952eb9a8bf95518eb192a54b83770', '917041439950', 'rajkot', 'Gujarat', 'rajkot', 'B6', '360005', NULL, NULL, NULL, NULL, '', 0, 0, NULL, 'fv9uTOY0nujsJmbLp5qL', NULL, '2020-08-26 16:13:46', '2020-08-26 16:13:46'),
(30, NULL, 'Gargi', 'Kantesaria', 'gargikantesaria@gmail.com', '2e95f50dba29b157ee7d73c81d05567896def9b222d45ce1f78b438cea205cb4', '919925014279', 'Rajkot', 'Gujarat', 'Alay Park', 'B6', '360005', NULL, NULL, NULL, NULL, '8e77089e8ab0e266c7d1a3', 0, 0, NULL, 'm0rmvhrZ1U9XFp9n9RFe', NULL, '2020-08-26 16:13:46', '2020-08-26 16:13:46'),
(31, NULL, 'Chandrashekar', 'Mutha', 'csmutha@gmail.com', '4ea5a7fdf62b9fcf6eadfa5bc1456ea10357c7700fe91b64b6c6a20ea22e4af6', '919248761599', 'Hyderabad', 'Telangana', 'Nizampet', 'Bandari', '500072', NULL, NULL, NULL, NULL, '', 0, 0, NULL, 'mw6wzWyTp2gyH5hPNQwe', NULL, '2020-08-26 16:13:46', '2020-08-26 16:13:46'),
(32, NULL, 'Gargi', 'Kantesaria', 'gargikantesaria@mailinator.com', 'c1f49881bda18676b53d605eec5641fe998b5a93a0ba1d850721acdda110d35d', '918780807379', 'Rajkot', 'Gujarat', 'Aaalay Park', 'B6', '360005', NULL, NULL, NULL, NULL, '07c6abf5e7c32921172e1c', 0, 0, NULL, 'oCrAZfljJ3giR4bfiFhF', NULL, '2020-08-26 16:13:46', '2020-08-26 16:13:46');

-- --------------------------------------------------------

--
-- Table structure for table `wallets`
--

CREATE TABLE `wallets` (
                           `id` int(11) NOT NULL,
                           `balance` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                           `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                           `document_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                           `created_at` timestamp NULL DEFAULT NULL,
                           `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_types`
--
ALTER TABLE `category_types`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
    ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
    ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
    ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
    ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
    ADD PRIMARY KEY (`id`),
    ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
    ADD PRIMARY KEY (`id`),
    ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
    ADD PRIMARY KEY (`id`),
    ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
    ADD PRIMARY KEY (`id`),
    ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
    ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photourls`
--
ALTER TABLE `photourls`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
    ADD PRIMARY KEY (`permission_id`,`role_id`),
    ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roooms`
--
ALTER TABLE `roooms`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `users_email_unique` (`email`),
    ADD UNIQUE KEY `users_api_token_unique` (`api_token`);

--
-- Indexes for table `wallets`
--
ALTER TABLE `wallets`
    ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `category_types`
--
ALTER TABLE `category_types`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=564;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
    MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
    MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
    MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `photourls`
--
ALTER TABLE `photourls`
    MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
    MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roooms`
--
ALTER TABLE `roooms`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `wallets`
--
ALTER TABLE `wallets`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Auto_increment for table `Settings`
--



--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
    ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
    ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
    ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
