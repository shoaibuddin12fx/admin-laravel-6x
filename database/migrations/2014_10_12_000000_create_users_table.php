<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->string('name')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('contact')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('street')->nullable();
            $table->string('unit')->nullable();
            $table->string('zip')->nullable();
            $table->string('username')->nullable();
            $table->string('api_token', 60)->unique()->nullable()->default(null);
            $table->string('fcm_token')->nullable();
            $table->string('web_token')->nullable();
            $table->string('device_token')->nullable();
            $table->string('otp_code')->nullable();
            $table->integer('contact_verified')->default(0);
            $table->integer('email_verified')->default(0);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('document_id')->nullable();
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
