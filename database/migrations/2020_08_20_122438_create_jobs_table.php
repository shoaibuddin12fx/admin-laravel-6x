<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->string('delivery_address')->nullable();
            $table->double('delivery_latitude')->nullable();
            $table->double('delivery_longitude')->nullable();
            $table->string('description')->nullable();
            $table->string('expected_delivery_time')->nullable();
            $table->string('geohash')->nullable();
            $table->string('item_category')->nullable();
            $table->string('job_address')->nullable();
            $table->double('job_latitude')->nullable();
            $table->double('job_longitude')->nullable();
            $table->double('job_price');
            $table->string('package_size')->nullable();
            $table->string('posted_at')->nullable();
            $table->string('posted_id')->nullable();
            $table->string('priority')->nullable();
            $table->string('receiver_instructions')->nullable();
            $table->string('security_code')->nullable();
            $table->string('status')->nullable();
            $table->string('document_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
