<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->string("added_by");
            $table->string("category_id");
            $table->string("description");
            $table->json("extra_labels");// CHeck and verify it
            $table->string("geohash");
            $table->boolean("insured");
            $table->double("latitude");
            $table->double("longitude");
            $table->string("name");
            $table->double("price");
            $table->double("quantities_available");
            $table->string("document_id");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
