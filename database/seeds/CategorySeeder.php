<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $catArray = [
            'Toys & Games', 'Books, Movies & Music', 'Women\'s Clothing & Shoes',
            'Baby Products', 'Antiques & Collectibles', 'Home Decor',
            'Major Appliances', 'Men\'s Clothing & Shoes',
            'Garden & Outdoor', 'Bath', 'Arts & Crafts', 'Miscellaneous',
            'Health & Beauty', 'Kid\'s Clothing', 'Handbags',
            'Sporting Goods', 'Office Supplies', 'Tools & Home Improvement',
            'Furniture'
        ];

        //
        foreach ($catArray as $cat) {
            DB::table('categories')->insert([
                'name' => $cat,
            ]);
        }
    }
}
