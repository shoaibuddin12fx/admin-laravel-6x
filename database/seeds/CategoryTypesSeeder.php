<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $catArray = [
            'Baby & Kids', 'Entertainment & Hobbies', 'Clothing & Accessories',
            'Home & Garden', 'Miscellaneous', 'Electronics'
        ];

        //
        foreach ($catArray as $cat) {
            DB::table('category_types')->insert([
                'name' => $cat,
            ]);
        }
    }
}
