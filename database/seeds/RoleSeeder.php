<?php

use Spatie\Permission\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rolesArray = [
            [
                'name' => 'SuperAdmin',
                'guard_name' => 'web'
            ],
            [
                'name' => 'SuperAdmin',
                'guard_name' => 'api'
            ],
            [
                'name' => 'Admin',
                'guard_name' => 'web'
            ],
            [
                'name' => 'Admin',
                'guard_name' => 'api'
            ],
            [
                'name' => 'Driver',
                'guard_name' => 'api'
            ],
            [
                'name' => 'Consumer',
                'guard_name' => 'api'
            ],
            [
                'name' => 'User',
                'guard_name' => 'api'
            ],
        ];

        //
        for($i = 0; $i < count($rolesArray); $i++) {
            Role::create($rolesArray[$i]);
        }
    }
}
