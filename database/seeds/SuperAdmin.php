<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SuperAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user_id = DB::table('users')->insertGetId([
            'name' => 'anupresy',
            'first_name' => 'anupresy',
            'email' => 'anupresy@gmail.com',
            'password' => bcrypt('Superuser1'),
        ]);

        DB::table('role_user')->insert([
            'user_id' => $user_id,
            'role_id' => 1
        ]);
    }
}
