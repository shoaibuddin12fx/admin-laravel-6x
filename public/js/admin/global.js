$(document).ready(function(){

    var startLoading = function () {
        $('.customLoader').show();
    };
    var stopLoading = function () {
        $('.customLoader').hide()
    };

    //function ajax_request()
    var ajax_request = function ($url, $self, $method, onSuccess, onError, $reload = true, hideModal = true) {
        onSuccess = onSuccess || null;
        onError = onError || null;
        var ajaxOptions = {};
        ajaxOptions.url = $url;
        if ($method === 'PUT') {
            ajaxOptions.data = new FormData($self[0]);
            ajaxOptions.enctype = 'multipart/form-data';
            ajaxOptions.processData = false;
            ajaxOptions.contentType = false;
            ajaxOptions.cache = false;
            ajaxOptions.async = false;
            $method = 'POST';
            //For team help: https://stackoverflow.com/questions/31631206/method-not-allowed-when-put-used-over-ajax-for-laravel-resource
        } else {
            ajaxOptions.data = new FormData($self[0]);
            ajaxOptions.enctype = 'multipart/form-data';
            ajaxOptions.processData = false;
            ajaxOptions.contentType = false;
            ajaxOptions.cache = false;
        }
        ajaxOptions.method = $method;
        ajaxOptions.success = function (data) {
            var result = data;
            if (onSuccess) {
                onSuccess($self, result);
            }
            if(hideModal){
                $self.parents('div.modal').modal('hide');
            }

            new Noty({
                type: 'success',
                layout: 'topRight',
                text: result.message
            }).show();

            // if($reload == true){
            //     renderHTML();
            // }

            stopLoading();
        };
        ajaxOptions.error = function (data) {
            var result = JSON.parse(data.responseText);
            stopLoading();

            $.each(result.errors, function (k, v) {
                new Noty({
                    type: 'error',
                    layout: 'topRight',
                    text: v
                }).show();
            });

            if (onSuccess) {
                onSuccess($self, data.responseJSON );
            }




        };
        startLoading();
        $.ajax(ajaxOptions);
    };

})
