@extends('layouts.app')

@section('main')
<!-- Main Content -->
    <div class="gx-main-content">
        <!--gx-wrapper-->
        <div class="gx-wrapper">
            @include('includes.breadcrumb')
            <div class="row">
                <div class="col-lg-12">
                    <div class="gx-card">
                        <div class="gx-card-header">
                        </div>
                        <div class="gx-card-body">
                            <form class="form-horizontal" action="{{ route('admin.categories.update', $category->id) }}" method="POST" enctype="multipart/form-data">
                                {{method_field('PUT')}}
                                {{ csrf_field() }}
                                @include('categories.form')
                                <div class="form-group row">
                                    <div class="col-md-10 col-sm-9 offset-md-2 offset-sm-3">
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/gx-wrapper-->

        @include('includes.footer')

    </div>
    <!-- /main content -->

@endsection