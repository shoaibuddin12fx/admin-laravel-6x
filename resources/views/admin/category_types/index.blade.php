@extends('admin.index')

@section('content')
<!-- Main Content -->
<div class="gx-main-content">
    <!--gx-wrapper-->
    <div class="gx-wrapper">
        <div class="animated slideInUpTiny animation-duration-3">
            @include('includes.breadcrumb')
            <div class="row">
                <div class="col-lg-12">
                    <div class="gx-card">
                        <div class="gx-card-header">
                            <a href="{{ route('admin.category_types.create') }}" class="gx-btn btn-primary">Create</a>
                        </div>
                        <div class="gx-card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Image</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($category_types as $category_type)
                                            <tr class="gradeX">
                                                <td>{{$category_type->name}}</td>
                                                <td><img src="{{$category_type->image}}" width="100"></td>
                                                <td>
                                                    <form action="{{ route('admin.category_types.destroy',$category_type->id) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <a href='{{ route("admin.category_types.edit", $category_type->id) }}' class="gx-btn gx-btn-teal">
                                                            <i class="zmdi zmdi-edit zmdi-hc-fw"></i>
                                                        </a>
                                                        <button type="submit" class="gx-btn gx-btn-secondary"><i class="zmdi zmdi-delete zmdi-hc-fw"></i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Image</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/gx-wrapper-->
    @include('includes.footer')
</div>
<!-- /main content -->
@endsection