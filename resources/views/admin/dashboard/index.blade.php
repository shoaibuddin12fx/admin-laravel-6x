@extends('admin.index')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    @include('admin.dashboard.main')
@stop




