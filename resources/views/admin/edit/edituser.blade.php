@extends('admin.index')

@section('content')


    <div class="section">
        <div class="row">
            <div class="col-lg-12 ">
                <div class="col-lg-6">
                <div class="card">
        <h5 class="card-title p-3">Edit User</h5>
            <div class="card-body">
               
                <form action="{{url('admin/updateuser',$user->id)}}" method="POST">
                @csrf
                        <div class="form-group ">
                            <label for="my-input">First Name</label>
                            <input id="my-input" class="form-control" type="text" value="{{$user->first_name}}" name="fname">
                        </div>
                        <div class="form-group">
                            <label for="my-input">Last Name</label>
                            <input id="my-input" class="form-control" type="text" value="{{$user->last_name}}" name="lname">
                        </div>
                        <div class="form-group">
                            <label for="my-input">Email</label>
                            <input id="my-input" class="form-control" type="text" value="{{$user->email}}" name="email">
                        </div>
                        <div class="form-group">
                            <label for="my-input">Contact</label>
                            <input id="my-input" class="form-control" type="text" value="{{$user->contact}}" name="contact">
                        </div>
                        <div class="form-group">
                            <label for="my-input">city</label>
                            <input id="my-input" class="form-control" type="text" value="{{$user->city}}" name="city">
                        </div>
                        <div class="form-group">
                            <label for="my-input">state</label>
                            <input id="my-input" class="form-control" type="text" value="{{$user->state}}" name="state">
                        </div>
                        <div class="form-group">
                            <label for="my-input">street</label>
                            <input id="my-input" class="form-control" type="text" value="{{$user->street}}" name="stret">
                        </div>
                        <input type="submit" class="btn btn-info" value="Save">
                </form>
            </div>
        </div>
                </div>
            </div>
        </div>
        
    </div>
@endsection