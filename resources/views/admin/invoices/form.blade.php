<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Amount</label>
    <div class="col-md-10 col-sm-9">
        <input type="number" class="form-control" id="amount" value="{{ $invoice->amount ?? old('amount') }}" name="amount" placeholder="Amount">
    </div>
</div>
<div class="line-dashed"></div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Job</label>
    <div class="col-md-10 col-sm-9">
        <select class="form-control" id="job_id" name="job_id">
            @foreach ($jobs as $job)
                <option value="{{ $job->id }}" @isset($job)
                    @if($invoice->job_id == $job->id)selected @endif 
                @endisset >{{ $job->description }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Capture Id</label>
    <div class="col-md-10 col-sm-9">
        <input type="number" class="form-control" id="capture_id" value="{{ $invoice->capture_id ?? old('capture_id') }}" name="capture_id" placeholder="Capture Id">
    </div>
</div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Document Id</label>
    <div class="col-md-10 col-sm-9">
        <input type="number" class="form-control" id="document_id" value="{{ $invoice->document_id ?? old('document_id') }}" name="document_id" placeholder="Document Id">
    </div>
</div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Time</label>
    <div class="col-md-10 col-sm-9">
        <input type="number" class="form-control" id="timestamp" value="{{ $invoice->timestamp ?? old('timestamp') }}" name="timestamp" placeholder="Time">
    </div>
</div>
<div class="line-dashed"></div>
