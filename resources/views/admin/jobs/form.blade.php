<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Delivery Address</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="delivery_address" value="{{ $job->delivery_address ?? old('delivery_address') }}" name="delivery_address" placeholder="Delivery Address">
    </div>
</div>
<div class="line-dashed"></div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Delivery Latitude</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="delivery_latitude" value="{{ $job->delivery_latitude ?? old('delivery_latitude') }}" name="delivery_latitude" placeholder="Delivery Latitude">
    </div>
</div>
<div class="line-dashed"></div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Delivery Longitude</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="delivery_longitude" value="{{ $job->delivery_longitude ?? old('delivery_longitude') }}" name="delivery_longitude" placeholder="Delivery Longitude">
    </div>
</div>
<div class="line-dashed"></div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Description</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="description" value="{{ $job->description ?? old('description') }}" name="description" placeholder="Description">
    </div>
</div>
<div class="line-dashed"></div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Expected Delivery Time</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="expected_delivery_time" value="{{ $job->expected_delivery_time ?? old('expected_delivery_time') }}" name="expected_delivery_time" placeholder="Expected Delivery Time">
    </div>
</div>
<div class="line-dashed"></div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Geohash</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="geohash" value="{{ $job->geohash ?? old('geohash') }}" name="geohash" placeholder="Geohash">
    </div>
</div>
<div class="line-dashed"></div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Item Category</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="item_job" value="{{ $job->item_job ?? old('item_job') }}" name="item_job" placeholder="Item Category">
    </div>
</div>
<div class="line-dashed"></div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Job Address</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="job_address" value="{{ $job->job_address ?? old('job_address') }}" name="job_address" placeholder="Job Address">
    </div>
</div>
<div class="line-dashed"></div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Job Latitude</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="job_latitude" value="{{ $job->job_latitude ?? old('job_latitude') }}" name="job_latitude" placeholder="Job Latitude">
    </div>
</div>
<div class="line-dashed"></div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Job Price</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="job_price" value="{{ $job->job_price ?? old('job_price') }}" name="job_price" placeholder="Job Price">
    </div>
</div>
<div class="line-dashed"></div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Package Size</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="package_size" value="{{ $job->package_size ?? old('package_size') }}" name="package_size" placeholder="Package Size">
    </div>
</div>
<div class="line-dashed"></div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Posted At</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="posted_at" value="{{ $job->posted_at ?? old('posted_at') }}" name="posted_at" placeholder="Posted At">
    </div>
</div>
<div class="line-dashed"></div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Posted Id</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="posted_id" value="{{ $job->posted_id ?? old('posted_id') }}" name="posted_id" placeholder="Posted Id">
    </div>
</div>
<div class="line-dashed"></div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Priority</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="priority" value="{{ $job->priority ?? old('priority') }}" name="priority" placeholder="Priority">
    </div>
</div>
<div class="line-dashed"></div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Receiver Instructions</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="receiver_instructions" value="{{ $job->receiver_instructions ?? old('receiver_instructions') }}" name="receiver_instructions" placeholder="Receiver Instructions">
    </div>
</div>
<div class="line-dashed"></div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Security Code</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="security_code" value="{{ $job->security_code ?? old('security_code') }}" name="security_code" placeholder="Security Code">
    </div>
</div>
<div class="line-dashed"></div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Status</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="status" value="{{ $job->status ?? old('status') }}" name="status" placeholder="Status">
    </div>
</div>
<div class="line-dashed"></div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Document Id</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="document_id" value="{{ $job->document_id ?? old('document_id') }}" name="document_id" placeholder="Document Id">
    </div>
</div>
<div class="line-dashed"></div>