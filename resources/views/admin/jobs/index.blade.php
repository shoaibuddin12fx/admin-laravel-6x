@extends('admin.index')

@section('content')
    <!-- Main Content -->
    <div class="gx-main-content">
        <!--gx-wrapper-->
        <div class="gx-wrapper">
            <div class="animated slideInUpTiny animation-duration-3">

                <section class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1>Jobs</h1>
                            </div>
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                                    <li class="breadcrumb-item active">Jobs</li>
                                </ol>
                            </div>
                        </div>
                    </div><!-- /.container-fluid -->
                </section>
                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">List of all jobs</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <table class="table">
                                            <thead>
                                            <th>Delivery Address</th>
                                            <th>Expected Delivery Time</th>
                                            <th>Item Category</th>
                                            <th>Job Address</th>
                                            <th>Job Price</th>
                                            <th>Posted At</th>
                                            <th>Sender</th>
                                            <th>Receiver</th>
                                            <th>Priority</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                            </thead>
                                            <tbody>
                                            @foreach ($records as $record)
                                                <tr class="gradeX">
                                                    <td><a target="_blank" href="https://www.google.com/maps/search/?api=1&query={{$record->delivery_latitude}},{{$record->delivery_longitude}}">
                                                        {{$record->delivery_address}}
                                                        </a>
                                                    </td>
                                                    <td>{{date('M, d, Y H:i:s', strtotime($record->expected_delivery_time))}}</td>
                                                    <td>{{$record->item_category}}</td>
                                                    <td>
                                                        <a target="_blank" href="https://www.google.com/maps/search/?api=1&query={{$record->job_latitude}},{{$record->job_longitude}}">
                                                            {{$record->job_address}}
                                                        </a>
                                                    </td>
                                                    <td>$ {{$record->job_price}}</td>
                                                    <td>{{date('M, d, Y', strtotime($record->created_at))}}</td>
                                                    <td> @if($record->sender) {{$record->sender->first_name}} {{$record->sender->last_name}} @endif</td>
                                                    <td> @if($record->receiver) {{$record->receiver->first_name}} {{$record->receiver->last_name}} @endif</td>
                                                    <td>{{$record->priority}}</td>
                                                    <td>{{$record->status}}</td>
                                                    <td>
                                                        <a href="{{url('admin/jobs/'.$record->id)}}"><i class="fas fa-info-circle"></i></a>

                                                    </td>

                                            @endforeach


                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.card-body -->

                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->


                        </div>

                        <div class="row">
                            @if (count($records))
                                <div class="col-sm-4 hidden-xs">
                                    <small class="text-muted inline m-t-sm m-b-sm">Showing {!! $records->firstItem() . '-' . $records->lastItem() !!} of {!! $records->total() !!} record(s)</small>
                                </div>
                                <div class="col-sm-3 text-center">
                                </div>
                                <div class="col-sm-5 text-right text-center-xs">
                                    {{ $records->links() }}
                                </div>
                            @else
                                <div class="col-sm-12 text-center">
                                    <small class="text-muted inline m-t-sm m-b-sm">No records found.</small>
                                </div>
                            @endif
                        </div>

                        <!-- /.row -->
                    </div><!-- /.container-fluid -->
                </section>
            </div>
        </div>
        <!--/gx-wrapper-->
        @include('includes.footer')
    </div>


    <!-- /main content -->
@endsection
