@extends('admin.index')

@section('content')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    <!-- Main Content -->
    <div class="gx-main-content">
        <!--gx-wrapper-->
        <div class="gx-wrapper">
            <div class="animated slideInUpTiny animation-duration-3">

                <section class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1>Job Details</h1>
                            </div>
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                                    <li class="breadcrumb-item"><a href="/admin/jobs">Jobs</a></li>
                                    <li class="breadcrumb-item active">Jobs Information</li>
                                </ol>
                            </div>
                        </div>
                    </div><!-- /.container-fluid -->
                </section>
                <section class="content">
                    <div class="container-fluid">

                        <div class="card card-primary">
                        {{--<div class="card-header">
                            <h3 class="card-title">User Information</h3>
                        </div>--}}
                        <!-- /.card-header -->
                            <!-- form start -->
                            <div class="card-body">
                                <form action="{{url('/admin/jobs/status')}}" method="POST">
                                    @csrf

                                <div class="row">

                                    <div class="col-10">
                                        <label for="exampleInputPassword1">You can change job status from the selection</label>
                                        <select name="status" class="form-control">
                                            <option {{$job->status == "Accepted" ? "selected" : ""}}>Accepted</option>
                                            <option {{$job->status == "Pending" ? "selected" : ""}}>Pending</option>
                                            <option {{$job->status == "Delivered" ? "selected" : ""}}>Delivered</option>
                                        </select>
                                        <input type="hidden" name="job_id" value="{{$job->id}}">
                                    </div>
                                    <div class="col-2">
                                        <label>Change Status</label>
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>

                                </div>
                                </form>


                                <hr>


                                <div class="form-group">
                                    <label for="exampleInputPassword1">Delivery Address</label>
                                    <label class="form-control">
                                    <a target="_blank" href="https://www.google.com/maps/search/?api=1&query={{$job->delivery_latitude}},{{$job->delivery_longitude}}">
                                        {{$job->delivery_address}}
                                    </a>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Receiver</label>
                                    <label class="form-control">
                                        @if($job->receiver)
                                        {{$job->receiver->first_name}} {{$job->receiver->last_name}}
                                        @endif
                                    </label>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputPassword1">Expected Delivery Time</label>
                                    <label class="form-control">{{date('M, d, Y H:i:s', strtotime($job->expected_delivery_time))}}</label>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputPassword1">Job Price</label>
                                    <label class="form-control">$ {{$job->job_price}}</label>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputPassword1">Sender</label>
                                    <label class="form-control">
                                        @if($job->receiver)
                                        {{$job->receiver->sender}} {{$job->sender->last_name}}
                                        @endif
                                    </label>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputPassword1">Job Address</label>
                                    <label class="form-control">
                                        <a target="_blank" href="https://www.google.com/maps/search/?api=1&query={{$job->job_latitude}},{{$job->job_longitude}}">
                                            {{$job->job_address}}
                                        </a>
                                    </label>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputPassword1">Priority</label>
                                    <label class="form-control">
                                        @if($job->receiver)
                                        {{$job->receiver->sender}} {{$job->priority}}
                                        @endif
                                    </label>
                                </div>


                                <div class="form-group">
                                    <label for="exampleInputPassword1">Item Category</label>
                                    <label class="form-control">{{$job->item_category}}</label>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputPassword1">Package Size</label>
                                    <label class="form-control">{{$job->package_size}}</label>
                                </div>



                                <div class="form-group">
                                    <label for="exampleInputPassword1">Job Posted at</label>
                                    <label class="form-control">{{date('M, d, Y', strtotime($job->created_at))}}</label>
                                </div>




                            </div>
                            <!-- /.card-body -->


                        </div>
                        <!-- /.row -->
                    </div><!-- /.container-fluid -->
                </section>
            </div>
        </div>
        <!--/gx-wrapper-->
        @include('includes.footer')
    </div>


    <!-- /main content -->
@endsection
