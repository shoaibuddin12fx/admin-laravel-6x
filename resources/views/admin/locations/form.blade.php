<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Device Token</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="device_token" value="{{ $location->device_token ?? old('device_token') }}" name="device_token" placeholder="Device Token">
    </div>
</div>
<div class="line-dashed"></div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Geohash</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="geohash" value="{{ $location->geohash ?? old('geohash') }}" name="geohash" placeholder="Geohash">
    </div>
</div>
<div class="line-dashed"></div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Latitude</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="latitude" value="{{ $location->latitude ?? old('latitude') }}" name="latitude" placeholder="Latitude">
    </div>
</div>
<div class="line-dashed"></div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Longitude</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="longitude" value="{{ $location->longitude ?? old('longitude') }}" name="longitude" placeholder="Longitude">
    </div>
</div>
<div class="line-dashed"></div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Role</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="role" value="{{ $location->role ?? old('role') }}" name="role" placeholder="Role">
    </div>
</div>
<div class="line-dashed"></div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Document Id</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="document_id" value="{{ $location->document_id ?? old('document_id') }}" name="document_id" placeholder="Document Id">
    </div>
</div>
<div class="line-dashed"></div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">User</label>
    <div class="col-md-10 col-sm-9">
        <select class="form-control" id="user" name="user">
            @foreach ($users as $user)
                <option value="{{ $user->id }}" @isset($location)
                    @if($location->user_id == $user->id)selected @endif 
                @endisset >{{ $user->first_name }}</option>
            @endforeach
        </select>
    </div>
</div>