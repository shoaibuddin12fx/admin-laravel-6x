@extends('admin.index')

@section('content')
<!-- Main Content -->
<div class="gx-main-content">
    <!--gx-wrapper-->
    <div class="gx-wrapper">
        <div class="animated slideInUpTiny animation-duration-3">
            @include('includes.breadcrumb')
            <div class="row">
                <div class="col-lg-12">
                    <div class="gx-card">
                        <div class="gx-card-header">
                            <a href="{{ route('admin.locations.create') }}" class="gx-btn btn-primary">Create</a>
                        </div>
                        <div class="gx-card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Device Token</th>
                                            <th>Geohash</th>
                                            <th>Latitude</th>
                                            <th>Longitude</th>
                                            <th>Role</th>
                                            <th>User Id</th>
                                            <th>Document Id</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($locations as $location)
                                            <tr class="gradeX">
                                                <td>{{$location->device_token}}</td>
                                                <td>{{$location->geohash}}</td>
                                                <td>{{$location->latitude}}</td>
                                                <td>{{$location->longitude}}</td>
                                                <td>{{$location->role}}</td>
                                                <td>{{$location->user_id}}</td>
                                                <td>{{$location->document_id}}</td>
                                                <td>
                                                    <form action="{{ route('admin.locations.destroy',$location->id) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <a href='{{ route("admin.locations.edit", $location->id) }}' class="gx-btn gx-btn-teal">
                                                            <i class="zmdi zmdi-edit zmdi-hc-fw"></i>
                                                        </a>
                                                        <button type="submit" class="gx-btn gx-btn-secondary"><i class="zmdi zmdi-delete zmdi-hc-fw"></i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Device Token</th>
                                            <th>Geohash</th>
                                            <th>Latitude</th>
                                            <th>Longitude</th>
                                            <th>Role</th>
                                            <th>User Id</th>
                                            <th>Document Id</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/gx-wrapper-->
    @include('includes.footer')
</div>
<!-- /main content -->
@endsection