@extends('admin.index')

@section('content')

    <div class="section">
   
    <h3>Send Push notifocation</h3>
   
           
        
    <div class="row mt-4">
       <div class="col-lg-12">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                   
                            @if(Session::has('msg'))
                             <p class="alert alert-info" role="alert">
                              {{Session::get('msg')}}  
                              </p>
                              @endif
       <form action="{{url('admin/sendnotification')}}" method="POST">
       @csrf
            <div class="form-group">
                <label for="my-input">Heading</label>
                <input id="my-input" class="form-control" type="text" name="Heading" required>
            </div>
            <div class="form-group">
                <label for="my-input">Discription</label>
               <textarea name="Discription" id="" cols="10" rows="3" class="form-control" required></textarea>
            </div>
           
            
            <div class="form-group">
                <label for="my-input">User to send</label>
               <select  id="" class="form-control" name="userid" required>
               @if(count($users)==1)  
               <option value="{{$users[0]->id}}" >{{$users[0]->first_name}}</option>
               @else
                <option value="" selected>Select User</option>
               @foreach($users as $user)
                    <option value="{{$user->id}}" >{{$user->first_name}}</option>
                 @endforeach  


                  @endif 
               </select>

            </div>
            <button type="submit" class="btn btn-primary">Send</button>

        </form>
      
                    </div>
                </div>
            </div>
       </div>
      
    </div>
       
    </div>
@endsection