@extends('admin.index')
@section('content')
    @include('admin.tables.index', [ 'columns' => $columns, 'data' => $data])
@endsection
