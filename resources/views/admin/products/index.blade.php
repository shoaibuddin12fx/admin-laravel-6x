@extends('admin.index')

@section('content')
    <!-- Main Content -->
    <div class="gx-main-content">
        <!--gx-wrapper-->
        <div class="gx-wrapper">
            <div class="animated slideInUpTiny animation-duration-3">

                <section class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1>Products  </h1>
                            </div>
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                                    <li class="breadcrumb-item active">Products</li>
                                </ol>
                            </div>
                        </div>
                    </div><!-- /.container-fluid -->
                </section>
                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">List of all products</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <table class="table" id="example">
                                            <thead>
                                            <tr>
                                                <th>Image</th>
                                                <th>Category</th>
                                                <th>Name </th>
                                                <th>Price</th>
                                                <th>Quantity</th>
                                                <th>Location</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($records as $record)
                                                <tr class="gradeX">
                                                    <td>
                                                        @if(isset($record->image))
                                                            <img style="width: 75px; height: 75px" class="attachment-img" src="{{$record->image->firebase_url}}" alt="Attachment Image">
                                                        @endif
                                                    </td>
                                                    <td>{{$record->category->name}}</td>
                                                    <td>{{$record->name}}</td>
                                                    <td>$ {{$record->price}}</td>
                                                    <td>{{$record->quantities_available}}</td>
                                                    <td>{{$record->latitude}} - {{$record->longitude}}</td>
                                                </tr>

                                            @endforeach


                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.card-body -->

                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->


                        </div>

                        <div class="row">
                            @if (count($records))
                                <div class="col-sm-4 hidden-xs">
                                    <small class="text-muted inline m-t-sm m-b-sm">Showing {!! $records->firstItem() . '-' . $records->lastItem() !!} of {!! $records->total() !!} record(s)</small>
                                </div>
                                <div class="col-sm-3 text-center">
                                </div>
                                <div class="col-sm-5 text-right text-center-xs">
                                    {{ $records->links() }}
                                </div>
                            @else
                                <div class="col-sm-12 text-center">
                                    <small class="text-muted inline m-t-sm m-b-sm">No records found.</small>
                                </div>
                            @endif
                        </div>

                        <!-- /.row -->
                    </div><!-- /.container-fluid -->
                </section>
            </div>
        </div>
        <!--/gx-wrapper-->
        @include('includes.footer')
    </div>


    <!-- /main content -->
@endsection
