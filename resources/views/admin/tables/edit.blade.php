
<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content"> 
        
            <form action="{{route($formaction)}}" method="post" id=myform class="edit-form" data-public_url="{{ url('')}}" enctype="multipart/form-data" autocomplete="off">
                {{method_field('PUT')}}
                @include('admin.tables.partial.form', ['columns' => $columns])
                @php $isEdit = true @endphp
                <!-- <input type="hidden" name="id" id="item_id" value=""/>
                <input type="hidden" name="table" id="item_id" value=""/> -->
            </form>
        </div>
    </div>
</div>
