


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box box-primary">
{{--                @include('admin.activity_report.filter')--}}
            </div>
        </div>
        <div class="col-md-12">
            <?php ?>
            <div class="box" style="padding:10px;">
                <div id="indexData">
                    @if($errors->has('error_message'))
                        <div class="error">{{ $errors->first('error_message') }}</div>
                    @endif
                    @if(Session::has('msg'))
                  <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('msg') }}</p>
            @endif
                    <h2>{{ $breadcrumb ?? "" }}</h2>
                    <table id="DataTableId" class="table table-responsive table-bordered table-hover data-table"></table>
                </div>
                <div class="loading" style="display: none;"><div class="loader"></div></div>
            </div>
        </div>
    </div>
    @include('admin.tables.edit', ['columns' => $columns])
@stop

@section('js')
    <!-- DataTables -->
    <script src="{{asset('/plugins/datatables/jquery.dataTables.min.js') }} "></script>
    <script src="{{asset('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }} "></script>
    <script src="{{asset('/plugins/datatables-responsive/js/dataTables.responsive.min.js') }} "></script>
    <script src="{{asset('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }} "></script>
    <script type="text/javascript">

        var editor;
        function titleCase(string) {
            var sentence = string.toLowerCase().split(" ");
            for(var i = 0; i< sentence.length; i++){
                sentence[i] = sentence[i][0].toUpperCase() + sentence[i].slice(1);
            }
            return sentence.join(" ");
        }
       

        
            
       
        $(document).ready(function() {
            

            var columns = @json($columns);
            var data = @json($data);

            if ($.fn.DataTable.isDataTable('#DataTableId')) {
                $("#DataTableId").DataTable().clear().destroy();
                $("#DataTableId").empty();
            }

            if(columns.length == 0){
                console.log(columns.length);
                $('.loading').hide();
                return false;
            }

            columns = columns.map(function(x){
                var title = titleCase(x.toString().replace('_', ' '));
                return {data: x, title: title, width: '200px'}
            })

            columns = [
                ...columns,
                {
                    data: null,
                    className: "center",
                    render: function ( data, type, row ) {
                        console.log('data is')
                       // console.log(data)
                         $values=JSON.stringify(data)
                         @if(isset($actions))
                        $menu =
                      
                            `<div width="5%" class="menu-button">
                            <div style="position: relative">
                                
                                <a class="dropdown-toggle btn btn-lg" data-toggle="dropdown" href="javascript:;" aria-expanded="false"></a>
                                <ul class="dropdown-menu dropdown-con-set">`;

                                    @foreach($actions as $action )
                                        $link =  '{{ $action['action'] }}';
                                        $link = $link.replace(':id', data.id);
                                        $menu += `<li><a title="{{$action['label']}}"  data-values='${$values}' class="edit_record"  ><i class="fa fa-caret-right"></i>{{ $action['label'] }}</a></li>`;
                                    @endforeach
                                 
                         $menu += `</ul>
                             </div>
                        <div>`;
                       @else
                       $menu='';
                        @endif
                        return $menu;

                    }
                }
            ]
            console.log(columns, data);

            $('#DataTableId').DataTable({
                data: data,
                columns: columns,
                destroy: true,
                search: false,
                "scrollX": true,
                language: {
                    infoEmpty: "No records available",
                }

            });
            
           // $('.edit-record').on('click', function (e) {
               

            $(".edit_record").on("click",function(e){
                        e.preventDefault();
                        console.log($(this).data("values"));
                         $values = $(this).data("values");
                        
                        console.log($values);
                 Object.keys($values).forEach(key => {
                       // console.log(key);
                         $('.modal#edit_modal').find("input#"+key).val($values[key])
                  }); 
                        $('.modal#edit_modal').modal('show');
             })



        });
    </script>
@endsection
