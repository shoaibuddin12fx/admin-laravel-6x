<div class="modal-header">
    <h4 class="modal-title" id="myModalLabel">Form</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            @foreach($columns as $column)

                {{ $label = str_replace("_"," ",$column) }}
               
                <div class="form-group">
{{--                    <label for="user_name">{{ $label }}</label>--}}
                @if($column=='id')
                    <input type="text" class="form-control" name="{{$column}}" id="{{$column}}" pattern="*" title="{{$label}}"  value="" placeholder="{{$label}}" readonly/>
                    @else
                    <input type="text" class="form-control" name="{{$column}}" id="{{$column}}" pattern="*" title="{{$label}}"  value="" placeholder="{{$label}}" />
                    @endif
                </div>
               
            @endforeach


        </div>

    </div>
</div>
<div class="modal-footer">
    {{ csrf_field() }}
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary action-btn">Save</button>
</div>
