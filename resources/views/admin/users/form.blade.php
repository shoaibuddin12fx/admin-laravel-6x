<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Name</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="name" value="{{ $user->name ?? old('name') }}" name="name" placeholder="Name">
    </div>
</div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">First Name</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="first_name" value="{{ $user->first_name ?? old('first_name') }}" name="first_name" placeholder="First Name">
    </div>
</div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Last Name</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="last_name" value="{{ $user->last_name ?? old('last_name') }}" name="last_name" placeholder="Last Name">
    </div>
</div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Email</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="email" value="{{ $user->email ?? old('email') }}" name="email" placeholder="Email">
    </div>
</div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Password</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="password" value="{{ $user->password ?? old('password') }}" name="password" placeholder="Password">
    </div>
</div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Contact</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="contact" value="{{ $user->contact ?? old('contact') }}" name="contact" placeholder="Contact">
    </div>
</div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">City</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="city" value="{{ $user->city ?? old('city') }}" name="city" placeholder="City">
    </div>
</div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">State</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="state" value="{{ $user->state ?? old('state') }}" name="state" placeholder="State">
    </div>
</div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Street</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="street" value="{{ $user->street ?? old('street') }}" name="street" placeholder="Street">
    </div>
</div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Unit</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="unit" value="{{ $user->unit ?? old('unit') }}" name="unit" placeholder="Unit">
    </div>
</div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Zip</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="zip" value="{{ $user->zip ?? old('zip') }}" name="zip" placeholder="Zip">
    </div>
</div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Username</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="username" value="{{ $user->username ?? old('username') }}" name="username" placeholder="Username">
    </div>
</div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Api Token</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="api_token" value="{{ $user->api_token ?? old('api_token') }}" name="api_token" placeholder="Api Token">
    </div>
</div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Fcm Token</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="fcm_token" value="{{ $user->fcm_token ?? old('fcm_token') }}" name="fcm_token" placeholder="Fcm Token">
    </div>
</div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Web Token</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="web_token" value="{{ $user->web_token ?? old('web_token') }}" name="web_token" placeholder="Web Token">
    </div>
</div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Device Token</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="device_token" value="{{ $user->device_token ?? old('device_token') }}" name="device_token" placeholder="Device Token">
    </div>
</div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Contact Verified</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="contact_verified" value="{{ $user->contact_verified ?? old('contact_verified') }}" name="contact_verified" placeholder="Contact Verified">
    </div>
</div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Email Verified</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="email_verified" value="{{ $user->email_verified ?? old('email_verified') }}" name="email_verified" placeholder="Email Verified">
    </div>
</div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Email Verified At</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="email_verified_at" value="{{ $user->email_verified_at ?? old('email_verified_at') }}" name="email_verified_at" placeholder="Email Verified At">
    </div>
</div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Document Id</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="document_id" value="{{ $user->document_id ?? old('document_id') }}" name="document_id" placeholder="Document Id">
    </div>
</div>
<div class="form-group row">
    <label class="col-md-2 col-sm-3 control-label">Remember Token</label>
    <div class="col-md-10 col-sm-9">
        <input type="text" class="form-control" id="remember_token" value="{{ $user->remember_token ?? old('remember_token') }}" name="remember_token" placeholder="Remember Token">
    </div>
</div>
