@extends('admin.index')

@section('content')

    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    <!-- Main Content -->
    <div class="gx-main-content">
        <!--gx-wrapper-->
        <div class="gx-wrapper">
            <div class="animated slideInUpTiny animation-duration-3">

                <section class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1>User Information</h1>
                            </div>
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                                    <li class="breadcrumb-item"> <a href="/admin/users">Users</a></li>
                                    <li class="breadcrumb-item active">User Information</li>
                                </ol>
                            </div>
                        </div>
                    </div><!-- /.container-fluid -->
                </section>
                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <!-- left column -->
                            <div class="col-md-12">
                                <!-- general form elements -->
                                <div class="card card-primary">
                                    {{--<div class="card-header">
                                        <h3 class="card-title">User Information</h3>
                                    </div>--}}
                                    <!-- /.card-header -->
                                    <!-- form start -->
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">First Name</label>
                                                <label class="form-control">{{$user->first_name}}</label>

                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputPassword1">Last Name</label>
                                                <label class="form-control">{{$user->last_name}}</label>

                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputPassword1">Phone</label>
                                                <label class="form-control">{{$user->contact}}</label>

                                            </div>

                                            <div class="form-group">
                                                <label for="exampleInputPassword1">OTP</label>
                                                <form action="{{ url("admin/user/otp") }}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="user_id" value="{{$user->id}}"/>
                                                    <button type="submit" class="btn btn-block btn-primary">Send OTP</button>
                                                </form>

                                            </div>
                                            <div class="form-group">
                                                <form action="{{ url("admin/user/reset-password") }}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="user_id" value="{{$user->id}}"/>
                                                <label for="exampleInputPassword1">Send Password Reset Email</label>
                                                <button type="submit" class="btn btn-block btn-info">Send Email</button>
                                                </form>

                                            </div>


                                        </div>
                                        <!-- /.card-body -->


                                </div>
                                <!-- /.card -->

                            </div>
                            <!--/.col (left) -->
                        </div>


                        <!-- /.row -->
                    </div><!-- /.container-fluid -->
                </section>
            </div>
        </div>
        <!--/gx-wrapper-->
        @include('includes.footer')
    </div>


    <!-- /main content -->
@endsection
