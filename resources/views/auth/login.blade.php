@extends('layouts.auth')

@section('content')
<div class="gx-main-content">

    <!--gx-wrapper-->
    <div class="gx-wrapper login-box" style="margin-bottom: 300px !important;">
        <div class="login-container d-flex justify-content-center align-items-center animated slideInUpTiny animation-duration-3">
            <div class="login-content" style="text-align: center">
                <img style="max-width: 180px" src="https://deliveryist.com/admin-laravel-6x/public/images/small_logo.png" alt="Deilveroo" title="Deilveroo">
                <p class="login-box-msg text-center">Sign in to start your session</p>
                <div class="login-form">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <fieldset>
                            <div class="form-group">
                                <input id="email" type="email" class="form-control form-control-lg @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input id="password" type="password" class="form-control form-control-lg @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group text-row-between">

                                <small>
                                    <a href="{{ route('password.request') }}" title="Reset Password">Forgot your password</a>
                                </small>
                            </div>
                            <div class="text-center">
                                <button href="javascript:void(0)" type="submit" class="gx-btn gx-btn-rounded gx-btn-primary">Sign In</button>

                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--/gx-wrapper-->
</div>
@endsection

