@extends('layouts.auth')

@section('main')
<div class="gx-main-content">
    <!--gx-wrapper-->
    <div class="gx-wrapper">

        <div class="login-container d-flex justify-content-center align-items-center animated slideInUpTiny animation-duration-3">
            <div class="login-content text-center">
                <div class="login-header">
                    <a class="site-logo" href="javascript:void(0)" title="Jambo">
                        <img src="https://via.placeholder.com/194x65" alt="jambo" title="jambo">
                    </a>
                </div>
                <div class="mb-4">
                    <h2>Create an account</h2>
                </div>
                <div class="login-form">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form-group">
                            <input placeholder="Name" class="form-control form-control-lg @error('name') is-invalid @enderror" type="text" value="{{ old('name') }}" id="name" name="name" required autocomplete="name" autofocus>
                             @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input placeholder="Email" class="form-control form-control-lg @error('email') is-invalid @enderror" type="email" value="{{ old('email') }}" id="email" name="email" required autocomplete="email">
                             @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input placeholder="Password" class="form-control form-control-lg @error('password') is-invalid @enderror" type="password" id="password" name="password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>                                
                        <div class="form-group">
                            <input placeholder="Password" class="form-control form-control-lg" type="password" id="password_confrimation" name="password_confirmation">
                        </div>
                        <div class="mt-4 mb-2">
                            <button href="javascript:void(0)" type="submit" class="gx-btn gx-btn-rounded gx-btn-primary">Regsiter</button>
                        </div>
                        <p>Have an account
                            <a href="{{ route('login') }}">Sign in</a>
                        </p>
                    </form>
                </div>
            </div>
        </div>

    </div>
    <!--/gx-wrapper-->
</div>
@endsection
