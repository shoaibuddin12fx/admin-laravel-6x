<div class="page-heading d-sm-flex justify-content-sm-between align-items-sm-center">
    <h2 class="title mb-3 mb-sm-0">{{$breadcrumb[0]}}</h2>
    <nav class="mb-0 breadcrumb">
        <a href="{{ url('/') }}" class="breadcrumb-item">Dashboard</a>
        @foreach ($breadcrumb as $key => $item)
            @if (($key == 0) && (count($breadcrumb) > 1))
                @php
                    $link = str_replace(' ', '_', strtolower($item))
                @endphp
                <a href='{{ route("admin.$link.index") }}' class="breadcrumb-item">{{$item}}</a>
            @else
                <span class="active breadcrumb-item">{{$item}}</span>
            @endif
            
        @endforeach
        
        
    </nav>
</div>
@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
    </div>
@endif
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif