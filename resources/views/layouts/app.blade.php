<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Jumbo - A fully responsive, HTML5 based admin theme">
        <meta name="keywords" content="Responsive, HTML5, admin theme, business, professional, jQuery, web design, CSS3, sass">
        <title>Jumbo Admin</title>
        <!-- Site favicon -->
        <link rel='shortcut icon' type='image/x-icon' href='images/favicon.ico' />
        <!-- /site favicon -->
        <!-- Font Material stylesheet -->
        <link rel="stylesheet" href="{{ asset('fonts/material-design-iconic-font/css/material-design-iconic-font.min.css') }}">
        <!-- /font material stylesheet -->
        <!-- Bootstrap stylesheet -->
        <link href="{{ asset('css/jumbo-bootstrap.css') }}" rel="stylesheet">
        <!-- /bootstrap stylesheet -->
        <!-- Perfect Scrollbar stylesheet -->
        <link href="{{ asset('node_modules/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet">
        <!-- /perfect scrollbar stylesheet -->
        <!-- Datatables stylesheet -->
        <link href="{{ asset('node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
        <!-- /datatables stylesheet -->
        <!-- Jumbo-core stylesheet -->
        <link href="{{ asset('css/jumbo-core.min.css') }}" rel="stylesheet">
        <!-- /jumbo-core stylesheet -->
        <!-- Color-Theme stylesheet -->
        <link id="override-css-id" href="{{ asset('css/theme-dark-indigo.css') }}" rel="stylesheet">
        <!-- Color-Theme stylesheet -->
        @yield('css')
    </head>
    <body id="body" data-theme="dark-indigo">
        <!-- Loader Backdrop -->
        <div class="loader-backdrop">
            <!-- Loader -->
            <div class="loader">
                <svg class="circular" viewBox="25 25 50 50">
                    <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
                </svg>
            </div>
            <!-- /loader-->
        </div>
        <!-- loader backdrop -->
        <!-- Page container -->
        <div class="gx-container">
            <!-- Page Sidebar -->
            <div id="menu" class="side-nav gx-sidebar">
                <div class="navbar-expand-lg">
                    <!-- Sidebar header  -->
                    <div class="sidebar-header">
                        <div class="user-profile">
                            <img class="user-avatar" alt="Domnic" src="https://via.placeholder.com/150x150">
                            <div class="user-detail">
                                <h4 class="user-name">
                                    <span class="dropdown">
                                        @if(Auth::user())
                                        <a class="dropdown-toggle" href="#" role="button" id="userAccount"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{ Auth::user()->name }}
                                        </a>
                                        @endif
                                        <span class="dropdown-menu dropdown-menu-right" aria-labelledby="userAccount">
                                            <a class="dropdown-item" href="javascript:void(0)">
                                                <i class="zmdi zmdi-account zmdi-hc-fw mr-2"></i>
                                                Profile
                                            </a>
                                            <a class="dropdown-item" href="{{route('auth.logout')}}">
                                                <i class="zmdi zmdi-sign-in zmdi-hc-fw mr-2"></i>
                                                Logoutj
                                            </a>
                                        </span>
                                    </span>
                                </h4>
                            </div>
                        </div>
                    </div>
                    <!-- /sidebar header -->
                    <!-- Main navigation -->
                    <div id="main-menu" class="main-menu navbar-collapse collapse">
                        <ul class="nav-menu">
                            <li class="menu no-arrow">
                                <a href="{{ route('home') }}">
                                <i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>
                                    <span class="nav-text">Dashboard</span>
                                </a>
                            </li>
                            <li class="menu no-arrow">
                                <a href="{{ route('admin.category_types.index') }}">
                                <i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>
                                    <span class="nav-text">Category Types</span>
                                </a>
                            </li>
                            <li class="menu no-arrow">
                                <a href="{{ route('admin.categories.index') }}">
                                <i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>
                                    <span class="nav-text">Categories</span>
                                </a>
                            </li>
                            <li class="menu no-arrow">
                                <a href="{{ route('admin.invoices.index') }}">
                                <i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>
                                    <span class="nav-text">Invoices</span>
                                </a>
                            </li>
                            <li class="menu no-arrow">
                                <a href="{{ route('admin.jobs.index') }}">
                                <i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>
                                    <span class="nav-text">Jobs</span>
                                </a>
                            </li>
                            <li class="menu no-arrow">
                                <a href="{{ route('admin.locations.index') }}">
                                <i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>
                                    <span class="nav-text">Locations</span>
                                </a>
                            </li>
                            <li class="menu no-arrow">
                                <a href="{{ route('admin.products.index') }}">
                                <i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>
                                    <span class="nav-text">Products</span>
                                </a>
                            </li>
                            <li class="menu no-arrow">
                                <a href="{{ route('admin.users.index') }}">
                                <i class="zmdi zmdi-trending-up zmdi-hc-fw"></i>
                                    <span class="nav-text">Users</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                    <!-- /main navigation -->
                </div>
            </div>
            <!-- /page sidebar -->


            <!-- Main Container -->
            <div class="gx-main-container">
                <!-- Main Header -->
                <header class="main-header">
                    <div class="gx-toolbar">
                        <div class="sidebar-mobile-menu d-block d-lg-none">
                            <a class="gx-menu-icon menu-toggle" href="#menu">
                            <span class="menu-icon"></span>
                            </a>
                        </div>
                        <a class="site-logo" href="/">
                            <img src="https://via.placeholder.com/194x65" alt="Jumbo" title="Jumbo">
                        </a>
                        {{--  <ul class="quick-menu header-notifications ml-auto">
                            <li class="dropdown">
                                <a href="javascript:void(0)" data-toggle="dropdown" aria-haspopup="true" class="d-inline-block" aria-expanded="true">
                                    <i class="zmdi zmdi-notifications-active icons-alert animated infinite wobble"></i>
                                </a>
                                <div role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                    <div class="gx-card-header d-flex align-items-center">
                                        <div class="mr-auto">
                                            <h3 class="card-heading">Notifications</h3>
                                        </div>
                                    </div>
                                    <div class="dropdown-menu-perfectscrollbar">
                                        <div class="messages-list">
                                            <ul class="list-unstyled">
                                                <li class="media">
                                                    <img alt="stella-johnson" src="https://via.placeholder.com/150x150"
                                                        class="size-40 mr-2 rounded-circle">
                                                    <div class="media-body align-self-center">
                                                        <p class="sub-heading mb-0">Stella Johnson has recently posted an
                                                            album
                                                        </p>
                                                        <a href="javascript:void(0)" class="gx-btn gx-flat-btn gx-btn-xs mb-0">
                                                        <i class="zmdi zmdi-thumb-up text-blue zmdi-hc-fw"></i>
                                                        </a>
                                                        <span class="meta-date"><small>4:10 PM</small></span>
                                                    </div>
                                                </li>
                                                <li class="media">
                                                    <img alt="domnic-harris" src="https://via.placeholder.com/150x150"
                                                        class="size-40 mr-2 rounded-circle">
                                                    <div class="media-body align-self-center">
                                                        <p class="sub-heading mb-0">Alex Brown has shared Martin Guptils
                                                            post
                                                        </p>
                                                        <a href="javascript:void(0)" class="gx-btn gx-flat-btn gx-btn-xs mb-0">
                                                        <i class="zmdi zmdi-comment-text text-grey zmdi-hc-fw"></i>
                                                        </a>
                                                        <span class="meta-date"><small>5:18 PM</small></span>
                                                    </div>
                                                </li>
                                                <li class="media">
                                                    <img alt="domnic-brown" src="https://via.placeholder.com/150x150"
                                                        class="size-40 mr-2 rounded-circle">
                                                    <div class="media-body align-self-center">
                                                        <p class="sub-heading mb-0">Domnic Brown has sent you a group invitation
                                                            for Global Health
                                                        </p>
                                                        <a href="javascript:void(0)" class="gx-btn gx-flat-btn gx-btn-xs mb-0">
                                                        <i class="zmdi zmdi-card-giftcard text-info zmdi-hc-fw"></i>
                                                        </a>
                                                        <span class="meta-date"><small>5:36 PM</small></span>
                                                    </div>
                                                </li>
                                                <li class="media">
                                                    <img alt="john-smith" src="https://via.placeholder.com/150x150"
                                                        class="size-40 mr-2 rounded-circle">
                                                    <div class="media-body align-self-center">
                                                        <p class="sub-heading mb-0">John Smith has birthday today</p>
                                                        <a href="javascript:void(0)" class="gx-btn gx-flat-btn gx-btn-xs mb-0">
                                                        <i class="zmdi zmdi-cake text-warning zmdi-hc-fw"></i>
                                                        </a>
                                                        <span class="meta-date"><small>5:54 PM</small></span>
                                                    </div>
                                                </li>
                                                <li class="media">
                                                    <img alt="jimmy-jo" src="https://via.placeholder.com/150x150"
                                                        class="size-40 mr-2 rounded-circle">
                                                    <div class="media-body align-self-center">
                                                        <p class="sub-heading mb-0">Chris has updated his profile picture</p>
                                                        <a href="javascript:void(0)" class="gx-btn gx-flat-btn gx-btn-xs mb-0">
                                                        <i class="zmdi zmdi-account-box-o text-grey zmdi-hc-fw"></i>
                                                        </a>
                                                        <span class="meta-date"><small>5:25 PM</small></span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="dropdown">
                                <a href="javascript:void(0)" data-toggle="dropdown" aria-haspopup="true" class="d-inline-block" aria-expanded="true">
                                <i class="zmdi zmdi-comment-alt-text icons-alert zmdi-hc-fw"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" data-placement="bottom-end" data-x-out-of-boundaries="">
                                    <div class="gx-card-header d-flex align-items-center">
                                        <div class="mr-auto">
                                            <h3 class="card-heading">Messages</h3>
                                        </div>
                                    </div>
                                    <div class="dropdown-menu-perfectscrollbar1">
                                        <div class="messages-list">
                                            <ul class="list-unstyled">
                                                <li class="media">
                                                    <div class="user-thumb">
                                                        <img alt="Domnic Brown" src="https://via.placeholder.com/150x150"
                                                            class="rounded-circle size-40 user-avatar">
                                                        <span class="badge badge-danger rounded-circle">5</span>
                                                    </div>
                                                    <div class="media-body">
                                                        <div class="d-flex justify-content-between align-items-center">
                                                            <h5 class="text-capitalize user-name mb-0">
                                                                <a href="javascript:void(0)">Domnic Brown</a>
                                                            </h5>
                                                            <span class="meta-date"><small>6:19 PM</small></span>
                                                        </div>
                                                        <p class="sub-heading">There are many variations of passages of...</p>
                                                        <a href="javascript:void(0)" class="gx-btn gx-flat-btn gx-btn-xs text-muted">
                                                        <i class="zmdi zmdi-mail-reply"></i>
                                                        <span>Reply</span>
                                                        </a>
                                                        <a href="javascript:void(0)" class="gx-btn gx-flat-btn gx-btn-xs text-muted">
                                                        <i class="zmdi zmdi-eye"></i>
                                                        <span>Read</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="media">
                                                    <div class="user-thumb">
                                                        <img alt="John Smith" src="https://via.placeholder.com/150x150" class="rounded-circle size-40 user-avatar">
                                                        <span class="badge badge-danger rounded-circle"></span>
                                                    </div>
                                                    <div class="media-body">
                                                        <div class="d-flex justify-content-between align-items-center">
                                                            <h5 class="text-capitalize user-name mb-0">
                                                                <a href="javascript:void(0)">John Smith</a>
                                                            </h5>
                                                            <span class="meta-date"><small>4:18 PM</small></span>
                                                        </div>
                                                        <p class="sub-heading">Lorem Ipsum is simply dummy text of the...</p>
                                                        <a href="javascript:void(0)" class="gx-btn gx-flat-btn gx-btn-xs text-muted">
                                                        <i class="zmdi zmdi-mail-reply"></i>
                                                        <span>Reply</span>
                                                        </a>
                                                        <a href="javascript:void(0)" class="gx-btn gx-flat-btn gx-btn-xs text-muted">
                                                        <i class="zmdi zmdi-eye"></i>
                                                        <span>Read</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="media">
                                                    <div class="user-thumb">
                                                        <img alt="John Smith" src="https://via.placeholder.com/150x150"
                                                            class="size-40 rounded-circle user-avatar">
                                                        <span class="badge badge-danger rounded-circle">8</span>
                                                    </div>
                                                    <div class="media-body">
                                                        <div class="d-flex justify-content-between align-items-center">
                                                            <h5 class="text-capitalize user-name mb-0">
                                                                <a href="javascript:void(0)">John Smith</a>
                                                            </h5>
                                                            <span class="meta-date"><small>7:10 PM</small></span>
                                                        </div>
                                                        <p class="sub-heading">The point of using Lorem Ipsum is that it
                                                            has...
                                                        </p>
                                                        <a href="javascript:void(0)" class="gx-btn gx-flat-btn gx-btn-xs text-muted">
                                                        <i class="zmdi zmdi-mail-reply"></i>
                                                        <span>Reply</span>
                                                        </a>
                                                        <a href="javascript:void(0)" class="gx-btn gx-flat-btn gx-btn-xs text-muted">
                                                        <i class="zmdi zmdi-eye"></i>
                                                        <span>Read</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="media">
                                                    <div class="user-thumb">
                                                        <img alt="alex dolgove" src="https://via.placeholder.com/150x150"
                                                            class="size-40 rounded-circle user-avatar">
                                                        <span class="badge badge-danger rounded-circle"></span>
                                                    </div>
                                                    <div class="media-body">
                                                        <div class="d-flex justify-content-between align-items-center">
                                                            <h5 class="text-capitalize user-name mb-0">
                                                                <a href="javascript:void(0)">alex dolgove</a>
                                                            </h5>
                                                            <span class="meta-date"><small>5:10 PM</small></span>
                                                        </div>
                                                        <p class="sub-heading">It is a long established fact that a reader
                                                            will...
                                                        </p>
                                                        <a href="javascript:void(0)" class="gx-btn gx-flat-btn gx-btn-xs text-muted">
                                                        <i class="zmdi zmdi-mail-reply"></i>
                                                        <span>Reply</span>
                                                        </a>
                                                        <a href="javascript:void(0)" class="gx-btn gx-flat-btn gx-btn-xs text-muted">
                                                        <i class="zmdi zmdi-eye"></i>
                                                        <span>Read</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="media">
                                                    <div class="user-thumb">
                                                        <img alt="Domnic Harris" src="https://via.placeholder.com/150x150" class="size-40 rounded-circle user-avatar">
                                                        <span class="badge badge-danger rounded-circle">3</span>
                                                    </div>
                                                    <div class="media-body">
                                                        <div class="d-flex justify-content-between align-items-center">
                                                            <h5 class="text-capitalize user-name mb-0">
                                                                <a href="javascript:void(0)">Domnic Harris</a>
                                                            </h5>
                                                            <span class="meta-date"><small>7:35 PM</small></span>
                                                        </div>
                                                        <p class="sub-heading">All the Lorem Ipsum generators on the...</p>
                                                        <a href="javascript:void(0)" class="gx-btn gx-flat-btn gx-btn-xs text-muted">
                                                        <i class="zmdi zmdi-mail-reply"></i>
                                                        <span>Reply</span>
                                                        </a>
                                                        <a href="javascript:void(0)" class="gx-btn gx-flat-btn gx-btn-xs text-muted">
                                                        <i class="zmdi zmdi-eye"></i>
                                                        <span>Read</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>  --}}
                    </div>
                </header>
                <!-- /main header -->
                @yield('main')
            </div>
            <!-- /main container -->

        </div>
        <!-- /page container -->

        <!-- Menu Backdrop -->
        <div class="menu-backdrop fade"></div>
        <!-- /menu backdrop -->
        <!--Load JQuery-->
        <script src="{{ asset('node_modules/jquery/dist/jquery.min.js') }}"></script>
        <!--Bootstrap JQuery-->
        <script src="{{ asset('node_modules/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
        <!--Perfect Scrollbar JQuery-->
        <script src="{{ asset('node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js') }}"></script>
        <!--Big Slide JQuery-->
        <script src="{{ asset('node_modules/bigslide/dist/bigSlide.min.js') }}"></script>
        <!--Datatables JQuery-->
        <script src="{{ asset('node_modules/datatables.net/js/jquery.dataTables.js') }}"></script>
        <script src="{{ asset('node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js') }}"></script>
        <!--Custom JQuery-->
        <script src="{{ asset('js/functions.js') }}"></script>
        <script src="{{ asset('js/custom/data-tables.js') }}"></script>
        @yield('js')
    </body>
</html>
