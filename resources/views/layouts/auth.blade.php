<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Jumbo - A fully responsive, HTML5 based admin theme">
        <meta name="keywords" content="Responsive, HTML5, admin theme, business, professional, jQuery, web design, CSS3, sass">
        <title>{{env("APP_NAME")}}</title>
        <!-- Site favicon -->
        <link rel='shortcut icon' type='image/x-icon' href='images/favicon.ico' />
        <!-- /site favicon -->
        <!-- Font Material stylesheet -->
        <link rel="stylesheet" href="{{ asset('fonts/material-design-iconic-font/css/material-design-iconic-font.min.css') }}">
        <!-- /font material stylesheet -->
        <!-- Bootstrap stylesheet -->
        <link href="{{ asset('css/jumbo-bootstrap.css') }}" rel="stylesheet">
        <!-- /bootstrap stylesheet -->
        <!-- Perfect Scrollbar stylesheet -->
        <link href="{{ asset('node_modules/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet">
        <!-- /perfect scrollbar stylesheet -->
        <!-- Datatables stylesheet -->
        <link href="{{ asset('node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
        <!-- /datatables stylesheet -->
        <!-- Jumbo-core stylesheet -->
        <link href="{{ asset('css/jumbo-core.min.css') }}" rel="stylesheet">
        <!-- /jumbo-core stylesheet -->
        <!-- Color-Theme stylesheet -->
        <link id="override-css-id" href="{{ asset('css/theme-dark-indigo.css') }}" rel="stylesheet">
        <!-- Color-Theme stylesheet -->
        @yield('css')
    </head>
    <body id="body" data-theme="dark-indigo">

        <!-- Page container -->
        <div class="gx-container">
            <!-- Main Container -->
            <div class="gx-main-container">
                @yield('content')
            </div>
            <!-- /main container -->

        </div>
        <!-- /page container -->

        <!-- Menu Backdrop -->
        <div class="menu-backdrop fade"></div>
        <!-- /menu backdrop -->
        <!--Load JQuery-->
        <script src="{{ asset('node_modules/jquery/dist/jquery.min.js') }}"></script>
        <!--Bootstrap JQuery-->
        <script src="{{ asset('node_modules/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
        <!--Perfect Scrollbar JQuery-->
        <script src="{{ asset('node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js') }}"></script>
        <!--Big Slide JQuery-->
        <script src="{{ asset('node_modules/bigslide/dist/bigSlide.min.js') }}"></script>
        <!--Datatables JQuery-->
        <script src="{{ asset('node_modules/datatables.net/js/jquery.dataTables.js') }}"></script>
        <script src="{{ asset('node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js') }}"></script>
        <!--Custom JQuery-->
        <script src="{{ asset('js/functions.js') }}"></script>
        <script src="{{ asset('js/custom/data-tables.js') }}"></script>
        @yield('js')
    </body>
</html>
