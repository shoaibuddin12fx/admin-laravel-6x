<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//header('Access-Control-Allow-Origin: *');
//header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization, Accept-Encoding, Accept-Language, Content-Length, Host");
//header("Access-Control-Allow-Headers: *");
//header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");



Route::get('user/create_superadmin_password', 'API\ApiServiceController@createSuperadminPassword')->name('create_superadmin_password.api');

Route::namespace('Auth')->group(function($request) {

    Route::group(['prefix' => 'user'], function () {
        Route::post('/login', 'ApiAuthController@login')->name('login.api');
        Route::post('/register','ApiAuthController@register')->name('register.api');
        Route::post('/createsuperlogin', 'ApiAuthController@createsuperlogin')->name('createsuperlogin.api');
        Route::post('/forgetpassword', 'ApiAuthController@forgot_password');
        Route::post('/social_login', 'ApiAuthController@sociallogin')->name('socialLogin.api');
    });

});

Route::post('user/verifyDriverLicense', 'API\ApiServiceController@verifyDriverLicense')->name('verify_driver_license.api');

Route::group(['middleware' => [ 'cors', 'json.response' ] ], function () {
    // ...



    Route::post('user/sendOTP', 'API\UserController@sendOTP')->name('send_otp.api');
    Route::post('user/verifyOTP', 'API\UserController@verifyOTP')->name('verify_otp.api');

    Route::middleware('auth:api')->group(function($request) {

        Route::post('/track/currentUserLocation', 'API\LocationController@apiCurrentUserLocation')->name('current_user_location.api');

        Route::group(['prefix' => 'user'], function () {
            Route::post('/logout', 'Auth\ApiAuthController@logout')->name('logout.api');
            Route::get('/profile', 'API\UserController@getProfile')->name('profile.api');
            Route::post('/updateProfile', 'API\UserController@updateProfile')->name('update_profile.api');
            Route::post('/consumerProfile/{id}', 'API\UserController@consumerProfile')->name('consumer_profile.api');
            Route::post('/registerDeviceToken', 'API\UserController@registerDeviceToken')->name('register_device_token.api');
            Route::post('/changeRole', 'API\UserController@changeRole')->name('change_role.api');
            Route::post('/deleteAccount', 'API\UserController@deleteAccount')->name('delete_account.api');
            Route::post('/updatePassword', 'API\UserController@updatePassword')->name('update_password.api');
            Route::post('/isVehicleVerified', 'API\UserController@isVehicleVerified')->name('is_vehicle_verified.api');

        });

        Route::group(['prefix' => 'wallet'], function () {
            Route::post('/setAmountInWallet', 'API\JobController@setAmountInWallet')->name('set_amount_in_wallet.api');
            Route::post('/getWalletRecords', 'API\JobController@getWalletRecords')->name('get_wallet_records.api');
            Route::post('/getWalletCashout', 'API\JobController@getWalletCashout')->name('get_wallet_cashout.api');

        });

        Route::group(['prefix' => 'job'], function () {
            Route::get('/postedJobs', 'API\JobController@postedJobs')->name('posteed_jobs.api');
            Route::get('/trackJob/{id}', 'API\JobController@trackJob')->name('posteed_jobs.api');
            Route::post('/add', 'API\JobController@addJob')->name('add_jobs.api');

            Route::post('/findNearByDrivers', 'API\JobController@findNearByDrivers')->name('find_near_by_drivers.api');
            Route::post('/accept', 'API\JobController@acceptJob')->name('accept_jobs.api');
            Route::post('/update', 'API\JobController@updateJob')->name('update_jobs.api');
            // verify complete job
            Route::post('/verifyCompletedJob', 'API\JobController@verifyCompletedJob')->name('verify_completed_job.api');



            Route::post('/delete', 'API\JobController@deleteJob')->name('delete_jobs.api');
            Route::post('/nearbyJobs', 'API\JobController@nearbyJobs')->name('near_by_jobs.api');
            Route::post('/deliveryJobs', 'API\JobController@deliveryJobs')->name('delivery_jobs.api');
            Route::post('/ratingsFromDriver', 'API\JobController@ratingsFromDriver')->name('rating_driver.api');
            Route::post('/uploadDriverLicence', 'API\UserController@uploadDriverLicence')->name('vehicle_data.api');
            Route::post('/reviewFromClient', 'API\JobController@ratingsFromDriver')->name('review_from_client.api');
            Route::post('/reviewFromDriver', 'API\JobController@ratingsFromDriver')->name('review_from_driver.api');




        });

        Route::group(['prefix' => 'product'], function () {
            Route::get('/getCategories', 'API\CategoryController@getCategories')->name('categories_all.api');
            Route::get('/products/{id}', 'API\ProductController@apiGetSingleProduct')->name('products_id.api');
            Route::get('/getVehicleSizes', 'API\ProductController@getVehicleSizes')->name('products_vehiclesizes.api');
            Route::get('/getConditions', 'API\ProductController@getConditions')->name('products_conditions.api');
            Route::post('/add', 'API\ProductController@apiAddProduct')->name('products_add.api');
            Route::post('/allProducts', 'API\ProductController@apiGetAllProducts')->name('products_all.api');
            Route::post('/setFavorites', 'API\ProductController@apiAddToFavorite')->name('products_fav.api');
            Route::post('/imageUpload', 'API\ProductController@imageUploadPost')->name('products_imageupload.api');
            Route::post('/orderProduct', 'API\ProductController@orderProduct')->name('order_product.api');
            Route::post('/myitem', 'API\ProductController@myItems')->name('my_items.api');



        });

        Route::group(['prefix' => 'chat'], function () {
            Route::get('/messages/{id}', 'API\ChatsController@fetchMessages');
            Route::post('/messages', 'API\ChatsController@sendMessage');
            Route::post('/roomMessages', 'API\ChatsController@sendRoomMessage');
            Route::post('/chatList', 'API\ChatsController@chatList');
        });

        Route::group(['prefix' => 'payment'], function () {
            Route::post('/getPayableAmount', 'API\JobController@getPayableAmount');
            Route::post('/payWithCard', 'API\JobController@payWithCard');
            Route::post('/getPaymentMethods', 'API\JobController@getPaymentMethods');
            Route::post('/createPaypalOrder', 'API\JobController@setAmountInWallet');
            Route::post('/createPaypalCapture', 'API\JobController@createPaypalCapture');
        });

        Route::group(['prefix' => 'driver'], function () {
            Route::post('/uploadDriverLicence', 'API\UserController@uploadDriverLicence'); // equals job/vehicleData
            Route::post('/payWithCard', 'API\JobController@payWithCard');
        });







        });


//    Route::post('/api_add_category', 'CategoryController@apiAddCategories')->name('api_add_category');
//    Route::post('/api_add_users', 'UserController@apiAddUsers')->name('api_add_users');
//    Route::post('/api_add_users', 'UserController@apiAddUsers')->name('api_add_users');
//    Route::post('/api_add_products', 'ProductController@apiAddProduct')->name('api_add_products');
//    Route::post('/api_add_jobs', 'JobController@apiAddJobs')->name('api_add_jobs');
//    Route::post('/api_add_invoices', 'InvoiceController@apiAddInvoices')->name('api_add_invoices');
//    Route::post('/api_add_locations', 'LocationController@apiAddLocations')->name('api_add_locations');



    Route::namespace('API')->group(function($request) {

        Route::get('/firebasetest', 'ApiServiceController@firebasetest')->name('firebasetest');

        Route::middleware('auth:api')->get('/user', function (Request $request) {
            return $request->user();
        });


    });

});
Route::get('dirver_earnings/{id}','API\ApiServiceController@driverearnings');
