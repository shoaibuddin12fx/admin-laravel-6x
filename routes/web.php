<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    Route::get('/clear_cache', function() {
        Artisan::call('cache:clear');
        Artisan::call('view:clear');
        Artisan::call('route:clear');
        Artisan::call('config:clear');
    });

    Auth::routes();
    Route::get('/home', 'HomeController@index')->name('home');
    Route::post('/login', 'Auth\LoginController@login');
    Route::get('/logout', 'Auth\LoginController@logout')->name('auth.logout');

    Route::get('/', 'HomeController@index')->name('home')->middleware('superAdmin');
    /*Route::get('/home', 'HomeController@index')->name('home')->middleware('superAdmin');*/

    Route::middleware('superAdmin')->prefix('admin')->name('admin.')->group(function () {
        Route::resources([
            'jobs' => 'JobController',
            'products' => 'ProductController',
            'users' => 'UserController',
            'vehicles'=>'VehiclesController',
        ]);
        Route::put('updatereview',"Notificationcontroller@updatereview")->name('review.edit');
        Route::put('updatepayment','InvoiceController@update')->name('payment.edit');
        Route::put('updatejob','JobController@update')->name('jobs.edit');
        Route::put('updateuser','UserController@updateuser')->name('users.edit');
        Route::put('updatevehicles','VehiclesController@updaterecord')->name('vehicles.edit');
        Route::post('updateuser/{id}','UserController@updateuser');
       // Route::get('edituser/{id}','UserController@edituser')->name('users.edit');
        Route::get('sendnotification/{id}','Notificationcontroller@sendtouser')->name('users.sendnotification');
        Route::post('user/otp', 'UserController@sendOTP');
        Route::get('sentnotifications','Notificationcontroller@sentnotifications')->name('sentnotifications');
        Route::post('user/reset-password', 'UserController@sendResetPassword');
        Route::post('jobs/status', 'JobController@changeStatus');
        Route::post('addvehicle','VehiclesController@addvehicle');
        Route::get('payments','PaymentController@index');
        Route::get('settings','UserController@getSettings');
        Route::post('settings', 'UserController@postSettings');
        Route::get('reviews','JobController@getReviews');
        Route::get('orders','ProductController@getOrders');
        Route::get('orders','OrderController@index');
        Route::get('notification','Notificationcontroller@index');
        Route::post('sendnotification','Notificationcontroller@save');
        Route::get('invoice','InvoiceController@view');
        Route::get('deleteincoice/{id}','InvoiceController@trash');
        Route::get('deletevehicle/{id}','VehiclesController@deletevehicle');
    });
    Route::get('/deletevehicle/{id}','VehiclesController@deletevehicle');
    Route::get('/Getvehicle/{id}','VehiclesController@Getvehicle');

    
